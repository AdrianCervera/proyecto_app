<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>


	<script src="resources/lib/angular.min.js"></script>
	<script src="resources/lib/ui-bootstrap-tpls-2.1.1.min.js"></script>
	<link rel="stylesheet" href="resources/lib/bootstrap-3.3.7-dist/css/bootstrap.min.css"> <!-- the reason for everything. Clean styles-->
    <link rel="stylesheet" href="resources/lib/font-awesome-4.7.0/css/font-awesome.min.css"><!-- Styles for right controls-->
    <link rel="stylesheet" href="resources/lib/angular-material.min.css">
	<script src="resources/lib/angular-route.min.js"></script>
	<script src="resources/lib/angular-animate.min.js"></script>
    <script src="resources/lib/angular-aria.min.js"></script>
	<script src="resources/lib/angular-touch.min.js"></script>
    <script src='resources/lib/textAngular-rangy.min.js'></script>
    <script src='resources/lib/textAngular-sanitize.min.js'></script>
	<script src="resources/lib/angular-material.min.js"></script>
    <script src='resources/lib/textAngular.min.js'></script>
    
    <script src="resources/lib/jquery.min.js"></script>
  	<script src="resources/lib/bootstrap.min.js"></script>
  	
  	<!-- Bootstrap Date-Picker Plugin -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
  	
	<script src="resources/home.js"></script>

<title>Curso aplicación</title>

  
<style type="text/css">
[ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
  display: none !important;
}


/* textangular */
.ta-editor {
    min-height: 300px;
    height: auto;
    overflow: auto;
    font-family: inherit;
    font-size: 100%;
    margin: 20px 0;
}


/*********   SIDE MENU   ********************************/
 /* The side navigation menu */
.sidenav {
    height: 100%; /* 100% Full-height */
    width: 20%; /* 0 width - change this with JavaScript */
    position: fixed; /* Stay in place */
    z-index: 1; /* Stay on top */
    top: 0;
    left: 0;
    background-color: #111;/*#ff3d3d;*/ /* Red */
   /* color: #000000 */  /* Black */
    overflow-x: hidden; /* Disable horizontal scroll */
    padding-top: 60px; /* Place content 60px from the top */
    transition: 0.5s; /* 0.5 second transition effect to slide in the sidenav */
}

/* The navigation menu links */
.sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
   /* color: #000000; */
    display: block;
    transition: 0.3s
}

/* When you mouse over the navigation links, change their color */
.sidenav a:hover, .offcanvas a:focus{
    color: #f1f1f1;
}

/* Position and style the close button (top right corner) */
.sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
}

/* Style page content - use this if you want to push the page content to the right when you open the side navigation */
#main {
    transition: margin-left .5s;
    padding: 20px;
}

/* On smaller screens, where height is less than 450px, change the style of the sidenav (less padding and a smaller font size) */
@media screen and (max-height: 450px) {
    .sidenav {padding-top: 15px;}
    .sidenav a {font-size: 18px;}
}

 /* Contenedor principal */
.defMainViewContainer {
    margin-left: 20em;  /* margen inicial */
  }
  
  /* openNavSpan */
  
 .hide-opener {
 	visibility:hidden;
   /*display: none;*/
 }

/*********   SIDE MENU  FIN  ********************************/

/* custom form */

customForm {
	padding-left: 10%;
}


/* TABS  */

.tabsdemoDynamicHeight md-content {
  background-color: transparent !important; }
  .tabsdemoDynamicHeight md-content md-tabs {
    background: #870101;
    border: 1px solid #a5a4a4; }
    .tabsdemoDynamicHeight md-content md-tabs md-tabs-wrapper {
      background: white; }
  .tabsdemoDynamicHeight md-content h1:first-child {
    margin-top: 0; }


/* PADDING BOTONES */

/**********/

.padding-button-custom {
	padding: 0.5em;
}

</style>
      

</head>

<body ng-app="home">

	<div id="mainAppContainer">
	
	
	<!--   MOSTRAR ELEMENTOS SI PERFIL AUTENTICADO
	
		<sec:authorize access="isAuthenticated()">
		  <div style="border: 1px solid red"> YES, you are logged in! </div>
		</sec:authorize>
	-->
          
	  <div ng-view></div>

	<!-- <script src="js/angular-bootstrap.js" type="text/javascript"></script> -->
	<!--  <script src="resources/home.js"></script>  -->

	<script src="resources/controller/alumno_controller.js"></script>
	<script src="resources/controller/profesor_controller.js"></script>
	<script src="resources/controller/admin_controller.js"></script>
	<script src="resources/controller/curso_controller.js"></script>
	<script src="resources/controller/listado_curso_controller.js"></script>
	<script src="resources/controller/gestion_curso_controller.js"></script>
	<script src="resources/controller/gestion_cursos_admin_controller.js"></script>
	<script src="resources/controller/gestion_alumno_controller.js"></script>
	<script src="resources/controller/gestion_profesor_controller.js"></script>
	<script src="resources/controller/gestion_admin_controller.js"></script>
	<script src="resources/controller/editar_tema_controller.js"></script>
	<script src="resources/controller/editar_ejercicio_controller.js"></script>
	<script src="resources/controller/editar_curso_controller.js"></script>
	<script src="resources/controller/tema_controller.js"></script>
	<script src="resources/controller/teoria_controller.js"></script>
	<script src="resources/controller/examen_controller.js"></script>
	<script src="resources/controller/ejercicio_controller.js"></script>
	<script src="resources/controller/login_controller.js"></script>
	<script src="resources/controller/main_view_controller.js"></script>
	<script src="resources/controller/evaluacion_controller.js"></script>
	<script src="resources/service/usuario_service.js"></script>
	<script src="resources/service/curso_service.js"></script>
	<script src="resources/service/tema_service.js"></script>
	<script src="resources/service/teoria_service.js"></script>
	<script src="resources/service/examen_service.js"></script>
	<script src="resources/service/ejercicio_service.js"></script>
	<script src="resources/service/evaluacion_service.js"></script>
	<script src="resources/service/login_service.js"></script>
	<script src="resources/service/main_view_service.js"></script>
	<script src="resources/service/alumno_curso_service.js"></script>
	<script src="resources/side_navigation_menu.js"></script>
 <!--     <script src="resources/service/flash_service.js"></script>   -->



</body>
</html>
