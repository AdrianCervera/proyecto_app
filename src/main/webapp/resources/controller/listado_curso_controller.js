'use strict';

angular.module('home').controller('listado_curso_controller', ['$scope', '$location', 'curso_service', 'Usuario', 'Curso', function($scope, $location, curso_service, Usuario, Curso) {
    var self = this;

    self.ver_curso = ver_curso;
    
    obtener_listado_cursos();
    
    function obtener_listado_cursos(){
    	console.log(" == OBTENER LISTADO CURSOS PROFESOR: ==");
    	console.log(Usuario.getId());
    	curso_service.listadoCursos(Usuario.getId()).then(
	            function(content) {
	            	$scope.listadoCursos = content
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	                self.success = false;
	                self.error = true;
	            }
	    );
    }
    
    function ver_curso(id_curso){

    	console.log("VER CURSO:");
    	console.log(id_curso);
    	
    	Curso.setId(id_curso); // establecemos curso activo
    	
    	
    	$location.path("/gestion_curso");
    }
  
 
}]);
