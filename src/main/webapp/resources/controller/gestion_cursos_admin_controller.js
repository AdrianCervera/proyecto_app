'use strict';

angular.module('home').controller('gestion_cursos_admin_controller', ['$scope', '$location', 'curso_service', 'Usuario', 'Curso', function($scope, $location, curso_service, Usuario, Curso) {
    var self = this;

    self.crear_curso = crear_curso;
    self.editar_curso = editar_curso;
    self.borrar_curso = borrar_curso;
    
    obtener_listado_cursos();
    
    function obtener_listado_cursos(){
    	console.log(" == OBTENER LISTADO CURSOS: ==");

    	curso_service.listadoCursosAdmin().then(
	            function(content) {
	            	console.log(content);
	            	$scope.listadoCursos = content
	            },
	            function(errResponse){
	                console.log(errResponse);
	            }
	    );
    }  
    
    
    function editar_curso(id_curso){

    	console.log("EDITAR CURSO:");
    	console.log(id_curso);
    	Curso.setId(id_curso);
    	console.log(Curso.getId());
    		
    	$location.path("/editar_curso");

    }  
    
    function borrar_curso(id_curso){
    	
    	if (confirm("¿Seguro que desea borrar el curso? Se eliminarán los temas junto con todos los contenidos relacionados.")){
        	curso_service.borrarCurso(id_curso).then(
    	            function(content) {
    	            	console.log(content);
    	            	obtener_listado_cursos();  // refrescamos variable listado cursos
    	            },
    	            function(errResponse){
    	                console.log(errResponse);
    	            }
    	    );
    	}
    	
    }  

    function crear_curso(){

    	console.log("CREAR CURSO:");
    	$location.path("/crear_curso");

    }     
 
}]);
