'use strict';

angular.module('home').controller('gestion_profesor_controller', ['$scope', '$location', 'usuario_service', 'Profesor', function($scope, $location, usuario_service, Profesor) {
    var self = this;

    self.crear_usuario = crear_usuario;
    self.editar_usuario = editar_usuario;
   // self.borrar_usuario = borrar_usuario;
    
    cargar_datos_usuarios();
    
    function cargar_datos_usuarios(){
    	// indicamos a la vista que se trata de listado de profesores
    	self.tipo = 'profesor'
    	
    	// obtenemos listado de profesores
    	usuario_service.listadoProfesores().then(
	            function(content) {
	            	console.log(content);
	            	$scope.listadoUsuarios = content
	            },
	            function(errResponse){
	                console.log(errResponse);
	            }
	    );
    }  
    
    
    function editar_usuario(id_profesor){

    	Profesor.setId(id_profesor);    		
    	$location.path("/editar_profesor");

    }  
  
    /*  LOS USUARIOS PUEDEN DESHABILITARSE, PERO NO ELIMINARSE DE LA BASE DE DATOS
    
    function borrar_usuario(id_alumno){
    	
    	if (confirm("Seguro que desea borrar el alumno?")){
    		usuario_service.borrarAlumno(id_alumno).then(
    	            function(content) {
    	            	console.log(content);
    	            	obtener_listado_alumnos();  // refrescamos variable listado usuarios
    	            },
    	            function(errResponse){
    	                console.log(errResponse);
    	            }
    	    );
    	}
    	
    }  
*/
    function crear_usuario(){
    	Profesor.setId(0);
    	$location.path("/crear_profesor");
    }     
 
}]);
