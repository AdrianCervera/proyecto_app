'use strict';

angular.module('home').controller('editar_tema_controller', ['$scope', '$location', 'tema_service', 'main_view_service', 'teoria_service', 'ejercicio_service', 'Usuario', 'Tema', 'Ejercicio', function($scope, $location, tema_service, main_view_service, teoria_service, ejercicio_service, Usuario, Tema, Ejercicio) {
    var self = this;
    
 //   self.tema = {
 //           titulo : '',
 //   }

    self.editar_tema = editar_tema;
    self.editar_teoria = editar_teoria;
    self.editar_ejercicio = editar_ejercicio;
    self.borrar_ejercicio = borrar_ejercicio;
    self.crear_ejercicio = crear_ejercicio;
    
    cargar_contenido_tema();
    
    function cargar_contenido_tema(){
    	// carga los elementos de la vista
    	
        cargar_datos_tema();
        obtener_teoria();
        obtener_listado_ejercicios();
    }
    
    
    function cargar_datos_tema(){
    	// por ahora solo hay titulo
    	
    	tema_service.obtenerTema(Tema.getId()).then(
	            function(content) {
	            	console.log(content);
	                self.tema = content;
	            	console.log("-----------");
	            	console.log(self.tema);
	                
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	            }
	    );
    	
    }
    
    function obtener_teoria(){

    	teoria_service.obtenerTeoria(Tema.getId()).then(
		         function(content) {
		        	 $scope.teoria = content;
		         },
			     function(errResponse){
			     	console.error(errResponse);
			     	//$scope.contenido.contenido = '<p><b>NO HAY CONTENIDO PARA ESTA SECCIÓN</b></p>';
			     }
		);
  	    
    }
    
    function obtener_listado_ejercicios(){
    	console.log(" == OBTENER LISTADO EJERCICIOS TEMA: ==");
    	console.log(Usuario.getId());
    	console.log(Tema.getId());
    	
    	// ejercicios del tema
    	ejercicio_service.listadoEjercicios(Tema.getId()).then(
	            function(content) {
	            	console.log(content);
	            	$scope.listadoEjercicios = content
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	            }
	    );
    	
    	// ejercicios del examen
    	ejercicio_service.listadoEjerciciosExamen(Tema.getId()).then(
	            function(content) {
	            	console.log(content);
	            	$scope.listadoEjerciciosExamen = content
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	            }
	    );
    	
    }  
    
    
    function editar_ejercicio(id_ejercicio){

    	console.log("EDITAR ejercicio:");
    	console.log(id_ejercicio);
    	Ejercicio.setId(id_ejercicio);  // establecemos ejercicio activo
    	console.log(Ejercicio.getId());
    	
    	$location.path("/editar_ejercicio");

    }  
    
    function borrar_ejercicio(id_ejercicio){
    	
    	if (confirm("¿Seguro que desea borrar el ejercicio? Se eliminarán todas sus evaluaciones. ")){
        	ejercicio_service.borrarEjercicio(id_ejercicio).then(
    	            function(content) {
    	            	console.log(content);
    	            	obtener_listado_ejercicios();  // refrescamos variable temas del curso
    	            },
    	            function(errResponse){
    	                console.log(errResponse);
    	                $scope.mensaje_error = errResponse.data.respuesta;
    	            }
    	    );
    	}
    	
    }  

    function crear_ejercicio(){

    	console.log(" crear_ejercicio:");
    	$location.path("/crear_ejercicio");

    }     
    
    function editar_tema(){
    	
    	tema_service.actualizarTema(self.tema).then(
	            function(content) {
	            	console.log(content);
	                self.success_tema = true;
	                self.error_tema = false;
	                self.success_teoria = false;
	                self.error_teoria = false;
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	                self.success = false;
	                self.error = true;
	                self.success_teoria = false;
	                self.error_teoria = false;
	            }
	    );
    	
    }   
    
    function editar_teoria(){
    	    	
    	teoria_service.actualizarTeoria($scope.teoria).then(
	            function(content) {
	            	console.log(content);
	                self.success_teoria = true;
	                self.error_teoria = false;
	                self.success_tema = false;
	                self.error_tema = false;
	                $('html, body').animate({scrollTop:0}, 'slow');
	                
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	                self.success_teoria = false;
	                self.error_teoria = true;
	                self.success_tema = false;
	                self.error_tema = false;
	            }
	    );
    	
    }
 
}]);
