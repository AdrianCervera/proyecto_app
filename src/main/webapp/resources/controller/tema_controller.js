'use strict';

angular.module('home').controller('tema_controller', ['$scope', '$location', 'tema_service', 'Curso', 'Tema', function($scope, $location, tema_service, Curso, Tema) {
    var self = this;
 
    self.submit = submit;
    self.reset = reset;
 
    function crearTema(tema){
    	
    	console.log(Curso.getId())
    	self.tema.curso_id = Curso.getId()
    	
    	tema_service.crearTema(tema).then(
	            function(response) {

	            	console.log('RESPUESTA CREAR TEMA');
	            	console.log(response);
	            	Tema.setId(response.id)  // ID del objeto Tema creado en servidor
	                self.error = false;
	                self.success = true;
	            	$location.path("/editar_tema");

	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	                self.success = false;
	                self.error = true;
	            }
	    );
    	
    }
 
 
    function submit() {
        console.log('Creando nuevo tema', self.tema);
        crearTema(self.tema);
        
    }
    
    function reset(){
        self.tema={titulo:'',curso_id:''};
        $scope.temaForm.$setPristine(); //reset Form
    }
 
}]);
