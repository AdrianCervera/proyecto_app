'use strict';

angular.module('home').controller('alumno_controller', ['$scope', '$location', 'usuario_service', 'curso_service', 'alumno_curso_service', 'Alumno', function($scope, $location, usuario_service, curso_service, alumno_curso_service, Alumno) {
    var self = this;
     
  /*  self.alumno = {
            login : 'prueba',
            password : 'prueba',
            nombre : 'nombreprueba',
            habilitado : 1,
    };*/
     
    self.submit = submit;
    self.reset = reset;
    self.actualizarAlumno = actualizarAlumno;
    self.inscribir_alumno = inscribir_alumno;
    self.desinscribir_alumno = desinscribir_alumno;
 
    cargar_alumno_edicion();
    
    function cargar_listado_cursos_alumno(){
    	// cargamos listado de cursos para suscribir/desuscribir
    	curso_service.listadoCursosAlumno(Alumno.getId()).then(
	            function(response) {
	            	console.log(response);
	            	$scope.listadoCursosInscrito = response[0];
	            	$scope.listadoCursosNoInscrito = response[1];
	            },
	            function(errResponse){
	                console.log(errResponse);
	            }
	    );
    }
    
    function cargar_alumno_edicion(){
      	
    	console.log('ALUMNO ACTIVO')
    	console.log(Alumno.getId())
    	
    	if(Alumno.getId() > 0){  // es un ID de alumno activo, indica que se carga el controlador para la vista de edición
    		
    		// cargamos alumno a editar
        	usuario_service.obtenerAlumno(Alumno.getId()).then(
    	            function(response) {
    	            	console.log(response);
    	            	self.alumno = response;
    	            },
    	            function(errResponse){
    	                console.log(errResponse);
    	            }
    	    );
        	
        	// cargamos el listado de cursos inscritos/ no inscritos para el alumno
        	cargar_listado_cursos_alumno();
        	
    	}

    	
    }
  
    function crearAlumno(alumno){ 
    	
    	console.log(alumno);
    	
    	usuario_service.crearAlumno(alumno).then(
	            function(response) {
	            	console.log(response);
	                $scope.resp = response;
	                self.error = false;
	                self.success = true;
	            	$location.path("/gestion_alumnos");
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	                self.success = false;
	                self.error = true;
	            }
	    );
    	
    }
    
    function actualizarAlumno(){
    	console.log(self.alumno);

    	usuario_service.editarAlumno(self.alumno).then(
	            function(response) {
	            	console.log(response);
	                $scope.resp = response;
	                self.error = false;
	                self.success = true;

	            	// quitamos alumno activo
	            	console.log(Alumno.getId());
	            	Alumno.setId(0);
	            	console.log(Alumno.getId());
	            	
	            	$location.path("/gestion_alumnos");
	                
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	                self.success = false;
	                self.error = true;
	            }
	    );
    	
    	
    }
    
    
    function inscribir_alumno(id_curso){
    	alumno_curso_service.inscribirAlumno(Alumno.getId(), id_curso).then(
	            function(content) {
	            	// recargamos el listado
	            	cargar_listado_cursos_alumno();
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	            }
	    );
    }
    
    
    function desinscribir_alumno(id_curso){
    	
    	if (confirm("¿Seguro que desea desinscribir al alumno? Se eliminarán todas sus evaluaciones.")){
	    	alumno_curso_service.desinscribirAlumno(Alumno.getId(), id_curso).then(
		            function(content) {
		            	// recargamos el listado
		            	cargar_listado_cursos_alumno();
		            },
		            function(errResponse){
		                console.log(errResponse);
		                $scope.mensaje_error = errResponse.data.respuesta;
		            }
		    );
    	}
    }
    
    
    
 
    function submit() {
        crearAlumno(self.alumno);
        //reset();
    }
 
 
    function reset(){
        self.alumno.login='';
        self.alumno.password='';
        self.alumno.nombre='';
        self.alumno.apellidos='';
        self.alumno.dni='';
        self.alumno.telefono='';
        self.alumno.direccion='';
        self.alumno.ciudad='';
        self.alumno.email='';
        $scope.alumnoForm.$setPristine(); //reset Form
    }
 
}]);

