'use strict';

angular.module('home').controller('admin_controller', ['$scope', '$location', 'usuario_service', 'Admin', function($scope, $location, usuario_service, Admin) {
    var self = this;
     
   /* self.admin = {
            login : 'prueba',
            password : 'prueba',
            nombre : 'nombreprueba',
            habilitado : 1,
    };*/
     
    self.submit = submit;
    self.reset = reset;
    self.actualizarAdmin = actualizarAdmin;
    
    cargar_admin_edicion();
    
    function cargar_admin_edicion(){
      	
    	console.log('ADMIN ACTIVO')
    	console.log(Admin.getId())
    	
    	if(Admin.getId() > 0){  // es un ID de admin activo, cargamos el admin para editar
        	usuario_service.obtenerAdmin(Admin.getId()).then(
    	            function(response) {
    	            	console.log(response);
    	            	self.admin = response
    	            },
    	            function(errResponse){
    	                console.log(errResponse);
    	            }
    	    );
    	}

    	
    }
  
    function crearAdmin(admin){ 
    	
    	console.log(admin);
    	
    	usuario_service.crearAdmin(admin).then(
	            function(response) {
	            	console.log(response);
	                $scope.resp = response;
	                self.error = false;
	                self.success = true;
	            	$location.path("/gestion_administradores");
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	                self.success = false;
	                self.error = true;
	            }
	    );
    	
    }
    
    function actualizarAdmin(){
    	console.log(self.admin);

    	usuario_service.editarAdmin(self.admin).then(
	            function(response) {
	            	console.log(response);
	                $scope.resp = response;
	                self.error = false;
	                self.success = true;

	            	// quitamos admin activo
	            	console.log(Admin.getId());
	            	Admin.setId(0);
	            	console.log(Admin.getId());
	            	
	            	$location.path("/gestion_administradores");
	                
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	                self.success = false;
	                self.error = true;
	            }
	    );
    	
    	
    }
 
    function submit() {
        crearAdmin(self.admin);
        //reset();
    }
 
 
    function reset(){
        self.admin.login='';
        self.admin.password='';
        self.admin.nombre='';
        self.admin.apellidos='';
        self.admin.dni='';
        self.admin.telefono='';
        self.admin.direccion='';
        self.admin.ciudad='';
        self.admin.email='';
        $scope.adminForm.$setPristine(); //reset Form
    }
 
}]);

