'use strict';

angular.module('home').controller('teoria_controller', ['$scope', 'teoria_service', function($scope, teoria_service) {
    var self = this;
    //self.teoria={titulo:'tiTulop', contenido:'<p><b>Contenido teórico</b></p><br><p>Prueba</p>',tema_id:1};
 
    self.submit = submit;
    self.reset = reset;
 
    function crearTeoria(teoria){
    	console.log('controller creater');
    	teoria_service.crearTeoria(teoria)
            .then(
            function(errResponse){
            	console.error('Error while creationg teoria');
            }
        );
    }
 
 
    function submit() {
        console.log('Creando nuevo teoria', self.teoria);
        
        crearTeoria(self.teoria);
        
        reset();
    }
    
    
    function reset(){
        self.teoria={contenido:'bn',tema_id:''};
        $scope.teoriaForm.$setPristine(); //reset Form
    }
 
}]);
