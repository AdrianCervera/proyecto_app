'use strict';

angular.module('home').controller('main_view_controller', ['$scope', 'main_view_service', 'ejercicio_service', 'evaluacion_service','Usuario', function($scope, main_view_service, ejercicio_service, evaluacion_service, Usuario) {
    var self = this;
    //self.usuario={login:'Pruebalogin',password:'passprueba',rol:null};
    self.listadoDeTemas;
    self.teoria;
    self.examen;
    self.contenido;
    self.listadoDeEjercicios;
    self.evaluacion;
    
    self.submit = submit;
    self.reset = reset;
    
    $scope.cargar_contenido = cargar_contenido;
    $scope.cargar_listado_temas = cargar_listado_temas;
    $scope.cargar_teoria = cargar_teoria;
    $scope.cargar_examen = cargar_examen;
    $scope.cargar_ejercicios = cargar_ejercicios;
    $scope.mostrar_ejercicio = mostrar_ejercicio;
    $scope.mostrar_solucion = mostrar_solucion;
    $scope.enviar_evaluacion = enviar_evaluacion;

    // variables para ocultar elementos de la vista
    self.ocultar_ejercicio = true;
    self.mostrar_enviado = false;
    self.mostrar_solucion = false;


    //cargar_listado_temas();
    cargar_listado_cursos();
    
    
    function cargar_teoria(id_tema){
   	    //reset();
	   	 $scope.ejercicio = null;
		 $scope.evaluacion = null;
		 self.ocultar_ejercicio = true;
	   	 self.mostrar_solucion = false;
    	
	   	 main_view_service.cargar_teoria(id_tema)
			.then(
		         function(content) {
		        	 console.log('el content responsao a controller cargar teoria ');
		        	 console.log(content);
		        	 $scope.ejercicio = null;
		        	 $scope.contenido = content;
		
			},
		     function(errResponse){
		     	console.error(errResponse);
		     	console.error('Main view controller. Error teoria');
		     	$scope.contenido.contenido = '<p><b>NO HAY CONTENIDO PARA ESTA SECCIÓN</b></p>';
		     });
   	    
    }
    
    function cargar_ejercicios(id_tema, examen){  // examen indica si se cargan ejercicios de tipo examen o normales
    	
    	if(examen){
        	 console.log("ES EXAMEN");
	       	 ejercicio_service.listadoEjerciciosExamenAlumno(id_tema)
				.then(
			         function(content) {
			        	 console.log(content);
			        	 self.listadoDeEjerciciosExamen = content;
			        	 
					},
				     function(errResponse){
			        	 console.log(errResponse);
				     })        	
    		
    	} else{
        	 console.log("NO EXAMEN");
	       	 ejercicio_service.listadoEjerciciosAlumno(id_tema)
				.then(
			         function(content) {
			        	 console.log(content);
			        	 self.listadoDeEjercicios = content;
			        	 
					},
				     function(errResponse){
			        	 console.log(errResponse);
				     });
    		
    	}
    	
    }
    
    function mostrar_ejercicio(id_ejercicio){
   	 	$scope.contenido = null;
   	 	self.mostrar_solucion = false;
    	ejercicio_service.obtenerEjercicioAlumno(id_ejercicio)
		.then(
	         function(content) {	        	 
	        	 console.log(content);
	        	 $scope.ejercicio = content;
	        	 self.ocultar_ejercicio = false;
	        	 console.log(self.mostrar_ejercicio);
	             //document.getElementById("mainViewEj").style.visibility = "visible"; // muestra div con contenido ejercicio y evaluacion
	        	 
			},
		     function(errResponse){
	        	 console.log(errResponse);
		});
    	
    	evaluacion_service.obtenerEvaluacion(Usuario.getId(), id_ejercicio)
    	.then(
   	         function(content) {	        	 
   	        	 console.log("Evaluacion ========");      	 
   	        	 console.log(content);
   	        	 
   	        	 if(!content){  // si no hay evaluacion creada
   	        		 console.log("Ejercicio sin evaluación creada (sin responder por el alumno)");
   	        		 self.mostrar_enviado = false;
   	        	 }
   	        	 else {
   	        		 console.log("Ejercicio con evaluación (ya respondido)")
   	        		 self.mostrar_enviado = true;
   	        	 }
   	        	 $scope.evaluacion = content;
   	        	 
   			},
   		     function(errResponse){
   	        	 console.log(errResponse);
   		});
    	
    }
    
    function mostrar_solucion(id_ejercicio){
    	
    	if(self.mostrar_solucion){
    		self.mostrar_solucion = false;
    	} else{
	    	ejercicio_service.obtenerEjercicioSolucionAlumno(id_ejercicio, Usuario.getId())
			.then(
		         function(content) {	        	 
		        	 console.log(content);
		        	 $scope.ejercicio.respuesta = content.respuesta;
		        	 self.mostrar_solucion = true;
				},
			     function(errResponse){
		        	 console.log(errResponse);
			});
    	}
    	
    }
    
    function enviar_evaluacion(id_ejercicio){

    	console.log($scope.evaluacion);
    	self.evaluacion = $scope.evaluacion;
    	self.evaluacion.ejercicio_id = id_ejercicio;
    	self.evaluacion.alumno_id = Usuario.getId();
    	
    	console.log(self.evaluacion);
    	
    	evaluacion_service.crearEvaluacion(self.evaluacion)
    	.then(
  	         function(content) {	        	 
  	        	 console.log(content);
  	             //document.getElementById("button_enviar").style.visibility = "hidden";  // ocultar o mostrar botones Enviar
  	             //document.getElementById("button_enviado").style.visibility = "visible";
  	             self.mostrar_enviado = true;

  	        	 
  	        	 
      		 },
  		     function(errResponse){
  	        	 console.log(errResponse);
      		 });
    	
    }
    
    // NO SE USA POR AHORA
    function cargar_examen(id_tema){
    	console.log('cargar examen id tema:');
   	    console.log(id_tema);
   	    reset();
	   	 main_view_service.cargar_examen(id_tema)
			.then(
		         function(content) {
		        	 console.log('el content responsao a controller cargar examen ');
		        	 console.log(content);
		        	 $scope.contenido = content;
		
			},
		     function(errResponse){
		     	console.error(errResponse);
		     	console.error('Main view controller. Error examenes');
		     	$scope.contenido.contenido = '<p><b>NO HAY CONTENIDO PARA ESTA SECCIÓN</b></p>';
		     });
    }
    
    
    function cargar_contenido(id_tema){
    	alert(id_tema);
   	    console.log('cargar contenido id tema:');
   	    
   	    console.log(id_tema);
    	main_view_service.cargar_teoria(id_tema)
    		.then(
    	         function(content) {
    	        	 console.log('response a controller cargar teoria ');
    	        	 console.log(content);
    	        	 $scope.teoria = content;
    	
    		},
	        function(errResponse){
	        	console.error(errResponse);
	        	console.error('Main view controller. Error teoria');
	        });
    	
    	/*main_view_service.cargar_examen(id_tema)
    		.then(
    	         function(content) {
    	        	 console.log('response a controller cargar examen ');
    	        	 console.log(content);
    	        	 $scope.examen = content;
    	
    		},
	        function(errResponse){
	        	console.error(errResponse);
	        	console.error('Main view controller. Error examenes');
	        });*/


    }
    
    
    function cargar_listado_temas(curso_id){
    	
    	console.log('controller cargar temas, curso:');
    	console.log(curso_id);
    	
		main_view_service.cargar_listado_temas(curso_id)
			.then(
	            function(content) {
	            	console.log('response a controller cargar_listado_temas: ');
	            	console.log(content);
	            	$scope.listadoDeTemas = content;
	            },
		        function(errResponse){
		        	console.error(errResponse);
		        	console.error('Main view controller. Error cargar_listado_temas');
		        });

    	console.log(' FIN controller cargar temas');
  

    }
    
    function cargar_listado_cursos(){
    	console.log('controller cargar cursos');

    	console.log(Usuario.getId());
    	
    	main_view_service.cargar_listado_cursos(Usuario.getId())
		.then(
            function(content) {
            	console.log('response a controller cargar_listado_cursos: ');
            	console.log(content);
            	$scope.listadoDeCursos = content;
            },
	        function(errResponse){
	        	console.error(errResponse);
	        	console.error('Main view controller. Error cargar_listado_cursos');
	        });

	console.log(' FIN controller cargar cursos');
    }
    
    function submit() {
        console.log('Login...');
        login(self.usuario);
        console.log('Loginao');
        reset();

    }
    
    function reset(){
    	console.log('RESETE');
    	self.contenido=null;
    }      
     
    
}]);