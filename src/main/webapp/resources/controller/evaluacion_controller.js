'use strict';

angular.module('home').controller('evaluacion_controller', ['$scope', '$location', 'tema_service', 'ejercicio_service', 'evaluacion_service', 'Curso', 'Alumno', 'Ejercicio', function($scope, $location, tema_service, ejercicio_service, evaluacion_service, Curso, Alumno, Ejercicio) {
    var self = this;
 
    self.evaluarAlumno = evaluarAlumno;
    self.evaluarEjercicio = evaluarEjercicio;
    self.cargarEjerciciosTema = cargarEjerciciosTema;
    
    cargar_datos_vista();
    
    function cargar_datos_vista(){
    	
    	if(!(Ejercicio.getId() > 0) && Alumno.getId() > 0){  // solo alumno activo - vista lista de ejercicios para evaluar
    		cargarListadoTemas();
    	}
    	
    	if(Ejercicio.getId() > 0 && Alumno.getId() > 0){  // alumno y ejercicio activos - vista form evaluar
        	
        	console.log('EJERCICIO ACTIVO');
        	console.log(Ejercicio.getId());
        	
        	console.log('ALUMNO ACTIVO');
        	console.log(Alumno.getId());
        	
    		// campos modelo ejercicio
    		ejercicio_service.obtenerEjercicio(Ejercicio.getId()).then(
    	            function(response) {
    	            	console.log(response);
    	            	$scope.ejercicio = response;
    	            },
    	            function(errResponse){
    	                console.log(errResponse);
    	            }
    	    );
    		
    		// campos modelo evaluacion
        	evaluacion_service.obtenerEvaluacion(Alumno.getId(), Ejercicio.getId()).then(
    	            function(response) {
    	            	console.log(response);
    	            	$scope.evaluacion = response;

    	            	console.log("EVALUACION ANTES:");
    	        		console.log($scope.evaluacion);
    	            },
    	            function(errResponse){
    	                console.log(errResponse);
    	            }
    	    );
    	}
    	
    }


    // button vista form evaluar
    function evaluarAlumno(){
    	
    	console.log("EVALUACION DESPUES:");
		console.log($scope.evaluacion);

    	evaluacion_service.actualizarEvaluacion($scope.evaluacion).then(
    			function(response) {
	            	console.log(response);	            	
	            	Ejercicio.setId(0);
	            	console.log(Ejercicio.getId());
	                self.success = true;
	                self.error = false;
	            	$location.path("/evaluar_alumno_listado");
	                
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	                self.success = false;
	                self.error = true;
	                $('html, body').animate({scrollTop:0}, 'slow');
	            }
	    );
    	
    }
    
    // cargar listado temas del curso
    function cargarListadoTemas(){
    	tema_service.listadoTemas(Curso.getId()).then(
	            function(content) {
	            	console.log(content);
	            	$scope.listadoTemas = content
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	                self.success = false;
	                self.error = true;	            
	            }
	    )
	     
    }
    
    // cargar ejercicios al seleccionar un tema
    function cargarEjerciciosTema(id_tema){
    	
    	console.log("CARGAR EJERCICIOS TEMA");
		
		 // listado de ejercicios
      	 ejercicio_service.listadoEjerciciosExamen(id_tema)
			.then(
		         function(content) {
		        	 console.log(content);
		        	 
		        	 content.forEach( function(ejercicio, indice, array) {
			        		ejercicio.calificado=false;
			        		ejercicio.respondido=false;
		        		    
		        		    // para cada ejercicio comprobamos si ya ha sido evaluado por el profesor o respondido por el alumno
		        		    evaluacion_service.obtenerEvaluacion(Alumno.getId(), ejercicio.id).then(
		        			         function(content) {
		        			        	 if(content){
			        			        	 var evaluacion = content;
			        			        	 if(evaluacion.calificacion >= 0){
			        			        		 ejercicio.calificado=true;
			        			        	 } else {
			        			        		 ejercicio.respondido=true;
			        			        	 }
		        			        	 }
		        			        		 
		        					 },
		        				     function(errResponse){
		        						 console.log("Error al obtener la evaluación");
		        			        	 console.log(errResponse);
		        				     });
		        		});

		        	 $scope.listadoEjerciciosExamen = content;
		        	 
				},
			     function(errResponse){
		        	 console.log(errResponse);
			     })        	
	
      	 ejercicio_service.listadoEjercicios(id_tema)
			.then(
		         function(content) {
		        	 console.log(content);
		        	 
		        	 content.forEach( function(ejercicio, indice, array) {
			        		ejercicio.calificado=false;
			        		ejercicio.respondido=false;
		        		    
		        		    // para cada ejercicio comprobamos si ya ha sido evaluado por el profesor o respondido por el alumno
		        		    evaluacion_service.obtenerEvaluacion(Alumno.getId(), ejercicio.id).then(
		        			         function(content) {
		        			        	 console.log("CONTENT: ")
		        			        	 console.log(content)
		        			        	 if(content){
			        			        	 console.log("HAY CONTENT")
		        			        		 
			        			        	 var evaluacion = content;
			        			        	 if(evaluacion.calificacion >= 0){
			        			        		 ejercicio.calificado=true;
			        			        	 } else {
			        			        		 ejercicio.respondido=true;
			        			        	 }
		        			        	 }
		        			        		 
		        					 },
		        				     function(errResponse){
		        						 console.log("Error al obtener la evaluación");
		        			        	 console.log(errResponse);
		        				     });
		        		});
		        	 
		        	 $scope.listadoEjercicios = content;
		        	 
				},
			     function(errResponse){
		        	 console.log(errResponse);
			     });
    	
    }
 
    // buttons vista lista de ejercicios del alumno
    function evaluarEjercicio(id_ejercicio){
    	Ejercicio.setId(id_ejercicio);
    	$location.path("/evaluar_alumno");
    }
    

}]);
