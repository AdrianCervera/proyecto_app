'use strict';

angular.module('home').controller('gestion_alumno_controller', ['$scope', '$location', 'usuario_service', 'Alumno', function($scope, $location, usuario_service, Alumno) {
    var self = this;

    self.crear_usuario = crear_usuario;
    self.editar_usuario = editar_usuario;
   // self.borrar_usuario = borrar_usuario;
    
    cargar_datos_usuarios();
    
    function cargar_datos_usuarios(){
    	// indicamos a la vista que se trata de listado de alumnos
    	self.tipo = 'alumno'
    	
    	// obtenemos listado de alumnos
    	usuario_service.listadoAlumnos().then(
	            function(content) {
	            	console.log(content);
	            	$scope.listadoUsuarios = content
	            },
	            function(errResponse){
	                console.log(errResponse);
	            }
	    );
    }  
    
    
    function editar_usuario(id_alumno){

    	Alumno.setId(id_alumno); // alumno activo para edicion
    	$location.path("/editar_alumno");

    }  
  
    /*  LOS USUARIOS PUEDEN DESHABILITARSE, PERO NO ELIMINARSE DE LA BASE DE DATOS
    
    function borrar_usuario(id_alumno){
    	
    	if (confirm("Seguro que desea borrar el alumno?")){
    		usuario_service.borrarAlumno(id_alumno).then(
    	            function(content) {
    	            	console.log(content);
    	            	obtener_listado_alumnos();  // refrescamos variable listado usuarios
    	            },
    	            function(errResponse){
    	                console.log(errResponse);
    	            }
    	    );
    	}
    	
    }  
*/
    function crear_usuario(){
    	Alumno.setId(0);
    	$location.path("/crear_alumno");
    }     
 
}]);
