'use strict';

angular.module('home').controller('gestion_curso_controller', ['$scope', '$location', 'tema_service', 'alumno_curso_service', 'Usuario', 'Curso', 'Tema', 'Alumno', 'Ejercicio', function($scope, $location, tema_service, alumno_curso_service, Usuario, Curso, Tema, Alumno, Ejercicio) {
    var self = this;

    self.editar_tema = editar_tema;
    self.borrar_tema = borrar_tema;
    self.crear_tema = crear_tema;
    self.ejerciciosAlumno = ejerciciosAlumno;
    
    obtener_listado_temas();
    obtener_listado_alumnos();
    
    function obtener_listado_temas(){
    	tema_service.listadoTemas(Curso.getId()).then(
	            function(content) {
	            	console.log(content);
	            	$scope.listadoTemas = content
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	                self.success = false;
	                self.error = true;
	            }
	    );
    }  
    
    function obtener_listado_alumnos(){
    	alumno_curso_service.obtenerAlumnosPorCurso(Curso.getId()).then(
	            function(content) {
	            	console.log(content);
	            	$scope.listadoAlumnos = content
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	                self.success = false;
	                self.error = true;
	            }
	    );
    }  
    
    function editar_tema(id_tema){

    	console.log("EDITAR TEMA:");
    	console.log(id_tema);
    	Tema.setId(id_tema);
    		
    	$location.path("/editar_tema");

    }  
    
    function borrar_tema(id_tema){
    	
    	if (confirm("¿Seguro que desea borrar el tema? Se borrarán la teoría y los ejercicios relacionados.")){
        	tema_service.borrarTema(id_tema).then(
    	            function(content) {
    	            	console.log(content);
    	            	obtener_listado_temas();  // refrescamos variable temas del curso
    	            },
    	            function(errResponse){
    	                console.log(errResponse);
    	                $scope.mensaje_error = errResponse.data.respuesta;
    	                self.success = false;
    	                self.error = true;
    	            }
    	    );
    	}
    	
    }  

    function crear_tema(){
    	$location.path("/crear_tema");
    }     
    
    function ejerciciosAlumno(id_alumno){
    	Alumno.setId(id_alumno); // alumno a evaluar
    	Ejercicio.setId(0);  // no debe haber ejercicio activo en la vista lista
    	$location.path("/evaluar_alumno_listado");
    }
    
 
}]);
