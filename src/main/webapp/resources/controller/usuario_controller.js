'use strict';

angular.module('home').controller('usuario_controller', ['$scope', 'usuario_service', function($scope, usuario_service) {
    var self = this;
    //self.user={login:'',password:'',rol:null};
    //self.user.roles = ["Alumno", "Profesor", "Administrador"];
    
    // NO SE USA !!!
        
    self.usuario = {
            rol : 'alumno',
        };
     
    self.submit = submit;
    self.reset = reset;
 
 
    obtenerUsuarios();
 
    function obtenerUsuarios(){
    	usuario_service.obtenerUsuarios()
            .then(
            function(response) {
                $scope.usuarios = response;
            },
            function(errResponse){
                console.error('Error obtener usuarios');
            }
        );
    }
 
    function crearUsuario(usuario){
    	console.log('controller creater');
    	console.log(usuario.rol);
    	
    	if(usuario.rol == 'admin'){
    		usuario_service.crearAdmin(usuario).then(
    	            obtenerUsuarios,
    	            function(errResponse){
    	            	console.error('Controlador. Error al crear usuario Administrador');
    	            }
    	        );
    	}
    	else if(usuario.rol == 'profesor'){
    		usuario_service.crearProfesor(usuario).then(
    	            obtenerUsuarios,
    	            function(errResponse){
    	            	console.error('Controlador. Error al crear usuario Profesor');
    	            }
    	        );
    	}
    	else if(usuario.rol == 'alumno'){
    		usuario_service.crearAlumno(usuario).then(
    	            function(response) {
    	                $scope.resp = response;
    	            },
    	            function(errResponse){
    	                console.error('Error while crear usuario Alumno controller ');
    	            }
    	    );
    					
    	}
    	
    	else{
    		console.error('Controlador. Valor selected_rol desconocido');
    	}
    	
    	
    	/*usuario_service.crearUsuario(user)
            .then(
            obtenerUsuarios,
            function(errResponse){
            	console.error('Error while creationg User');
            }
        );*/
    }
 
 
    function submit() {
        console.log('Saving New User', self.usuario);
        crearUsuario(self.usuario);

        reset();
    }
 
 
    function reset(){
        self.usuario.nombre='';
        self.usuario.apellidos='';
        self.usuario.dni='';
        self.usuario.telefono='';
        self.usuario.direccion='';
        self.usuario.ciudad='';
        self.usuario.email='';
        $scope.usuarioForm.$setPristine(); //reset Form
    }
 
}]);


