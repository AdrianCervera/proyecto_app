'use strict';

angular.module('home').controller('examen_controller', ['$scope', 'examen_service', function($scope, examen_service) {
    var self = this;
 
    self.submit = submit;
    self.reset = reset;
    
    function crearExamen(examen){
    	
    	console.log(examen.pregunta);
    	console.log(examen.respuesta);
    	
    	examen_service.crearExamen(examen).then(
	            function(response) {
	            	console.log(response);
	                self.error = false;
	                self.success = true;
	                reset();
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	                self.success = false;
	                self.error = true;
	            }
	    );
    	
    }
 
 
    function submit() {
        console.log('Creando nuevo examen', self.examen);
        crearExamen(self.examen);
    }
    
    
    function reset(){
        self.examen={titulo:'',examen_id:''};
        $scope.examenForm.$setPristine();
    }
 
}]);
