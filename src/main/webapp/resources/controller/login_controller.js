'use strict';

angular.module('home').controller('login_controller', ['$scope', '$rootScope', '$location', 'login_service', 'Usuario', function($scope, $rootScope, $location, login_service, Usuario) {
    var self = this;
    self.usuario={login:'admin',password:'admin'};

    self.submit = submit;
    self.reset = reset;
    
    // el usuario no debe estar logueado al ejecutarse este controlador
    auto_logout();
    
    function auto_logout(){
    	login_service.auto_logout().then(
	            function(response) {
	        		console.log('logout OK');
	            },
	            function(errResponse){
	        		console.log('logout KO');
                	$location.path("/login");
	            }
		); 
    }
    
    function login(usuario){
		console.log(usuario);
    	console.log(usuario.rol)
		login_service.login(usuario).then(
	            function(response) {
	        		console.log('login OK');
	        		console.log(response.respuesta);
	                Usuario.setId(response.respuesta);
	                console.log(Usuario.getId())
	                $rootScope.authenticated = true;  // variable para mostrar contenido autenticated
	                self.error = false;

	        		console.log('ROL: ');
	        		console.log(usuario.rol);
	        		
	                if(usuario.rol == 'admin'){
	                	$rootScope.auth_admin = true;
	                	$rootScope.auth_alumno = false;
	                	$rootScope.auth_profesor = false;
	                	$location.path("/main_view_admin");
	                }
	                else if(usuario.rol == 'profesor'){
	                	$rootScope.auth_profesor = true;
	                	$rootScope.auth_admin = false;
	                	$rootScope.auth_alumno = false;
	                	$location.path("/main_view_profesor");
	                }
	                else if(usuario.rol == 'alumno'){
	                	$rootScope.auth_alumno = true;
	                	$rootScope.auth_admin = false;
	                	$rootScope.auth_profesor = false;
	                	$location.path("/main_view_alumno");
	                }

	            	console.log('vuelvo de service js');
	                
	            },
	            function(errResponse){
	        		console.log('login KO');
		        	console.error(errResponse);
	                $rootScope.authenticated = false;
	                reset();
	                self.error = true;
	                $scope.mensaje_error = errResponse.data.respuesta;
	            }
		);   		
	}    	
    
    
    
    /*
    function login(usuario){
    	console.log(usuario.rol)
    	
    	if(usuario.rol == 'admin'){
    		login_service.login_admin(usuario).then(
    		        function(errResponse){
    		        	console.error(errResponse);
    		        	console.error('Login controller. Error login admin: ' + usuario.login);
    		        });
    	}
    	else if(usuario.rol == 'profesor'){
    		login_service.login_profesor(usuario).then(
    		        function(errResponse){
    		        	console.error(errResponse);
    		        	console.error('Login controller. Error login profesor: ' + usuario.login);
    		        });   		
    	}
    	else if(usuario.rol == 'alumno'){
    		console.log('rol==alumno');
    		console.log(usuario);
    		login_service.login_alumno(usuario).then(
    	            function(response) {
    	        		console.log('login alumno');
    	        		console.log(response);
    	                //$scope.resp = response;  // na
    	                Usuario.setId(response.id);
    	                console.log(Usuario.getId())
    	                $rootScope.authenticated = true;  // variable para mostrar contenido autenticated
    	                $location.path("/main_view");
    	                self.error = false;
    	                
    	            },
    	            function(errResponse){
    		        	console.error(errResponse);
    	                console.error('Error crear usuario Alumno controller ');
    	                $rootScope.authenticated = false;
    	                reset();
    	                self.error = true;
    	            }
    		);   		
    	}
    	else{
    		console.error('Login controller. Tipo de rol desconocido. Usuario: ' + usuario.login);
    	}
    	console.log('vuelvo de service js');
    }
    */
    
    function submit() {
        login(self.usuario);
    }
    
    function reset(){
        self.usuario.login='';
        self.usuario.password='';
        $scope.loginForm.$setPristine();
    }      
 
}]);