'use strict';

angular.module('home').controller('curso_controller', ['$scope', '$location', 'curso_service', 'usuario_service', function($scope, $location, curso_service, usuario_service) {
    var self = this;
   //self.curso={titulo= '',profesor_id = ''};
   //self.curso={fecha_fin = '2017-03-14 07:32:34'};
   // self.cursos=[];
   
   self.curso = {
		   fecha_inicio : '2017-09-18',
		   fecha_fin : '2018-01-20',
   };
 
    self.submit = submit;
    self.reset = reset;
    
    obtener_listado_profesores();
    
    function obtener_listado_profesores(){
    	console.log(" == OBTENER LISTADO PROFESORES ==");
    	usuario_service.listadoProfesores().then(
	            function(content) {
	            	console.log(content);
	            	$scope.listadoDeProfesores = content
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	                self.success = false;
	                self.error = true;
	            }
	    );
    }
    
    
 
    function crearCurso(curso){
    	
    	console.log(curso.titulo);
    	console.log(curso.profesor_id);
    	
    	curso_service.crearCurso(curso).then(
	            function(response) {
	            	console.log(response);
	               // $scope.resp = response;
	                self.error = false;
	                self.success = true;
	            	$location.path("/gestion_cursos");
	                
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	                self.success = false;
	                self.error = true;
	            }
	    );
    	
    }
 
 
    function submit() {
        console.log('Creando nuevo curso', self.curso);
        crearCurso(self.curso);
        
       // reset();
    }
    
    
    function reset(){
        self.curso={titulo:'bn',curso_id:''};
        $scope.cursoForm.$setPristine(); //reset Form
    }
 
}]);
