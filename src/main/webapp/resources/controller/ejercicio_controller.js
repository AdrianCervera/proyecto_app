'use strict';

angular.module('home').controller('ejercicio_controller', ['$scope', '$location', 'ejercicio_service', 'tema_service', 'Tema', 'Ejercicio', function($scope, $location, ejercicio_service, tema_service, Tema, Ejercicio) {
    var self = this;
 
    self.submit = submit;
    self.reset = reset;
    self.actualizarEjercicio = actualizarEjercicio;
    
    cargar_ejercicio_edicion();
    
    function cargar_ejercicio_edicion(){
    	
    	console.log('EJERCICIO ACTIVO')
    	console.log(Ejercicio.getId())
    	
    	if(Ejercicio.getId() > 0){  // es un ID de ejercicio activo, cargamos el ejercicio para editar
        	ejercicio_service.obtenerEjercicio(Ejercicio.getId()).then(
    	            function(response) {
    	            	console.log(response);
    	            	$scope.ejercicio = response
    	            },
    	            function(errResponse){
    	                console.log(errResponse);
    	            }
    	    );
    	}

    	
    }

    function crearEjercicio(ejercicio){
    	
    	console.log(ejercicio.pregunta);
    	console.log(ejercicio.respuesta);
    	
    	ejercicio.tema_id = Tema.getId();

    	console.log(Tema.getId());
    	console.log(ejercicio.tema_id);
    	
    	
    	ejercicio_service.crearEjercicio(ejercicio).then(
	            function(response) {
	            	console.log(response);
	                //self.error = false;
	                //self.success = true;
	                //reset();
	                
	            	$location.path("/editar_tema");
	                
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	                self.success = false;
	                self.error = true;
	            }
	    );
    	
    }    

    function actualizarEjercicio(){
    	
    	console.log($scope.ejercicio.id);
    	console.log($scope.ejercicio.pregunta);
    	console.log($scope.ejercicio.respuesta);

    	ejercicio_service.actualizarEjercicio($scope.ejercicio).then(
	            function(response) {
	            	console.log(response);

	            	console.log(' RESET ID EJERSICIO');
	            	
	            	console.log(Ejercicio.getId());
	            	Ejercicio.setId(0);
	            	console.log(Ejercicio.getId());
	            	
	            	$location.path("/editar_tema");
	                
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	                self.success = false;
	                self.error = true;
	            }
	    );
    	
    }
 
 
    function submit() {
        console.log('Creando nuevo ejercicio', self.ejercicio);
        crearEjercicio(self.ejercicio);
    }
    
    
    function reset(){
        self.ejercicio={titulo:'',ejercicio_id:''};
        $scope.ejercicioForm.$setPristine();
    }
 
}]);
