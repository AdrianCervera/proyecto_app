'use strict';

angular.module('home').controller('editar_ejercicio_controller', ['$scope', '$location', 'ejercicio_service', 'Ejercicio', function($scope, $location, ejercicio_service, Ejercicio) {
    var self = this;
 
    self.submit = submit;
    self.reset = reset;
    

    cargar_contenido_ejercicio();
    
    function cargar_contenido_ejercicio(){
    	
    	ejercicio_service.obtenerEjercicio(Ejercicio.getId()).then(
	            function(content) {
	            	console.log(content);
	            	$scope.ejercicio = content
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	                self.success = false;
	                self.error = true;
	            }
	    );
    }
    
    
    function actualizarEjercicio(ejercicio){

        console.log('  ejercio:');
        console.log(ejercicio);
    	
    	ejercicio_service.actualizarEjercicio(ejercicio).then(
	            function(content) {
	            	console.log(content);
	            	$scope.ejercicio = content
	            	$location.path("/gestion_curso");
	            	
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	                self.success = false;
	                self.error = true;
	            }
	    );
    
    }

    function submit() {
        console.log('Creando nuevo ejercicio', self.ejercicio);
        actualizarEjercicio(self.ejercicio);
    }
    
    
    function reset(){
        self.ejercicio={titulo:'',ejercicio_id:''};
        $scope.ejercicioForm.$setPristine();
    }
 
}]);