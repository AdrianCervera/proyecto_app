'use strict';

angular.module('home').controller('profesor_controller', ['$scope', '$location', 'usuario_service', 'Profesor', function($scope, $location, usuario_service, Profesor) {
    var self = this;
     
 /*   self.profesor = {
            login : 'prueba',
            password : 'prueba',
            nombre : 'nombreprueba',
    };*/
     
    self.submit = submit;
    self.reset = reset;
    self.actualizarProfesor = actualizarProfesor;

    cargar_profesor_edicion();
    
    function cargar_profesor_edicion(){
      	
    	console.log('PROFESOR ACTIVO')
    	console.log(Profesor.getId())
    	
    	if(Profesor.getId() > 0){  // es un ID de profesor activo, cargamos el profesor para editar
        	usuario_service.obtenerProfesor(Profesor.getId()).then(
    	            function(response) {
    	            	console.log(response);
    	            	self.profesor = response
    	            },
    	            function(errResponse){
    	                console.log(errResponse);
    	            }
    	    );
    	}

    } 
  
    function crearProfesor(profesor){ 
    	
    	console.log(profesor);
    	
    	usuario_service.crearProfesor(profesor).then(
	            function(response) {
	            	console.log(response);
	                $scope.resp = response;
	                self.error = false;
	                self.success = true;
	            	$location.path("/gestion_profesores");
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	                self.success = false;
	                self.error = true;
	            }
	    );
    	
    }

    
    function actualizarProfesor(){
    	
    	console.log(self.profesor);

    	usuario_service.editarProfesor(self.profesor).then(
	            function(response) {
	            	console.log(response);
	                $scope.resp = response;
	                self.error = false;
	                self.success = true;

	            	// quitamos profesor activo
	            	console.log(Profesor.getId());
	            	Profesor.setId(0);
	            	console.log(Profesor.getId());
	            	
	            	$location.path("/gestion_profesores");
	                
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	                self.success = false;
	                self.error = true;
	            }
	    );
    		
    }
    
    
    function submit() {
        crearProfesor(self.profesor);
    }
 
 
    function reset(){
        self.profesor.login='';
        self.profesor.password='';
        self.profesor.nombre='';
        self.profesor.apellidos='';
        self.profesor.departamento='';
        self.profesor.dni='';
        self.profesor.telefono='';
        self.profesor.direccion='';
        self.profesor.ciudad='';
        self.profesor.email='';
        $scope.profesorForm.$setPristine(); //reset Form
    }
 
}]);

