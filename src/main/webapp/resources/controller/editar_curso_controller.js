'use strict';

angular.module('home').controller('editar_curso_controller', ['$scope', '$location', 'curso_service', 'usuario_service', 'alumno_curso_service', 'Curso', function($scope, $location, curso_service, usuario_service, alumno_curso_service, Curso) {
    var self = this;
   //self.curso={titulo= '',profesor_id = ''};
   //self.curso={fecha_fin = '2017-03-14 07:32:34'};
   // self.cursos=[];
 
    self.editarCurso = editarCurso;
    self.desinscribir_alumno = desinscribir_alumno;
    
    obtener_listado_profesores();
    cargar_datos_curso();
    
    function obtener_listado_profesores(){
    	console.log(" == OBTENER LISTADO PROFESORES ==");
    	usuario_service.listadoProfesores().then(
	            function(content) {
	            	console.log(content);
	            	$scope.listadoDeProfesores = content
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	                self.success = false;
	                self.error = true;
	            }
	    );
    }
    
    function obtener_listado_alumnos_curso(){
    	// obtenemos los alumnos inscritos en el curso
    	alumno_curso_service.obtenerAlumnosPorCurso(Curso.getId()).then(
	            function(content) {

	            	console.log("VUELVO A JS CONTROLLER:");
	            	$scope.listadoAlumnos = content;
	            	console.log($scope.listadoAlumnos);
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	            }
	    );
    }
    
    function cargar_datos_curso(){
    	
    	// obtenemos el contenido del curso
    	curso_service.obtenerCurso(Curso.getId()).then(
	            function(content) {
	            	console.log(content);
	                self.curso = content;	                
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	            }
	    );
    	
    	// cargamos el listado
    	obtener_listado_alumnos_curso();
    	
    }

    function editarCurso(){
    	console.log(self.curso);
    	
    	curso_service.actualizarCurso(self.curso).then(
	            function(content) {
	            	console.log(content);
	                self.success = true;
	                self.error = false;
	            	$location.path("/gestion_cursos");
	                
	            },
	            function(errResponse){
	                console.log(errResponse);
	                $scope.mensaje_error = errResponse.data.respuesta;
	                self.success = false;
	                self.error = true;
	            }
	    );
    	
    }
    
    function desinscribir_alumno(id_alumno){
    	
    	if (confirm("¿Seguro que desea desinscribir al alumno? Se eliminarán todas sus evaluaciones.")){
	    	alumno_curso_service.desinscribirAlumno(id_alumno, Curso.getId()).then(
		            function(content) {
		            	// recargamos el listado
		            	obtener_listado_alumnos_curso();
		            },
		            function(errResponse){
		                console.log(errResponse);
		                $scope.mensaje_error = errResponse.data.respuesta;
		            }
		    );
    	}
    }
    
}]);
