<div class="generic-container">
		
    <div class="container">
        <div class="generic-container col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
        
	        <div id="pillsContainer">
				<ul class="nav nav-pills" role="tablist">
					<li class="active"><a href="#/main_view_admin">Inicio</a></li>
					<li class="active"><a href="#/" onclick="return confirm('Volver al inicio?')">Logout</a></li>
				</ul>
			</div>		
			<div style="height:4em;"></div>

		    <div class="panel panel-default">
            <div class="panel-heading" ng-show="gestion_usuario_ctrl.tipo=='alumno'"><span class="lead">Listado de alumnos</span></div>
            <div class="panel-heading" ng-show="gestion_usuario_ctrl.tipo=='profesor'"><span class="lead">Listado de profesores</span></div>
            <div class="panel-heading" ng-show="gestion_usuario_ctrl.tipo=='admin'"><span class="lead">Listado de administradores</span></div>
              <div class="tablecontainer">
                  <table class="table table-hover" style="width:100%;min-width:300px">
                      <thead>
                          <tr>
                              <th>Login</th>
                              <th>Nombre</th>
                              <th>Apellidos</th>
                              <th>email</th>
                              <th></th>
                             <!-- <th></th>  -->
                          </tr>
                      </thead>
                     
                      <tbody>
                          <tr ng-repeat="usuario in listadoUsuarios">
                              <td><span ng-bind="usuario.login"></span></td>
                              <td><span ng-bind="usuario.nombre"></span></td>
                              <td><span ng-bind="usuario.apellidos"></span></td>
                              <td><span ng-bind="usuario.email"></span></td>
                              <td>
                              <button type="button" ng-click="gestion_usuario_ctrl.editar_usuario(usuario.id)" class="btn btn-warning custom-width">Editar</button>
                              </td>
                              <!-- <td><button type="button" ng-click="gestion_usuario_ctrl.borrar_usuario(usuario.id)" class="btn btn-danger custom-width">Borrar</button>
                              </td>  -->
                          </tr>
                      </tbody>
                  </table>
                  <div class="padding-button-custom">
                  	<button type="button" ng-click="gestion_usuario_ctrl.crear_usuario()" class="btn btn-success custom-width">Nuevo</button>
	              </div>
	              
	              </div>
	          </div>

		</div>
	</div>
	
</div>