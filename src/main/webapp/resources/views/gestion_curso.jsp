<div class="container">
    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
       <div id="pillsContainer">
				<ul class="nav nav-pills" role="tablist">
					<li class="active"><a href="#/main_view_profesor">Inicio</a></li>
					<li class="active"><a href="#/" onclick="return confirm('Volver al inicio?')">Logout</a></li>
				</ul>
			</div>		
	</div>
</div>


<div class="jumbotron">
	<div class="container">
	    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
	    	   
	    	   
	    	   
	    	   

<div ng-cloak>
  <md-content>
    <md-tabs md-dynamic-height md-border-bottom>
      <md-tab label="TEMAS">
        <md-content class="md-padding">

   			<div class="panel-heading"><span class="lead">Listado de temas </span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <tbody>
                          <tr ng-repeat="tema in listadoTemas">
                              <td>Tema <span ng-bind="tema.secuencia"></span></td>
                              <td><span ng-bind="tema.titulo"></span></td>
                              <td>
                              <button type="button" ng-click="gestion_curso_ctrl.editar_tema(tema.id)" class="btn btn-warning custom-width">Editar</button>
                              <button type="button" ng-click="gestion_curso_ctrl.borrar_tema(tema.id)" class="btn btn-danger custom-width">Borrar</button>
                              </td>
                          </tr>
                      </tbody>
                  </table>
                  <button type="button" ng-click="gestion_curso_ctrl.crear_tema()" class="btn btn-success custom-width">Nuevo tema</button>
	          </div>
          
        </md-content>
      </md-tab>
      <md-tab label="ALUMNOS">
        <md-content class="md-padding">

   			<div class="panel-heading"><span class="lead">Evaluación de alumnos</span></div>
              <div class="tablecontainer">
                  <table class="table table-hover" style="width:100%;min-width:300px">
                      <thead>
                          <tr>
                              <th width="50%">Login</th>
                              <th width="40%">Email</th>
                              <th width="10%"></th>
                          </tr>
                      </thead>
                     
                      <tbody>
                          <tr ng-repeat="alumno in listadoAlumnos">
                              <td><span ng-bind="alumno.login"></span></td>
                              <td><span ng-bind="alumno.email"></span></td>
                              <td><button type="button" ng-click="gestion_curso_ctrl.ejerciciosAlumno(alumno.id)" class="btn btn-success custom-width">Ver</button></td>
                          </tr>
                      </tbody>
                  </table>
	            </div>

        </md-content>
      </md-tab>
    </md-tabs>
  </md-content>
</div>

	    	   
	    	   
	    	   
	    	   
		</div>
    </div>
</div>
