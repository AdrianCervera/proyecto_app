<div class="generic-container">
		
    <div class="container">
        <div class="generic-container col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
        
	        <div id="pillsContainer">
				<ul class="nav nav-pills" role="tablist">
					<li class="active"><a href="#/main_view_admin">Inicio</a></li>
					<li class="active"><a href="#/" onclick="return confirm('Volver al inicio?')">Logout</a></li>
				</ul>
			</div>			
			<div style="height:4em;"></div>

			<div class="jumbotron">
				<h2>Nuevo usuario</h2>
				<p class="lead">Selecciona el tipo de usuario</p>
			</div>
		
		    <div class="panel panel-default">
		  <!--  <div class="panel-heading"><span class="lead">Identificación</span></div>    -->
				<div class="list-group">
				  <a href="#/crear_alumno" class="list-group-item">Alumno</a>
				  <a href="#/crear_profesor" class="list-group-item">Profesor</a>
				  <a href="#/crear_admin" class="list-group-item">Administrador</a>
				</div>
		
		    </div>
		</div>
	</div>
	
</div>
