<div class="container">
    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
       <div id="pillsContainer">
			<ul class="nav nav-pills" role="tablist">
				<li class="active"><a href="#/" onclick="return confirm('Volver al inicio?')">Inicio</a></li>
				<li><a href="#/modificar_curso" >Cursos</a></li>
				<li><a href="#/evaluar_alumno" >Evaluaci�n</a></li>
				<li><a href="#/consultar_metricas">M�tricas de alumnos</a></li>
			</ul>
		</div>		
	</div>
</div>

<div class="jumbotron">
	<div class="container">
	    <div class="panel panel-default">
	        <div class="panel-heading"><span class="lead">Nuevo Examen</span></div>
	        <div class="formcontainer">
	            <form ng-submit="examen_ctrl.submit()" name="examenForm" class="form-horizontal">
					<div class="container">
	 
		                <div class="row">
		                    <div class="form-group col-md-12">
		                        <label class="col-md-2 control-lable" for="tema_examen">Tema</label>
		                        <div class="col-md-7">
		                            <input type="text" ng-model="examen_ctrl.examen.tema_id" id="tema_examen" class="form-control input-sm" placeholder="Tema al que pertenece" required/>
		                        </div>
		                    </div>
		                </div>
		                
		                <div class="row">
		                    <div class="form-group col-md-12">
		                        <label class="col-md-2 control-lable" for="contenido_examen">Contenido</label>
								<div text-angular ng-model="examen_ctrl.examen.contenido" id="contenido_examen"></div>
		                    </div>
		                </div>
		
		                <div class="padding-button-custom">
		                    <input type="submit"  value="Crear" class="btn btn-primary btn-sm" ng-disabled="examenForm.$invalid">
		                </div>
		                
		            </div>
	            </form>
	        </div>
	    </div>
    </div>
</div>