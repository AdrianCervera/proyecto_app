<div class="container">
    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
       <div id="pillsContainer">
			<ul class="nav nav-pills" role="tablist">
				<li class="active"><a href="#/gestion_curso">Volver</a></li>
				<li class="active"><a href="#/" onclick="return confirm('Volver al inicio?')">Logout</a></li>
			</ul>
		</div>		
	</div>
</div>

<div class="jumbotron">
	<div class="container">
	    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
	    	    	    	
			<div class="alert alert-danger" ng-show="tema_ctrl.error">
			    <p>Error al crear tema: {{mensaje_error}}</p>
			</div>
			<div class="alert alert-success" ng-show="tema_ctrl.success">
			    <p>Tema creado con �xito.</p>
			</div>
	    
		    <div class="panel panel-default">
		        <div class="panel-heading"><span class="lead">Nuevo Tema</span></div>
		        <div class="formcontainer">
		            <form ng-submit="tema_ctrl.submit()" name="temaForm" class="form-horizontal">
						<div class="container">
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="titulo_tema">T�tulo</label>
			                        <div class="col-md-7">
			                            <input type="text" ng-model="tema_ctrl.tema.titulo" id="titulo_tema" class="username form-control input-sm" placeholder="T�tulo" required ng-minlength="3"/>
			                        </div>
			                    </div>
			                </div>
			                
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="secuencia_tema">N�mero</label>
			                        <div class="col-md-7">
			                            <input type="number" ng-model="tema_ctrl.tema.secuencia" id="secuencia_tema" class="form-control input-sm" title="N�mero de tema del curso" min="1" required/>
			                        </div>
			                    </div>
			                </div>
						  
						
			                <div class="padding-button-custom">
			                    <input type="submit"  value="Crear" class="btn btn-primary btn-sm" ng-disabled="temaForm.$invalid">
			                </div>
			            </div>
			            
		            </form>
		        </div>
		    </div>
		</div>
    </div>
</div>
