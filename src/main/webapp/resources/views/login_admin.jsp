<div class="container">
    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
       <div id="pillsContainer">
			<ul class="nav nav-pills pull-right" role="tablist">
				<li class="active"><a href="#/">Inicio</a></li>
			</ul>
		</div>		
	</div>
</div>

<div class="jumbotron">
	<div class="container">
	    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">

			<div class="alert alert-danger" ng-show="login_ctrl.error">
			    <p>Error de acceso. {{mensaje_error}}</p>
			</div>
			    <div class="panel panel-default">
			        <div class="panel-heading"><span class="lead">Administrador</span></div>
			        <div class="formcontainer">
			            <form ng-submit="login_ctrl.submit()" name="loginForm" class="form-horizontal">
							<div class="container">
				                <div class="row">
				                    <div class="form-group col-md-12">
				                        <div class="col-md-7">
				                            <input type="hidden" ng-model="login_ctrl.usuario.rol" ng-init="login_ctrl.usuario.rol='admin'" id="usuarioRol" class="form-control input-sm" readonly required/>
				                        </div>
				                    </div>
				                </div>
				                <div class="row">
				                    <div class="form-group col-md-12">
				                        <label class="col-md-2 control-lable" for="usuarioLogin">Login</label>
				                        <div class="col-md-7">
				                            <input type="text" ng-model="login_ctrl.usuario.login" id="usuarioLogin" class="username form-control input-sm" placeholder="Login" required ng-minlength="3"/>
				                        </div>
				                    </div>
				                </div>
				                  
				                <div class="row">
				                    <div class="form-group col-md-12">
				                        <label class="col-md-2 control-lable" for="usuarioPassword">Password</label>
				                        <div class="col-md-7">
				                            <input type="password" ng-model="login_ctrl.usuario.password" id="usuarioPassword" class="form-control input-sm" placeholder="Contraseņa" required ng-minlength="3"/>
				                        </div>
				                    </div>
				                </div>
				
				                <div class="padding-button-custom">
				                    <input type="submit" value="Login" class="btn btn-primary btn-sm" ng-disabled="loginForm.$invalid">
				                </div>
				            </div>
			            </form>
			        </div>
			    </div>
			</div>
    	</div>
    </div>
</div>
