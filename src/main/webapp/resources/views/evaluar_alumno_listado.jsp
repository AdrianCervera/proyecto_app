<div class="container">
    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
       <div id="pillsContainer">
			<ul class="nav nav-pills" role="tablist">
				<li class="active"><a href="#/gestion_curso">Volver</a></li>
				<li class="active"><a href="#/" onclick="return confirm('Volver al inicio?')">Logout</a></li>
			</ul>
		</div>		
	</div>
</div>

<div class="jumbotron">
	<div class="container">
	    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
	    
	    	 <!-- selector tema -->
	    	<uib-accordion close-others="true">
				
			    <div uib-accordion-group ng-repeat="tema in listadoTemas" class="panel-default" is-open="status.open" width="100%">
			        <uib-accordion-heading>
		       			<a href="javascript:void(0)" ng-click="evaluacion_ctrl.cargarEjerciciosTema(tema.id)"><span style="font-size:14px">Tema {{tema.secuencia}}. {{tema.titulo}}</span></a>
			            <i class="pull-right glyphicon" ng-class="{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}"></i>
			        </uib-accordion-heading>
			        
					 <div class="panel-heading"><span class="lead">Listado de ejercicios</span></div>
		             <div class="tablecontainer">
		                 <table class="table table-hover">
		                     <tbody>
		                         <tr ng-repeat="ejercicio in listadoEjercicios">
		                             <td>Ejercicio <span ng-bind="ejercicio.secuencia"></span></td>
		                             <td><span ng-bind="ejercicio.pregunta"></span></td>
		                             <td><span ng-show="ejercicio.calificado" class="glyphicon glyphicon-ok-sign" style="color:green" title="Evaluado"></span></td>
		                             <td><span ng-show="ejercicio.respondido" class="glyphicon glyphicon-pencil" style="color:red" title="Falta evaluar"></span></td>
		                             <td>
		                             <button type="button" ng-click="evaluacion_ctrl.evaluarEjercicio(ejercicio.id)" class="btn btn-success custom-width">Evaluar</button>
		                             </td>
		                         </tr>
		                     </tbody>
		                 </table>
		             </div>
		
					 <div class="panel-heading"><span class="lead">Listado de ejercicios de examen</span></div>
		             <div class="tablecontainer">
		                 <table class="table table-hover">
		                     <tbody>
		                         <tr ng-repeat="ejercicioExamen in listadoEjerciciosExamen">
		                             <td>Ejercicio <span ng-bind="ejercicioExamen.secuencia"></span></td>
		                             <td><span ng-bind="ejercicioExamen.pregunta"></span></td>
		                             <td><span ng-show="ejercicioExamen.calificado" class="glyphicon glyphicon-ok-sign" style="color:green"></span></td>
		                             <td><span ng-show="ejercicioExamen.respondido" class="glyphicon glyphicon-pencil" style="color:red"></span></td>
		                             <td>
		                             <button type="button" ng-click="evaluacion_ctrl.evaluarEjercicio(ejercicioExamen.id)" class="btn btn-success custom-width">Evaluar</button>
		                             </td>
		                         </tr>
		                     </tbody>
		                 </table>
		             </div>
			      
			    </div>
	    
	    </uib-accordion>
	    	
	    	
	    	
<!--
			 <div class="panel-heading"><span class="lead">Listado de ejercicios</span></div>
             <div class="tablecontainer">
                 <table class="table table-hover">
                     <tbody>
                         <tr ng-repeat="ejercicio in listadoEjercicios">
                             <td><span ng-bind="ejercicio.secuencia"></span></td>
                             <td><span ng-bind="ejercicio.pregunta"></span></td>
                             <td><span class="glyphicons glyphicons-pen" style="color:green"></span></td>
                             <td><span class="glyphicons glyphicons-pen" style="color:red"></span></td>
                             <td>
                             <button type="button" ng-click="evaluar_ctrl.evaluarEjercicio(ejercicio.id)" class="btn btn-success custom-width">Evaluar</button>
                             </td>
                         </tr>
                     </tbody>
                 </table>
             </div>

			 <div class="panel-heading"><span class="lead">Listado de ejercicios de examen</span></div>
             <div class="tablecontainer">
                 <table class="table table-hover">
                     <tbody>
                         <tr ng-repeat="ejercicioExamen in listadoEjerciciosExamen">
                             <td>Ejercicio <span ng-bind="ejercicioExamen.secuencia"></span></td>
                             <td><span ng-bind="ejercicioExamen.pregunta"></span></td>
                             <td>
                             <button type="button" ng-click="evaluar_ctrl.evaluar_ejercicio(ejercicioExamen.id)" class="btn btn-success custom-width">Evaluar</button>
                             </td>
                         </tr>
                     </tbody>
                 </table>
             </div>
  -->
		</div>
    </div>
</div>
