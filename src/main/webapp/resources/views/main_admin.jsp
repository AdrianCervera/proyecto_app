<div class="generic-container">
		
    <div class="container">
        <div class="generic-container col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
        
	        <div id="pillsContainer">
				<ul class="nav nav-pills" role="tablist">
					<li class="active"><a href="#/" onclick="return confirm('Cerrar la sesi�n?')">Logout</a></li>
				</ul>
			</div>		
			<div style="height:4em;"></div>

			<div class="jumbotron">
				<h2>Bienvenido al panel de administraci�n</h2>
				<p class="lead">Selecciona el tipo de gesti�n</p>
			</div>
			
			
			  <!-- opciones de administrador -->
		    <div class="panel panel-default">
		    	
				<div ng-cloak>
				  <md-content>
				    <md-tabs md-dynamic-height md-border-bottom>
				      
				      <md-tab label="CURSOS">
				        <md-content class="md-padding">
						    <div class="panel panel-default">
								<div class="list-group">
								  <a href="#/gestion_cursos" class="list-group-item">Gesti�n de cursos</a>
								</div>
							</div>
				        </md-content>
				      </md-tab>
				      
				      <md-tab label="USUARIOS">
				        <md-content class="md-padding">
						    <div class="panel panel-default">
								<div class="list-group">
								  <a href="#/gestion_alumnos" class="list-group-item">Gesti�n de alumnos</a>
								  <a href="#/gestion_profesores" class="list-group-item">Gesti�n de profesores</a>
								  <a href="#/gestion_administradores" class="list-group-item">Gesti�n de administradores</a>
								</div>
							</div>
				        </md-content>
				      </md-tab>
				      
				    </md-tabs>
				  </md-content>
				</div>
		    	
		    </div>
		</div>
	</div>
	
</div>