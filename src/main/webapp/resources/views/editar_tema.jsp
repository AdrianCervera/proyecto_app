<div class="container">
    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
       <div id="pillsContainer">
				<ul class="nav nav-pills" role="tablist">
					<li class="active"><a href="#/gestion_curso">Volver</a></li>
					<li class="active"><a href="#/" onclick="return confirm('Volver al inicio?')">Logout</a></li>
				</ul>
			</div>		
	</div>
</div>

<div class="jumbotron">
	<div class="container">
	    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
	    	    	    	
			<div class="alert alert-danger" ng-show="editar_tema_ctrl.error_tema">
			    <p>Error al guardar. {{mensaje_error}}</p>
			</div>
			<div class="alert alert-success" ng-show="editar_tema_ctrl.success_tema">
			    <p>Guardado con �xito.</p>
			</div>
	    	    	    	
			<div class="alert alert-danger" ng-show="editar_tema_ctrl.error_teoria">
			    <p>Error al guardar el contenido. {{mensaje_error}}</p>
			</div>
			<div class="alert alert-success" ng-show="editar_tema_ctrl.success_teoria">
			    <p>Contenido te�rico guardado con �xito.</p>
			</div>
	    
		    <div class="panel panel-default">
		        <div class="panel-heading"><span class="lead">Editar Tema</span></div>
		        <div class="formcontainer">
		            <form ng-submit="editar_tema_ctrl.editar_tema()" name="temaForm" class="form-horizontal">
		            	<div class="container">
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="titulo_tema">T�tulo</label>
			                        <div class="col-md-7">
			                            <input type="text" ng-model="editar_tema_ctrl.tema.titulo" id="titulo_tema" class="username form-control input-sm" placeholder="T�tulo" required ng-minlength="3"/>
			                        </div>
			                    </div>
			                </div>
			                
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="secuencia_tema">N�mero</label>
			                        <div class="col-md-7">
			                            <input type="number" ng-model="editar_tema_ctrl.tema.secuencia" id="secuencia_tema" class="form-control input-sm" title="N�mero de tema del curso" min="1" required/>
			                        </div>
			                    </div>
			                </div>
						  
			                <div class="padding-button-custom">
			                    <input type="submit"  value="Cambiar" class="btn btn-primary btn-sm" ng-disabled="temaForm.$invalid">
			                </div>
			            </div>
		            
		            </form>
		        </div>
		    </div>
		    
		    <!-- tabla de contenidos del tema -->
		    <div class="panel panel-default">
		    	
				<div ng-cloak>
				  <md-content>
				    <md-tabs md-dynamic-height md-border-bottom>
				    
				      <md-tab label="TEOR�A">
				        <md-content class="md-padding">
							<div text-angular ng-model="teoria.contenido"></div>
				            <button type="button" ng-click="editar_tema_ctrl.editar_teoria()" class="btn btn-warning custom-width">Guardar cambios</button>
				        </md-content>
				      </md-tab>
				      
				      <md-tab label="EJERCICIOS">
				        <md-content class="md-padding">
				
				   			  <div class="panel-heading"><span class="lead">Listado de ejercicios del tema</span></div>
				              <div class="tablecontainer">
				                  <table class="table table-hover">
				                      <tbody>
				                          <tr ng-repeat="ejercicio in listadoEjercicios">
				                              <td>Ejercicio <span ng-bind="ejercicio.secuencia"></span></td>
				                              <td><span ng-bind="ejercicio.pregunta"></span></td>
				                             <!-- <td><span ng-bind="ejercicio.respuesta"></span></td> -->
				                              <td>
				                              <button type="button" ng-click="editar_tema_ctrl.editar_ejercicio(ejercicio.id)" class="btn btn-warning custom-width">Editar</button>
				                              <button type="button" ng-click="editar_tema_ctrl.borrar_ejercicio(ejercicio.id)" class="btn btn-danger custom-width">Borrar</button>
				                              </td>
				                          </tr>
				                      </tbody>
				                  </table>
				                  <button type="button" ng-click="editar_tema_ctrl.crear_ejercicio()" class="btn btn-success custom-width">Nuevo ejercicio</button>
					          </div>
				          
				        </md-content>
				      </md-tab>
				      
				      <md-tab label="EXAMEN">
				        <md-content class="md-padding">
				
				   			  <div class="panel-heading"><span class="lead">Ejercicios del examen</span></div>
				              <div class="tablecontainer">
				                  <table class="table table-hover">
				                      <tbody>
				                          <tr ng-repeat="ejercicioExamen in listadoEjerciciosExamen">
				                              <td>Ejercicio <span ng-bind="ejercicioExamen.secuencia"></span></td>
				                              <td><span ng-bind="ejercicioExamen.pregunta"></span></td>
				                              <td>
				                              <button type="button" ng-click="editar_tema_ctrl.editar_ejercicio(ejercicioExamen.id)" class="btn btn-warning custom-width">Editar</button>
				                              <button type="button" ng-click="editar_tema_ctrl.borrar_ejercicio(ejercicioExamen.id)" class="btn btn-danger custom-width">Borrar</button>
				                              </td>
				                          </tr>
				                      </tbody>
				                  </table>
				                  <button type="button" ng-click="editar_tema_ctrl.crear_ejercicio()" class="btn btn-success custom-width">Nuevo ejercicio</button>
					              </div>
				          
				        </md-content>
				      </md-tab>
				      
				    </md-tabs>
				  </md-content>
				</div>
		    	
		    	
		    </div>
		    
		</div>
    </div>
</div>
