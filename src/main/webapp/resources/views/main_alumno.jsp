 <div id="mainViewContainer" class="defMainViewContainer">
 
    <div id="pillsContainer">
		<ul class="nav nav-pills pull-right" role="tablist">
			<li>
				 <!-- selection cursos inscritos -->
				 <div class="dropdown">
					  <button class="btn btn-warning dropdown-toggle" type="button" data-toggle="dropdown">Cursos
					  <span class="caret"></span></button>
					  <ul class="dropdown-menu">
							<li ng-repeat="curso in listadoDeCursos">
			    				<a href="javascript:void(0)" ng-click="cargar_listado_temas(curso.id)">{{curso.nombre}}</a>
			       				<!--
			       				 <ul>
			            			<li ng-if="curso.id">
			                			{{ curso.nombre }}
			           				</li>
			        			</ul>  
			        			-->	
			    			</li>
				      </ul>
				  </div> 
			</li>
			<li class="active"><a href="#/" onclick="return confirm('Volver al inicio?')">Logout</a></li>
		</ul>
	</div>		
 
 	 <!-- MENU LATERAL  -->
	 <div id="mySidenav" class="sidenav">
	 
	 	 <!-- ocultar menu lateral -->
		  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
		  

		  
		  <!--
		  <li ng-repeat="tema in listadoDeTemas">
		  	<button ng-submit="main_view_ctrl.cargar_contenido({{tema.id}})">{{tema.titulo}}</button>
		  </li>
		  	-->		  	
		  	 <!--
		  	<ul>
    			<li ng-repeat="tema in listadoDeTemas">
    				<a href="javascript:void(0)" ng-click="cargar_contenido(tema.id)">{{tema.titulo}}</a> 
    				
       				<ul>
            			<li ng-if="teoria.id">
                			{{ teoria.titulo }}
           				</li>
        			</ul>	
    			</li>
			</ul>-->
			
		<!-- DESPLEGABLE TEMAS DE UN CURSO -->
		<uib-accordion close-others="true">
				
		    <div uib-accordion-group ng-repeat="tema in listadoDeTemas" class="panel-default" is-open="status.open" width="100%">
		        <uib-accordion-heading>
		            <span style="font-size:14px">{{tema.titulo}}</span>
		            <i class="pull-right glyphicon" ng-class="{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}"></i>
		        </uib-accordion-heading>
		        <a href="javascript:void(0)" ng-click="cargar_teoria(tema.id)"><span style="font-size:14px">Teor�a</span></a>
		        <!-- <a href="javascript:void(0)" ng-click="cargar_examen(tema.id)"><span style="font-size:14px">Examen</span></a>  -->
		      
		        <!-- desplegable ejercicios de un tema -->
		        <a href="javascript:void(0)" ng-click="cargar_ejercicios(tema.id, false)" data-toggle="collapse" data-target="#listado_ejercicios-{{$index}}"><span style="font-size:14px">Ejercicios</span></a>
		        <div id="listado_ejercicios-{{$index}}" class="collapse">
		        	<ul>
						<li ng-repeat="ejercicio in main_view_ctrl.listadoDeEjercicios">
			    			<a href="javascript:void(0)" ng-click="mostrar_ejercicio(ejercicio.id)"><span style="font-size:12px; color:black;">Ejercicio {{ejercicio.secuencia}}</span></a>	
			    		</li>
				    </ul>
				</div>
		      
		        <!-- desplegable ejercicios de examen de un tema -->
		        <a href="javascript:void(0)" ng-click="cargar_ejercicios(tema.id, true)" data-toggle="collapse" data-target="#listado_ejercicios_examen-{{$index}}"><span style="font-size:14px">Examen</span></a>
		        <div id="listado_ejercicios_examen-{{$index}}" class="collapse">
		        	<ul>
						<li ng-repeat="ejercicioExamen in main_view_ctrl.listadoDeEjerciciosExamen">
			    			<a href="javascript:void(0)" ng-click="mostrar_ejercicio(ejercicioExamen.id)"><span style="font-size:12px; color:black;">Ejercicio {{ejercicioExamen.secuencia}}</span></a>	
			    		</li>
				    </ul>
				</div>
		        
		        <!--
		        <a href="javascript:void(0)" ng-click="cargar_ejercicios_examen(tema.id)" data-toggle="collapse" data-target="#listado_ejercicios_examen"><span style="font-size:14px">Examen</span></a>
		        <div id="listado_ejercicios_examen" class="collapse">
		        	<p>cccc</p>
		        	<p>dddd</p>
		        	<ul>
						<li ng-repeat="ejercicioExamen in listadoDeEjerciciosExamen">
			    			<a href="javascript:void(0)" ng-click="cargar_ejercicio(ejercicioExamen.id)">Ejercicio {{ejercicio.secuencia}}</a>	
			    		</li>
				    </ul>
				</div>		        
		          -->
		          
		        <!--
  				  <a data-toggle="collapse" data-target="#evaluacion_links">Evaluaci�n</a>
				  <div id="evaluacion_links" class="collapse">
				        <a href="javascript:void(0)" ng-click="cargar_ejercicios(tema.id)"><span style="font-size:14px">Ejercicios</span></a>
				        <a href="javascript:void(0)" ng-click="cargar_examen(tema.id)"><span style="font-size:14px">Examen</span></a>
				  </div>
		         -->
		      
		    </div>
	    
	    </uib-accordion>
  
	</div>
	
	<!-- SWITCH MENU -->
	<span id="openNavSpan" style="font-size:48px;cursor:pointer;color:green" onclick="openNav()" class="fa fa-angle-double-left hide-opener"></span>


	<div style="width:80%">

	<!-- CONTENIDO TEORIA -->
	<div id="mainView" style="min-width:360px;"> <!-- style="border: 1px solid red;"  -->
	    <div class="col-xs-10 col-xs-offset-2 col-sm-10 col-sm-offset-2 col-md-10 col-md-offset-2 col-lg-10 col-lg-offset-2">
			<p ng-bind-html="contenido.contenido"></p>
		</div>  
	</div>
	
	<!-- CONTENIDO EJERCICIO -->
	<div id="mainViewEj" style="min-width:360px;" ng-hide="main_view_ctrl.ocultar_ejercicio"> <!-- style="border: 1px solid blue;"  -->
	    <div class="col-xs-10 col-xs-offset-2 col-sm-10 col-sm-offset-2 col-md-10 col-md-offset-2 col-lg-10 col-lg-offset-2">
	    		
			<div class="formcontainer">
	            <form ng-submit="enviar_evaluacion(ejercicio.id)" name="evalForm" class="form-horizontal">
					<div class="container">
						<div style="padding-bottom:4em;color:#152692"><h2>Ejercicio {{ejercicio.secuencia}}</h2></div>
		                <div class="row">
		                    <div class="form-group col-md-12">
		                        <div class="col-md-7">
		                            <textarea ng-model="ejercicio.pregunta" rows="4" class="form-control" readonly></textarea>
		                        </div>
		                    </div>
		                </div>
					
		                <div class="row">
		                    <div class="form-group col-md-12">
		                        <div class="col-md-7">
		                            <textarea ng-model="evaluacion.respuesta" rows="12" placeholder="Escriba la respuesta..." class="form-control" ng-readonly="main_view_ctrl.mostrar_enviado" required ng-minlength="3"></textarea>
		                        </div>
		                    </div>
		                </div>
           
		                <div id="button_enviar" ng-show="!main_view_ctrl.mostrar_enviado" class="padding-button-custom">
		                    <input type="submit"  value="Enviar" class="btn btn-primary btn-sm" ng-disabled="evalForm.$invalid">
		                </div>
		                
		                <div id="button_enviado" class="padding-button-custom" ng-show="main_view_ctrl.mostrar_enviado">
		                    <button type="button" onclick="alert('La respuesta ya ha sido enviada.');" class="btn btn-success btn-sm">Enviado!</button>
		                	<button type="button" class="btn btn-warning" ng-show="evaluacion.calificacion >= 0" ng-click="mostrar_solucion(ejercicio.id)">Mostrar soluci�n</button>	
		                </div>
		                             
		                <div class="row" id="div_solucion" ng-show="evaluacion.calificacion >= 0">
		                    <div class="form-group col-md-12">
		                        <div class="col-md-7">
			                        <!-- <label class="col-md-2 control-lable" ng-show="main_view_ctrl.mostrar_solucion" for="solucion_ejercicio">Soluci�n</label> -->
		                            <textarea id="solucion_ejercicio" ng-model="ejercicio.respuesta" rows="12" ng-show="main_view_ctrl.mostrar_solucion" class="form-control" readonly></textarea>
		                        </div>
		                    </div>
		                </div>
		                
		                
		                <div class="row" ng-show="evaluacion.calificacion >= 0">
		                    <div class="form-group col-md-12">
		                        <div class="col-md-7">
			                        <label class="col-md-2 control-lable" for="calificacion_ejercicio">Calificaci�n</label>
		                            <input id="calificacion_ejercicio" type="text" ng-model="evaluacion.calificacion" class="form-control" readonly/>
		                        </div>
		                    </div>
		                </div>
		                
		            </div>
		            
	            </form>
	        </div>
		
		</div>  
	</div>	
	
	</div>
	
	
</div>