<div class="container">
    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
       <div id="pillsContainer">
			<ul class="nav nav-pills" role="tablist">
				<li class="active"><a href="#/gestion_alumnos">Volver</a></li>
				<li class="active"><a href="#/" onclick="return confirm('Volver al inicio?')">Logout</a></li>			</ul>
		</div>		
	</div>
</div>

<div class="jumbotron">
	<div class="container">
	    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
	    
			<div class="alert alert-danger" ng-show="alumno_ctrl.error">
			    <p>Error al crear alumno: {{mensaje_error}}</p>
			</div>
			<div class="alert alert-success" ng-show="alumno_ctrl.success">
			    <p>Alumno creado con �xito.</p>
			</div>
		    <div class="panel panel-default">
		        <div class="panel-heading"><span class="lead">Nuevo alumno</span></div>
		        <div class="formcontainer">
		            <form ng-submit="alumno_ctrl.submit()" name="alumnoForm" class="form-horizontal">
						<div class="container">
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="login">Login</label>
			                        <div class="col-md-7">
			                            <input type="text" ng-model="alumno_ctrl.alumno.login" id="login" class="form-control input-sm" placeholder="Login" required ng-minlength="3"/>
			                        </div>
			                    </div>
			                </div>
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="password">Password</label>
			                        <div class="col-md-7">
			                            <input type="text" ng-model="alumno_ctrl.alumno.password" id="password" class="form-control input-sm" placeholder="Contrase�a" required ng-minlength="3"/>
			                        </div>
			                    </div>
			                </div>
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="nombre">Nombre</label>
			                        <div class="col-md-7">
			                            <input type="text" ng-model="alumno_ctrl.alumno.nombre" id="nombre" class="form-control input-sm" placeholder="Nombre" required ng-minlength="3"/>
			                        </div>
			                    </div>
			                </div>
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="apellidos">Apellidos</label>
			                        <div class="col-md-7">
			                            <input type="text" ng-model="alumno_ctrl.alumno.apellidos" id="apellidos" class="form-control input-sm" placeholder="Apellidos" required ng-minlength="3"/>
			                        </div>
			                    </div>
			                </div>
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="dni">DNI</label>
			                        <div class="col-md-7">
			                            <input type="text" ng-model="alumno_ctrl.alumno.dni" id="dni" class="form-control input-sm" placeholder="Documento de identificaci�n" required ng-minlength="9"/>
			                        </div>
			                    </div>
			                </div>
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="nombre">Tel�fono</label>
			                        <div class="col-md-7">
			                            <input type="text" ng-model="alumno_ctrl.alumno.telefono" id="telefono" class="form-control input-sm" placeholder="N�mero de tel�fono" required ng-minlength="9" />
			                        </div>
			                    </div>
			                </div>
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="email">Email</label>
			                        <div class="col-md-7">
			                            <input type="text" ng-model="alumno_ctrl.alumno.email" id="email" class="form-control input-sm" placeholder="Email" required ng-minlength="3"/>
			                        </div>
			                    </div>
			                </div>
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="direccion">Direcci�n</label>
			                        <div class="col-md-7">
			                            <input type="text" ng-model="alumno_ctrl.alumno.direccion" id="direccion" class="form-control input-sm" placeholder="Direcci�n del alumno"/>
			                        </div>
			                    </div>
			                </div>
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="ciudad">Ciudad</label>
			                        <div class="col-md-7">
			                            <input type="text" ng-model="alumno_ctrl.alumno.ciudad" id="ciudad" class="form-control input-sm" placeholder="Ciudad de residencia"/>
			                        </div>
			                    </div>
			                </div>
			                
			            	<div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="habilitado">Habilitado</label>
			                        <div class="col-md-7">
			                            <input type="checkbox" ng-model="alumno_ctrl.alumno.habilitado" value="es_examen" id="habilitado" class="form-control" checked/>
			                        </div>
			                    </div>
			                </div>
			                    
			                <div class="padding-button-custom">
			                    <input type="submit"  value="Crear" class="btn btn-primary" ng-disabled="alumnoForm.$invalid">
			                </div>
			            </div> 
		            </form>
		        </div>
		    </div>
	    </div>
    </div>
</div>