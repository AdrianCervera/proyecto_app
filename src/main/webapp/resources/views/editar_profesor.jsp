<div class="container">
    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
       <div id="pillsContainer">
			<ul class="nav nav-pills" role="tablist">
				<li class="active"><a href="#/gestion_profesores">Volver</a></li>
				<li class="active"><a href="#/" onclick="return confirm('Volver al inicio?')">Logout</a></li>
			</ul>
		</div>		
	</div>
</div>

<div class="jumbotron">
	<div class="container">
	    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
	    	
			<div class="alert alert-danger" ng-show="profesor_ctrl.error">
			    <p>Error al editar profesor: {{mensaje_error}}</p>
			</div>
			<div class="alert alert-success" ng-show="profesor_ctrl.success">
			    <p>Profesor editado con �xito.</p>
			</div>
		    <div class="panel panel-default">
		        <div class="panel-heading"><span class="lead">Editar profesor</span></div>
		        <div class="formcontainer">
		            <form ng-submit="profesor_ctrl.actualizarProfesor()" name="profesorForm" class="form-horizontal">
						<div class="container">
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="login">Login</label>
			                        <div class="col-md-7">
			                            <input type="text" ng-model="profesor_ctrl.profesor.login" id="login" class="form-control input-sm" placeholder="Login" required ng-minlength="3"/>
			                        </div>
			                    </div>
			                </div>
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="password">Password</label>
			                        <div class="col-md-7">
			                            <input type="text" ng-model="profesor_ctrl.profesor.password" id="password" class="form-control input-sm" placeholder="Contrase�a" required ng-minlength="3"/>
			                        </div>
			                    </div>
			                </div>
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="nombre">Nombre</label>
			                        <div class="col-md-7">
			                            <input type="text" ng-model="profesor_ctrl.profesor.nombre" id="nombre" class="form-control input-sm" placeholder="Nombre" required ng-minlength="3"/>
			                        </div>
			                    </div>
			                </div>
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="apellidos">Apellidos</label>
			                        <div class="col-md-7">
			                            <input type="text" ng-model="profesor_ctrl.profesor.apellidos" id="apellidos" class="form-control input-sm" placeholder="Apellidos" required ng-minlength="3"/>
			                        </div>
			                    </div>
			                </div>
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="departamento">Departamento</label>
			                        <div class="col-md-7">
			                            <input type="text" ng-model="profesor_ctrl.profesor.departamento" id="departamento" class="form-control input-sm" placeholder="Departamento" required ng-minlength="3"/>
			                        </div>
			                    </div>
			                </div>
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="dni">DNI</label>
			                        <div class="col-md-7">
			                            <input type="text" ng-model="profesor_ctrl.profesor.dni" id="dni" class="form-control input-sm" placeholder="Documento de identificaci�n" required ng-minlength="9"/>
			                        </div>
			                    </div>
			                </div>
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="nombre">Tel�fono</label>
			                        <div class="col-md-7">
			                            <input type="text" ng-model="profesor_ctrl.profesor.telefono" id="telefono" class="form-control input-sm" placeholder="N�mero de tel�fono" required ng-minlength="9" />
			                        </div>
			                    </div>
			                </div>
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="email">Email</label>
			                        <div class="col-md-7">
			                            <input type="text" ng-model="profesor_ctrl.profesor.email" id="email" class="form-control input-sm" placeholder="Email" required ng-minlength="3"/>
			                        </div>
			                    </div>
			                </div>
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="direccion">Direcci�n</label>
			                        <div class="col-md-7">
			                            <input type="text" ng-model="profesor_ctrl.profesor.direccion" id="direccion" class="form-control input-sm" placeholder="Direcci�n del profesor"/>
			                        </div>
			                    </div>
			                </div>
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="ciudad">Ciudad</label>
			                        <div class="col-md-7">
			                            <input type="text" ng-model="profesor_ctrl.profesor.ciudad" id="ciudad" class="form-control input-sm" placeholder="Ciudad de residencia"/>
			                        </div>
			                    </div>
			                </div>
			                <div class="padding-button-custom">
			                    <input type="submit"  value="Guardar" class="btn btn-primary" ng-disabled="profesorForm.$invalid">
			                </div>
			                
			            </div> 
		            </form>
		        </div>
		    </div>
	    </div>
    </div>
</div>
