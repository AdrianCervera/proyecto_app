<div class="generic-container">
		
    <div class="container">
        <div class="generic-container col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
        
	        <div id="pillsContainer">
				<ul class="nav nav-pills" role="tablist">
					<li class="active"><a href="#/main_view_admin">Inicio</a></li>
					<li class="active"><a href="#/" onclick="return confirm('Volver al inicio?')">Logout</a></li>
				</ul>
			</div>		
			<div style="height:4em;"></div>

		    <div class="panel panel-default">
              <div class="panel-heading"><span class="lead">Listado de cursos </span></div>
              <div class="tablecontainer">
                  <table class="table table-hover" style="width:100%;min-width:300px">
                      <thead>
                          <tr>
                              <th width="10%">ID.</th>
                              <th width="70%">Nombre</th>
                              <th width="10%"></th>
                              <th width="10%"></th>
                          </tr>
                      </thead>
                     
                      <tbody>
                          <tr ng-repeat="curso in listadoCursos">
                              <td><span ng-bind="curso.id"></span></td>
                              <td><span ng-bind="curso.nombre"></span></td>
                              <td>
                              <button type="button" ng-click="gestion_cursos_ctrl.editar_curso(curso.id)" class="btn btn-warning custom-width">Editar</button>
                              </td><td><button type="button" ng-click="gestion_cursos_ctrl.borrar_curso(curso.id)" class="btn btn-danger custom-width">Borrar</button>
                              </td>
                          </tr>
                      </tbody>
                  </table>
                  <div class="padding-button-custom">
                  	  <button type="button" ng-click="gestion_cursos_ctrl.crear_curso()" class="btn btn-success custom-width">Nuevo curso</button>
	              </div>
	              
	          </div>
	        </div>

		</div>
	</div>
	
</div>