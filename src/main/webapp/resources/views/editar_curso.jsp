<div class="container">
    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
       <div id="pillsContainer">
				<ul class="nav nav-pills" role="tablist">
					<li class="active"><a href="#/gestion_cursos">Volver</a></li>
					<li class="active"><a href="#/" onclick="return confirm('Volver al inicio?')">Logout</a></li>
				</ul>
			</div>		
	</div>
</div>

<div class="jumbotron">
	<div class="container">
	    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
	    	    	
			<div class="alert alert-danger" ng-show="curso_ctrl.error">
			    <p>Error al editar curso.</p>
			</div>
			<div class="alert alert-success" ng-show="curso_ctrl.success">
			    <p>Curso editado con �xito.</p>
			</div>
	    
		    <div class="panel panel-default">
		        <div class="panel-heading"><span class="lead">Editar Curso</span></div>
		        <div class="formcontainer">
		            <form ng-submit="editar_curso_ctrl.editarCurso()" name="cursoForm" class="form-horizontal">
						<div class="container">
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="nombre_curso">T�tulo</label>
			                        <div class="col-md-7">
			                            <input type="text" ng-model="editar_curso_ctrl.curso.nombre" id="nombre_curso" class="username form-control input-sm" placeholder="T�tulo" required ng-minlength="3"/>
			                        </div>
			                    </div>
			                </div>
			                            
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="profesor_curso">Responsable</label>
			                        <div class="col-md-7">
			                        <select id="profesor_curso" ng-model="editar_curso_ctrl.curso.profesor_id" class="form-control input-sm" required>
									    <option value="">Profesor responsable del curso</option> <!-- opcion sin valor -->
									    <option ng-repeat="profesor in listadoDeProfesores" value="{{profesor.id}}">{{profesor.nombre}} {{profesor.apellidos}}</option>
									</select>
									</div>
			                    </div>
			                </div>
			
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="fecha_inicio">Fecha de inicio</label>
			                        <div class="input-group date col-md-7" id="datetimePickerInicio">
			                            <input type="text" ng-model="editar_curso_ctrl.curso.fecha_inicio" id="fecha_inicio" class="form-control" placeholder="Fecha de inicio" required/>
			                            <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
			                        </div>
			                    </div>
							    <script type="text/javascript">
							        $(function() {              
							           // Bootstrap DateTimePicker
							           $('#datetimePickerInicio').datetimepicker({
							                 format: 'DD/MM/YYYY'
							           });
							        });      
							    </script>
			                </div>
			                
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="fecha_fin">Fecha de fin</label>
			                        <div class="input-group date col-md-7" id="datetimePickerFin">
			                            <input type="text" ng-model="editar_curso_ctrl.curso.fecha_fin" id="fecha_inicio" class="form-control" placeholder="Fecha de fin"/>
			                            <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
			                        </div>
			                    </div>
							    <script type="text/javascript">
							        $(function() {              
							           // Bootstrap DateTimePicker
							           $('#datetimePickerFin').datetimepicker({
							                 format: 'DD/MM/YYYY'
							           });
							        });      
							    </script>
			                </div>
			                
			                <!-- 
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="fecha_fin">Fecha de fin</label>
			                        <div class="col-md-7">
			                            <input type="text" ng-model="curso_ctrl.curso.fecha_fin" id="fecha_fin" class="username form-control input-sm" placeholder="T�tulo"/>
			                        </div>
			                    </div>
			                </div>
			                -->
			                
		                    <div class="padding-button-custom">
		                        <input type="submit"  value="Guardar" class="btn btn-primary btn-sm" ng-disabled="cursoForm.$invalid">
		                    </div>
			            </div>
			            
		            </form>
		        </div>
		    </div>
		        
		    <div class="panel panel-default">
		      <!-- LISTADO ALUMNOS -->
		      <div class="panel-heading"><span class="lead">Alumnos inscritos</span></div>
              <div class="tablecontainer">
                  <table class="table table-hover" style="width:100%;min-width:300px">
                      <thead>
                          <tr>
                              <th width="10%">ID.</th>
                              <th width="80%">Login</th>
                              <th width="10%"></th>
                          </tr>
                      </thead>
                     
                      <tbody>
                          <tr ng-repeat="alumno in listadoAlumnos">
                              <td><span ng-bind="alumno.id"></span></td>
                              <td><span ng-bind="alumno.login"></span></td>
                              <td><button type="button" ng-click="editar_curso_ctrl.desinscribir_alumno(alumno.id)" class="btn btn-warning custom-width">Borrar</button></td>
                          </tr>
                      </tbody>
                  </table>
                <!-- <button type="button" ng-click="editar_curso_ctrl.suscribir_alumno()" class="btn btn-success custom-width">Suscribir alumno</button>  -->
	            </div>
		        
		    </div>
		</div>
    </div>
</div>
