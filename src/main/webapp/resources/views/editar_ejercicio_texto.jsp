<div class="container">
    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
       <div id="pillsContainer">
				<ul class="nav nav-pills" role="tablist">
					<li class="active"><a href="#/gestion_curso">Volver</a></li>
					<li class="active"><a href="#/" onclick="return confirm('Volver al inicio?')">Logout</a></li>
				</ul>
			</div>		
	</div>
</div>

<div class="jumbotron">
	<div class="container">
	    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
	    	    	    	
			<div class="alert alert-danger" ng-show="ejercicio_ctrl.error">
			    <p>Error al guardar.</p>
			</div>

		    <div class="panel panel-default">
		        <div class="panel-heading"><span class="lead">Editar Ejercicio</span></div>
		        <div class="formcontainer">
		            <form ng-submit="ejercicio_ctrl.actualizarEjercicio()" name="ejercicioForm" class="form-horizontal">
						<div class="container">
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="pregunta_ejercicio">Pregunta</label>
			                        <div class="col-md-7">     <!--  TODO TEXTAREA OCULTA EL RESTO DE CAMPOS ?? -->
			                            <input type="text" ng-model="ejercicio.pregunta" id="pregunta_ejercicio" class="form-control" placeholder="Escriba la pregunta..." required ng-minlength="3"/>
			                        </div>
			                    </div>
			                </div>
						
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="respuesta_ejercicio">Respuesta</label>
			                        <div class="col-md-7">
			                    		<textarea ng-model="ejercicio.respuesta" id="respuesta_ejercicio" rows="6" placeholder="Escriba la respuesta..." class="form-control" required ng-minlength="3"></textarea>
			                        </div>
			                    </div>
			                </div>
			                
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="secuencia_ejercicio">N�mero</label>
			                        <div class="col-md-7">
			                            <input type="number" ng-model="ejercicio.secuencia" id="secuencia_ejercicio" class="form-control input-sm" title="N�mero de ejercicio del tema" min="1" required/>
			                        </div>
			                    </div>
			                </div>
            
			                <div class="row">
			                    <div class="form-group col-md-12">
			                        <label class="col-md-2 control-lable" for="tema_ejercicio">Es un ejercicio de examen</label>
			                        <div class="col-md-7">
			                            <input type="checkbox" ng-model="ejercicio.examen" value="es_examen" id="ejercicio_examen" class="form-control"/>
			                        </div>
			                    </div>
			                </div>
			                <div class="padding-button-custom">
			                    <input type="submit" value="Guardar" class="btn btn-primary btn-sm" ng-disabled="ejercicioForm.$invalid">
			                </div>
			            </div>
			            
		            </form>
		        </div>
		    </div>
		</div>
    </div>
</div>
