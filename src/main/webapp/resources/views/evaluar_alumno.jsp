<div class="container">
    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
       <div id="pillsContainer">
			<ul class="nav nav-pills" role="tablist">
				<li class="active"><a href="#/gestion_curso">Volver</a></li>
				<li class="active"><a href="#/" onclick="return confirm('Volver al inicio?')">Logout</a></li>
			</ul>
		</div>		
	</div>
</div>

<div class="jumbotron">
	    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
	    	    	    	
			<div class="alert alert-danger" ng-show="evaluacion_ctrl.error">
			    <p>Error al guardar: {{mensaje_error}}</p>
			</div>
			<div class="alert alert-success" ng-show="evaluacion_ctrl.success">
			    <p>Guardado con �xito.</p>
			</div>
			
			<div class="formcontainer">
	            <form ng-submit="evaluarAlumno()" name="evalForm" class="form-horizontal">
					<div class="container">
					
		                <div class="row">
							<div style="padding-bottom:4em;color:#152692"><h2>Ejercicio {{ejercicio.secuencia}}</h2></div>
		                </div>
		                
		                <div class="row">
		                    <div class="form-group col-md-12">
		                        <div class="col-md-7">
		                            <textarea ng-model="ejercicio.pregunta" rows="4" class="form-control" readonly></textarea>
		                        </div>
		                    </div>
		                </div>
					
		                <div class="row">
		                    <div class="form-group col-md-12">
		                        <div class="col-md-7">
		                            <textarea ng-model="evaluacion.respuesta" rows="12" class="form-control" readonly></textarea>
		                        </div>
		                    </div>
		                </div>
		                             
		                <div class="row">
		                    <div class="form-group col-md-12">
		                        <div class="col-md-7">
			                        <label class="col-md-2 control-lable" for="solucion_ejercicio">Soluci�n</label>
		                            <textarea id="solucion_ejercicio" ng-model="ejercicio.respuesta" rows="12" class="form-control" readonly></textarea>
		                        </div>
		                    </div>
		                </div>
		                
		                <div class="row">
		                    <div class="form-group col-md-12">
		                        <div class="col-md-7">
			                        <label class="col-md-2 control-lable" for="calificacion_ejercicio">Calificaci�n</label>
		                            <input id="calificacion_ejercicio" type="text" ng-model="evaluacion.calificacion" class="form-control"/>
		                        </div>
		                    </div>
		                </div>
		                
		                <div class="padding-button-custom">
		                	<button class="btn btn-success" ng-click="evaluacion_ctrl.evaluarAlumno()">Guardar</button>	
		                </div>		                
		                
		            </div>
	            </form>
	        </div>			

		</div>
</div>
