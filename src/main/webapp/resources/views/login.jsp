<div class="generic-container">
		
	<div style="height:4em;"></div>
    <div class="container">
           <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">

			<div class="jumbotron text-primary">
				<h1>Bienvenido!</h1>
				<p class="lead">Selecciona la forma mediante la cual quieres acceder a la plataforma. Puedes acceder mediante tu login asignado y contraseña.</p>
			</div>
		
		    <div class="panel panel-default">
		        <div class="panel-heading"><span class="lead">Identificación</span></div>
				<div class="list-group">
				  <a href="#/login_alumno" class="list-group-item">Como alumno</a>
				  <a href="#/login_profesor" class="list-group-item">Como profesor</a>
				  <a href="#/login_admin" class="list-group-item">Administrador del sistema</a>
				</div>
		
		    </div>
		</div>
	</div>
	
</div>
