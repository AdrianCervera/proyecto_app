<div class="container">
    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
       <div id="pillsContainer">
			<ul class="nav nav-pills" role="tablist">
				<li class="active"><a href="#/gestion_curso">Volver</a></li>
				<li class="active"><a href="#/" onclick="return confirm('Volver al inicio?')">Logout</a></li>
			</ul>
		</div>		
	</div>
</div>

<div class="jumbotron">
	<div class="container">
	    <div class="panel panel-default">
	        <div class="panel-heading"><span class="lead">Contenido te�rico </span></div>
	        <div class="formcontainer">
	            <form ng-submit="teoria_ctrl.submit()" name="teoriaForm" class="form-horizontal">
					<div class="container">
	 
		                <div class="row">
		                    <div class="form-group col-md-12">
		                        <label class="col-md-2 control-lable" for="tema_teoria">Tema</label>
		                        <div class="col-md-7">
		                            <input type="text" ng-model="teoria_ctrl.teoria.tema_id" id="tema_teoria" class="form-control input-sm" placeholder="Tema al que pertenece" required/>
		                        </div>
		                    </div>
		                </div>
		                
		                <div class="row">
		                    <div class="form-group col-md-12">
		                        <label class="col-md-2 control-lable" for="contenido_teoria">Contenido</label>
								<div text-angular ng-model="teoria_ctrl.teoria.contenido" id="contenido_teoria"></div>
		                    </div>
		                </div>
		
		                <div class="padding-button-custom">
		                    <input type="submit"  value="Crear" class="btn btn-primary btn-sm" ng-disabled="teoriaForm.$invalid">
		                </div>
		            </div>
	            </form>
	        </div>
    	</div>
	</div>
</div>