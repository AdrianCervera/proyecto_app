'use strict';
 
angular.module('home').factory('main_view_service', ['$http', '$q', 'REST_SERVICE_URI', function($http, $q, REST_SERVICE_URI){
 
    //var REST_SERVICE_URI = 'http://localhost:8080/proyecto_app/resources';
	var REST_SERVICE_URI_RESOURCE = REST_SERVICE_URI.resource
 
    var factory = {
    		cargar_listado_cursos: cargar_listado_cursos,
    		cargar_listado_temas: cargar_listado_temas,
    		cargar_teoria: cargar_teoria,
    		cargar_examen: cargar_examen,
    };
 
    return factory;
    
    function cargar_listado_cursos(usuario_id){

        console.log("====  URI  =========");
        console.log(REST_SERVICE_URI_RESOURCE);
        console.log(REST_SERVICE_URI);
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI_RESOURCE + 'cursos/alumno/' + usuario_id)  // ID del alumno logueado
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error while cargar_listado_temas service');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;    	
    }

    function cargar_listado_temas(curso_id) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI_RESOURCE + 'temas/' + curso_id + '/')  // ID del curso
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error while cargar_listado_temas service');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    function cargar_teoria(id_tema) {
        var deferred = $q.defer();
        //$http.get(REST_SERVICE_URI + '/teoria',{params: {id_tema: id_tema}})
        $http.get(REST_SERVICE_URI_RESOURCE + 'teoria/tema/' + id_tema)
            .then(
            function (response) {
            	console.log(response.data)
                deferred.resolve(response.data);
                
            },
            function(errResponse){
            	console.error('Error while cargar teoria tema service');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    function cargar_examen(id_tema) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI_RESOURCE + 'examen/' + id_tema)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error while cargar examen tema service');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
 
}]);