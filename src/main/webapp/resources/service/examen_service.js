'use strict';
 
angular.module('home').factory('examen_service', ['$http', '$q', 'REST_SERVICE_URI', function($http, $q, REST_SERVICE_URI){
 
    //var REST_SERVICE_URI_EXAMEN = 'http://localhost:8080/proyecto_app/examen/';
	var REST_SERVICE_URI_AUTH = REST_SERVICE_URI.auth;
 
    var factory = {
    		crearExamen: crearExamen,
    		obtenerExamen: obtenerExamen,
    };
 
    return factory;

 
    function crearExamen(examen) {
        console.log('crear examen service');
        console.log(examen)
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URI_AUTH + 'examen/', examen)
            .then(
            function (response) {
            	console.log(response);
            	console.log(response.data)
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error crear examen service');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 

    function obtenerExamen(id_tema) {    // una examen para un tema, pasamos el id del tema
        var deferred = $q.defer();   //algo a la url para get (/{id})
        $http.get(REST_SERVICE_URI_AUTH + 'examen/' + id_tema)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error en el servidor. No se ha podido obtener el alumno.');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    
 
}]);
