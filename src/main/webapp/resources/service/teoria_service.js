'use strict';
 
angular.module('home').factory('teoria_service', ['$http', '$q', 'REST_SERVICE_URI', function($http, $q, REST_SERVICE_URI){
 
    var REST_SERVICE_URI_AUTH = REST_SERVICE_URI.auth;
    var REST_SERVICE_URI_RESOURCE = REST_SERVICE_URI.resource;
 
    var factory = {
    		crearTeoria: crearTeoria,
    		obtenerTeoria: obtenerTeoria,
    		actualizarTeoria: actualizarTeoria,
    };
 
    return factory;

 
    function crearTeoria(teoria) {
        console.log('crear teoria service');
        console.log(teoria)
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URI_AUTH, teoria)
            .then(
            function (response) {
            	console.log(response);
            	console.log(response.data)
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 

    function obtenerTeoria(id_tema) {    // una teoria para un tema, pasamos el id del tema
        var deferred = $q.defer();   //algo a la url para get (/{id})
        $http.get(REST_SERVICE_URI_RESOURCE + 'teoria/tema/' + id_tema)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    
    function actualizarTeoria(teoria){

    	console.log(teoria);
        var deferred = $q.defer();
        $http.put(REST_SERVICE_URI_AUTH + 'teoria/', teoria)
            .then(
            function (response) {
            	console.log(response);
            	console.log(response.data)
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;   	
    	
    }
    
 
}]);
