'use strict';
 
angular.module('home').factory('tema_service', ['$http', '$q', 'REST_SERVICE_URI', function($http, $q, REST_SERVICE_URI){
 
    var REST_SERVICE_URI_RESOURCE = REST_SERVICE_URI.resource;
    var REST_SERVICE_URI_AUTH = REST_SERVICE_URI.auth;
    var factory = {
        obtenerTema: obtenerTema,
    	crearTema: crearTema,
    	actualizarTema: actualizarTema,
        borrarTema: borrarTema,
        listadoTemas: listadoTemas,
    };
 
    return factory;
    

    function obtenerTema(id_tema) {    // una ejercicio para un tema, pasamos el id del tema
        var deferred = $q.defer();   //algo a la url para get (/{id})
        $http.get(REST_SERVICE_URI_RESOURCE + 'tema/' + id_tema)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error en el servidor. No se ha podido obtener el tema.');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
 
    function crearTema(tema) {

    	console.log(tema);
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URI_AUTH + 'tema/', tema)
            .then(
            function (response) {
            	console.log(response);
            	console.log(response.data)
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error crear tema service');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function actualizarTema(tema){

    	console.log(tema);
        var deferred = $q.defer();
        $http.put(REST_SERVICE_URI_AUTH + 'tema/', tema)
            .then(
            function (response) {
            	console.log(response);
            	console.log(response.data)
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error crear tema service');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;   	
    	
    }
    
    
    function listadoTemas(id_curso) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI_RESOURCE + 'temas/' + id_curso)
            .then(
            function (response) {
            	console.log("listadoTemas controller response OKK");
            	console.log(response);
            	console.log(response.data)
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error listar TEMAS  service');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
     
    function borrarTema(id_tema) {

        var deferred = $q.defer();
        $http.delete(REST_SERVICE_URI_AUTH + 'tema/' + id_tema)
            .then(
            function (response) {
            	console.log(response);
            	console.log(response.data)
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error borrar tema service');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
}]);
