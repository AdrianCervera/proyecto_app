'use strict';
 
angular.module('home').factory('alumno_curso_service', ['$http', '$q', 'REST_SERVICE_URI', function($http, $q, REST_SERVICE_URI){
 
    var REST_SERVICE_URI_ADMIN = REST_SERVICE_URI.admin;
    var REST_SERVICE_URI_AUTH = REST_SERVICE_URI.auth;
 
    var factory = {
    		obtenerAlumnosPorCurso: obtenerAlumnosPorCurso,
    		inscribirAlumno: inscribirAlumno,
    		desinscribirAlumno: desinscribirAlumno,
            
          //  listadoCursos: listadoCursos,
          //  listadoCursosAdmin: listadoCursosAdmin,
          //  actualizarCurso: actualizarCurso,
          //  borrarCurso: borrarCurso,
    };
 
    return factory;

    
    function obtenerAlumnosPorCurso(id_curso){
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI_AUTH + 'cursos/alumno/curso/' + id_curso)
            .then(
            function (response) {
            	console.log(response.data)
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    
    function inscribirAlumno(id_alumno, id_curso){
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URI_ADMIN + 'cursos/alumno/curso/' + id_alumno + '/' + id_curso)
            .then(
            function (response) {
            	console.log(response.data)
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }    
    

    function desinscribirAlumno(id_alumno, id_curso){
        var deferred = $q.defer();
        $http.delete(REST_SERVICE_URI_ADMIN + 'cursos/alumno/curso/' + id_alumno + '/' + id_curso)
            .then(
            function (response) {
            	console.log(response.data)
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    } 
    
    
}]);

