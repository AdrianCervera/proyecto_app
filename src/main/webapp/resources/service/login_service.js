'use strict';
 
angular.module('home').factory('login_service', ['$http', '$q', 'REST_SERVICE_URI', function($http, $q, REST_SERVICE_URI){
 
  //  var REST_SERVICE_URI = 'http://localhost:8080/proyecto_app/login/';
  //  var REST_SERVICE_URI_ADMIN = 'http://localhost:8080/proyecto_app/login/admin/';
  //  var REST_SERVICE_URI_PROFESOR = 'http://localhost:8080/proyecto_app/login/profesor/';
  //  var REST_SERVICE_URI_ALUMNO = 'http://localhost:8080/proyecto_app/login/alumno/';
 
    var factory = {
        login: login,
        auto_logout: auto_logout,
    	/*login_alumno: login_alumno,
        login_profesor: login_profesor,
        login_admin: login_admin,*/
    };
 
    return factory;
    
    function auto_logout(){
        var deferred = $q.defer();

        URL = REST_SERVICE_URI.logout
        console.log("=================");
        
        $http.post(URL)
            .then(
            function (response) {
                console.log(response);
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.log(errResponse)
                console.error('Error logout service');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    	        
        
    }
    
    function login(usuario){
        var deferred = $q.defer();
        
        /*
        if(usuario.rol == 'admin'){
        	URL = REST_SERVICE_URI.login // + 'admin/'
        	//URL = REST_SERVICE_URI_ADMIN;
        }
        else if(usuario.rol == 'profesor'){
        	//URL = REST_SERVICE_URI_PROFESOR;
        	URL = REST_SERVICE_URI.login // + 'profesor/'
        }
        else if(usuario.rol == 'alumno'){
        	//URL = REST_SERVICE_URI_ALUMNO;
        	URL = REST_SERVICE_URI.login
        }
        */
        
        URL = REST_SERVICE_URI.login

        console.log(URL);
        console.log(usuario);
        console.log("=================");
        
        $http.post(URL, usuario)
            .then(
            function (response) {
                console.log(response);
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.log(errResponse)
                console.error('Error login service');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    	
    }
    
 /*
    function login_alumno(usuario) {
        var deferred = $q.defer();
        console.log(usuario);
        $http.post(REST_SERVICE_URI_ALUMNO, usuario)
            .then(
            function (response) {
                console.log('service in response');
                console.log(response);
                console.log(response.data);
                deferred.resolve(response.data);
                console.log('deferred:');
                console.log(deferred);
                console.log(deferred.resolve);
                
                deferred.resolve(deferred);
            },
            function(errResponse){
                console.log(errResponse)
                console.error('Error while login service');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 */
}]);
