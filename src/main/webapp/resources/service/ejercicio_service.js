'use strict';
 
angular.module('home').factory('ejercicio_service', ['$http', '$q', 'REST_SERVICE_URI', function($http, $q, REST_SERVICE_URI){
 
	var REST_SERVICE_URI_AUTH = REST_SERVICE_URI.auth;
	var REST_SERVICE_URI_RESOURCE = REST_SERVICE_URI.resource
 
    var factory = {
    		crearEjercicio: crearEjercicio,
    		obtenerEjercicio: obtenerEjercicio,
    		obtenerEjercicioAlumno: obtenerEjercicioAlumno,
    		obtenerEjercicioSolucionAlumno: obtenerEjercicioSolucionAlumno,
        	actualizarEjercicio: actualizarEjercicio,
    		listadoEjercicios: listadoEjercicios,
    		listadoEjerciciosAlumno: listadoEjerciciosAlumno,
    		listadoEjerciciosExamen: listadoEjerciciosExamen,
    		listadoEjerciciosExamenAlumno: listadoEjerciciosExamenAlumno,
    		borrarEjercicio: borrarEjercicio,
    };
 
    return factory;

 
    function crearEjercicio(ejercicio) {
        console.log('crear ejercicio service');
        console.log(ejercicio)
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URI_AUTH + 'ejercicio/', ejercicio)
            .then(
            function (response) {
            	console.log(response);
            	console.log(response.data)
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error crear ejercicio service');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 

    function obtenerEjercicio(id_ejercicio) {    // obtener ejercicio por ID
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI_RESOURCE + 'ejercicio/' + id_ejercicio)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function obtenerEjercicioAlumno(id_ejercicio) {    // obtener ejercicio por ID - con el campo de respuesta oculto
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI_RESOURCE + 'ejercicio/alumno/' + id_ejercicio)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    function obtenerEjercicioSolucionAlumno(id_ejercicio, id_alumno) {    // obtener respuesta del ejercicio para el alumno (comprobaciones en Spring)
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI_RESOURCE + 'ejercicio/alumno/solucion/' + id_ejercicio + '/' + id_alumno)
            .then(
            function (response) {
            	console.log("RESPSRP");
            	console.log(response.data);
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.log("petergg");
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function actualizarEjercicio(ejercicio){
    	
        var deferred = $q.defer();
        $http.put(REST_SERVICE_URI_AUTH + 'ejercicio/', ejercicio)
            .then(
            function (response) {
            	console.log(response);
            	console.log(response.data)
                deferred.resolve(response.data);            	
            },
            function(errResponse){
            	console.error('Error crear tema service');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;   	
    	
    }
    
    
    function listadoEjercicios(id_tema) {  // retorna listado de ejercicios de un tema
        var deferred = $q.defer();
        
    	console.log(id_tema);
    	console.log(REST_SERVICE_URI_RESOURCE + 'ejercicio/tema/' + id_tema);
    	
        $http.get(REST_SERVICE_URI_RESOURCE + 'ejercicio/tema/' + id_tema)
            .then(
            function (response) {
            	console.log("listadoEjercicios controller response OK");
            	console.log(response);
            	console.log(response.data)
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error listar ejercicios service');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function listadoEjerciciosAlumno(id_tema) {  // retorna listado de ejercicios de un tema - sin campos de respuesta
        var deferred = $q.defer();    	
        $http.get(REST_SERVICE_URI_RESOURCE + 'ejercicio/alumno/tema/' + id_tema)
            .then(
            function (response) {
            	console.log("listadoEjercicios controller response OK");
            	console.log(response);
            	console.log(response.data)
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error listar ejercicios service');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    
    function listadoEjerciciosExamen(id_tema) {
        var deferred = $q.defer();
            	
        $http.get(REST_SERVICE_URI_RESOURCE + 'ejercicio/tema/examen/' + id_tema)
            .then(
            function (response) {
            	console.log(response.data)
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error listar ejercicios examen service');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    
    function listadoEjerciciosExamenAlumno(id_tema) {
        var deferred = $q.defer();
            	
        $http.get(REST_SERVICE_URI_RESOURCE + 'ejercicio/alumno/tema/examen/' + id_tema)
            .then(
            function (response) {
            	console.log(response.data)
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error listar ejercicios examen service');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    
    function borrarEjercicio(id_ejercicio) {

        var deferred = $q.defer();
        $http.delete(REST_SERVICE_URI_AUTH + 'ejercicio/' + id_ejercicio)
            .then(
            function (response) {
            	console.log(response);
            	console.log(response.data)
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error borrar ejercicio service');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
 
}]);
