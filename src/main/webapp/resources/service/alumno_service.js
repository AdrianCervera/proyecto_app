'use strict';
 
angular.module('home').factory('alumno_service', ['$http', '$q', 'REST_SERVICE_URI', function($http, $q, REST_SERVICE_URI){
 
    //var REST_SERVICE_URI = 'http://localhost:8080/proyecto_app/alumno/';
	var REST_SERVICE_URI_ADMIN = REST_SERVICE_URI.admin;
	var REST_SERVICE_URI_AUTH = REST_SERVICE_URI.auth;
 
    var factory = {
        listadoAlumnos: listadoAlumnos,
        crearAlumno: crearAlumno,
    };
 
    return factory;
 
    function listadoAlumnos() {
        var deferred = $q.defer();
        console.log('listadoAlumnos');
        $http.get(REST_SERVICE_URI_AUTH)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error en el servidor. No se ha podido obtener el listado de alumnos.');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function obtenerAlumno(alumno) {
        var deferred = $q.defer();   //algo a la url para get (/{id})
        $http.post(REST_SERVICE_URI_AUTH, alumno)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error en el servidor. No se ha podido obtener el alumno.');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    
    function crearAlumno(alumno) {
        console.log('crearAlumno');
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URI_ADMIN, alumno)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error en el servidor. No se ha podido crear el alumno.');

                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    
    function editarAlumno(alumno) {
        var deferred = $q.defer();   //algo a la url para edit
        $http.post(REST_SERVICE_URI_ADMIN, alumno)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error en el servidor. No se ha podido editar el alumno.');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    
    function borrarAlumno(alumno) {
        var deferred = $q.defer();;   //algo a la url para borrar
        $http.post(REST_SERVICE_URI, alumno)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error en el servidor. No se ha podido borrar el alumno.');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
 
 
}]);
