'use strict';
 
angular.module('home').factory('evaluacion_service', ['$http', '$q', 'REST_SERVICE_URI', function($http, $q, REST_SERVICE_URI){
 
	var REST_SERVICE_URI_AUTH = REST_SERVICE_URI.auth;
	var REST_SERVICE_URI_RESOURCE = REST_SERVICE_URI.resource
 
    var factory = {
    		crearEvaluacion: crearEvaluacion,
    		obtenerEvaluacion: obtenerEvaluacion,
        	actualizarEvaluacion: actualizarEvaluacion,
    		//listadoEjercicios: listadoEjercicios,
    		//listadoEjerciciosExamen: listadoEjerciciosExamen,
    		//borrarEjercicio: borrarEjercicio,
    };
 
    return factory;

 
    function crearEvaluacion(evaluacion) {
        console.log(evaluacion)
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URI_RESOURCE + 'evaluacion/', evaluacion)
            .then(
            function (response) {
            	console.log(response);
            	console.log(response.data)
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 

    function obtenerEvaluacion(id_alumno, id_ejercicio) {    // obtener evaluacion por IDs
        var deferred = $q.defer();   //algo a la url para get (/{id})
        $http.get(REST_SERVICE_URI_RESOURCE + 'evaluacion/' + id_alumno + '/' + id_ejercicio)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function actualizarEvaluacion(evaluacion){
    	
        var deferred = $q.defer();
        $http.put(REST_SERVICE_URI_AUTH + 'evaluacion/', evaluacion)
            .then(
            function (response) {
            	console.log(response.data)
                deferred.resolve(response.data);            	
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;   	
    	
    }
    
 
}]);
