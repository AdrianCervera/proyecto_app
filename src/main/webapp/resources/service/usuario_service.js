'use strict';
 
angular.module('home').factory('usuario_service', ['$http', '$q', 'REST_SERVICE_URI', function($http, $q, REST_SERVICE_URI){
 
 //   var REST_SERVICE_URI = 'http://localhost:8080/proyecto_app/usuario/';
 //   var REST_SERVICE_URI_ADMIN = 'http://localhost:8080/proyecto_app/usuario/admin/';
 //   var REST_SERVICE_URI_PROFESOR = 'http://localhost:8080/proyecto_app/usuario/profesor/';
 //   var REST_SERVICE_URI_ALUMNO = 'http://localhost:8080/proyecto_app/usuario/alumno/';
    
    var REST_SERVICE_URI_ADMIN = REST_SERVICE_URI.admin + 'usuario/';
    var REST_SERVICE_URI_AUTH = REST_SERVICE_URI.auth + 'usuario/';
    //var REST_SERVICE_URI_RESOURCS = REST_SERVICE_URI.resource + 'usuario/';
    
    var factory = {

        listadoAdmins: listadoAdmins,
        obtenerAdmin: obtenerAdmin,
        crearAdmin: crearAdmin,
        editarAdmin: editarAdmin,
        borrarAdmin: borrarAdmin,
        
        listadoProfesores: listadoProfesores,
        obtenerProfesor: obtenerProfesor,
        crearProfesor: crearProfesor,
        editarProfesor: editarProfesor,
        borrarProfesor: borrarProfesor,
        
        listadoAlumnos: listadoAlumnos,
        obtenerAlumno: obtenerAlumno,
        crearAlumno: crearAlumno,
        editarAlumno: editarAlumno,
        borrarAlumno: borrarAlumno,
    };
 
    return factory;
 

    
    /*
     *  ADMINISTRADOR
     */
 

    function listadoAdmins() {
        var deferred = $q.defer();
        console.log('listadoAdmins');
        $http.get(REST_SERVICE_URI_AUTH + 'admin')
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    

    function obtenerAdmin(admin_id) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI_AUTH + 'admin/' + admin_id + '/')
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    
    function crearAdmin(admin) {
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URI_ADMIN + 'admin/', admin)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    
    function editarAdmin(admin) {
        var deferred = $q.defer();  
        $http.put(REST_SERVICE_URI_ADMIN + 'admin/', admin)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    
    function borrarAdmin(id_admin) {
        var deferred = $q.defer();;  
        $http.delete(REST_SERVICE_URI_ADMIN + 'admin/' + id_admin)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    
    /*
     *  PROFESOR
     */
 

    function listadoProfesores() {
        var deferred = $q.defer();
        console.log('listadoProfesores');
        $http.get(REST_SERVICE_URI_AUTH + 'profesor')
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error en el servidor. No se ha podido obtener el listado de profesores.');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    
    function obtenerProfesor(profesor_id) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI_AUTH + 'profesor/' + profesor_id + '/')
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error en el servidor. No se ha podido obtener profesor.');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    
    function crearProfesor(profesor) {
        console.log('crearProfesor');
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URI_ADMIN + 'profesor/', profesor)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error en el servidor. No se ha podido crear el profesor.');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    
    function editarProfesor(profesor) {
        var deferred = $q.defer();  
        $http.put(REST_SERVICE_URI_ADMIN + 'profesor/', profesor)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error en el servidor. No se ha podido editar el profesor.');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    

    function borrarProfesor(id_profesor) {
        var deferred = $q.defer();;  
        $http.delete(REST_SERVICE_URI_ADMIN + 'profesor/' + id_profesor)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error en el servidor. No se ha podido borrar el profesor.');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    
    
    /*
     *  ALUMNO
     */

    function listadoAlumnos() {
        var deferred = $q.defer();
        console.log('listadoAlumnos');
        $http.get(REST_SERVICE_URI_AUTH + 'alumno')
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error en el servidor. No se ha podido obtener el listado de alumnos.');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function obtenerAlumno(id_alumno) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI_AUTH + 'alumno/' + id_alumno + '/')
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error en el servidor. No se ha podido obtener el alumno.');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    
    function crearAlumno(alumno) {
        console.log('crearAlumno');
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URI_ADMIN + 'alumno/', alumno)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error en el servidor. No se ha podido crear el alumno.');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    
    function editarAlumno(alumno) {
        var deferred = $q.defer();
        $http.put(REST_SERVICE_URI_ADMIN + 'alumno/', alumno)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error en el servidor. No se ha podido editar el alumno.');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    
    function borrarAlumno(id_alumno) {
        var deferred = $q.defer();;   //algo a la url para borrar
        $http.delete(REST_SERVICE_URI_ADMIN + 'alumno/' + id_alumno)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error en el servidor. No se ha podido borrar el alumno.');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
 
 
 
}]);
