'use strict';
 
angular.module('home').factory('curso_service', ['$http', '$q', 'REST_SERVICE_URI', function($http, $q, REST_SERVICE_URI){
 
    var REST_SERVICE_URI_ADMIN = REST_SERVICE_URI.admin
    var REST_SERVICE_URI_AUTH = REST_SERVICE_URI.auth;
 
    var factory = {
            crearCurso: crearCurso,
            obtenerCurso: obtenerCurso,
            listadoCursos: listadoCursos,				// retorna listado de los cursos de un profesor
            listadoCursosAdmin: listadoCursosAdmin,     // retorna listado de todos los cursos creados
            listadoCursosAlumno: listadoCursosAlumno,   // retorna listados de los cursos inscritos/ no inscritos
            actualizarCurso: actualizarCurso,
            borrarCurso: borrarCurso,
    };
 
    return factory;


    function crearCurso(curso) {
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URI_ADMIN + 'curso/', curso)
            .then(
            function (response) {
            	console.log(response);
            	console.log(response.data)
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error crear curso service');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    

    function obtenerCurso(id_curso) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI_ADMIN + 'curso/' + id_curso)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error en el servidor. No se ha podido obtener el curso.');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    
    function listadoCursos(id_profesor) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI_AUTH + 'cursos/profesor/' + id_profesor)
            .then(
            function (response) {
            	console.log("listadoCursos controller response OKK");
            	console.log(response);
            	console.log(response.data)
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error listar cursos service');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    function listadoCursosAdmin() {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI_ADMIN + 'cursos/')
            .then(
            function (response) {
            	console.log("listadoCursos ADMIN response OKK");
            	console.log(response);
            	console.log(response.data)
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error listar cursos admin service');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    function listadoCursosAlumno(id_alumno) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI_ADMIN + 'cursos/alumno/' + id_alumno)
            .then(
            function (response) {
            	console.log(response.data)
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error listar cursos admin service');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    function actualizarCurso(curso){

    	console.log(curso);
        var deferred = $q.defer();
        $http.put(REST_SERVICE_URI_ADMIN + 'curso/', curso)
            .then(
            function (response) {
            	console.log(response);
            	console.log(response.data)
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error crear curso service');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;   	
    	
    }

    function borrarCurso(id_curso) {

        var deferred = $q.defer();
        $http.delete(REST_SERVICE_URI_ADMIN + 'curso/' + id_curso)
            .then(
            function (response) {
            	console.log(response);
            	console.log(response.data)
                deferred.resolve(response.data);
            },
            function(errResponse){
            	console.error('Error borrar curso service');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
}]);
