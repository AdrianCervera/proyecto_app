'use strict';

//var host = 'http://localhost:8080/';  // IP local
var host = 'http://192.168.1.9:8080/';  // IP servidor local
 
angular.module('home',['ui.bootstrap', 'ngRoute', 'ngSanitize', 'textAngular', 'ngMaterial']);

angular.module('home').config(['$routeProvider', function($routeProvider) {
   $routeProvider.
   
   when('/login', {
      templateUrl: 'resources/views/login.jsp'
   }).
   
   when('/crear_tema', {
	      templateUrl: 'resources/views/crear_tema.jsp',
	      controller: 'tema_controller',
	      controllerAs: 'tema_ctrl'
   }).	  

   when('/crear_curso', {
	      templateUrl: 'resources/views/crear_curso.jsp',
	      controller: 'curso_controller',
	      controllerAs: 'curso_ctrl'
   }).	   

   when('/crear_teoria', {
	      templateUrl: 'resources/views/crear_teoria.jsp',
	      controller: 'teoria_controller',
	      controllerAs: 'teoria_ctrl'
   }).	   
   
   when('/crear_ejercicio', {
	      templateUrl: 'resources/views/crear_ejercicio_texto.jsp',  // TODO variso tipos ejercicio
	      controller: 'ejercicio_controller',
	      controllerAs: 'ejercicio_ctrl'
   }).	   
   
   when('/crear_examen', {
	      templateUrl: 'resources/views/crear_examen.jsp',
	      controller: 'examen_controller',
	      controllerAs: 'examen_ctrl'
   }).	   
   
   when('/crear_usuario', {
	      templateUrl: 'resources/views/crear_usuario.jsp'
   }).	   
   
   when('/crear_alumno', {
	      templateUrl: 'resources/views/crear_alumno.jsp',
	      controller: 'alumno_controller',
	      controllerAs: 'alumno_ctrl'
   }).	   
   
   when('/crear_profesor', {
	      templateUrl: 'resources/views/crear_profesor.jsp',
	      controller: 'profesor_controller',
	      controllerAs: 'profesor_ctrl'
   }).	   
   
   when('/crear_admin', {
	      templateUrl: 'resources/views/crear_admin.jsp',
	      controller: 'admin_controller',
	      controllerAs: 'admin_ctrl'
   }).	   
   
   when('/login_alumno', {
	      templateUrl: 'resources/views/login_alumno.jsp',
	      controller: 'login_controller',
	      controllerAs: 'login_ctrl'
   }).	   
   
   when('/login_profesor', {
	      templateUrl: 'resources/views/login_profesor.jsp',
	      controller: 'login_controller',
	      controllerAs: 'login_ctrl'
   }).	   
   
   when('/login_admin', {
	      templateUrl: 'resources/views/login_admin.jsp',
	      controller: 'login_controller',
	      controllerAs: 'login_ctrl'
   }).
   
   when('/main_view_alumno', {
	      templateUrl: 'resources/views/main_alumno.jsp',
	      controller: 'main_view_controller',
	      controllerAs: 'main_view_ctrl'
   }).
   
   when('/main_view_profesor', {
	      templateUrl: 'resources/views/main_profesor.jsp',
   }).
   
   when('/main_view_admin', {
	      templateUrl: 'resources/views/main_admin.jsp',
   }).
   
   when('/listado_cursos', {
	      templateUrl: 'resources/views/listado_cursos.jsp',
	      controller: 'listado_curso_controller',
	      controllerAs: 'listado_curso_ctrl'
   }).
   
   when('/editar_curso', {
	      templateUrl: 'resources/views/editar_curso.jsp',
	      controller: 'editar_curso_controller',
	      controllerAs: 'editar_curso_ctrl'
   }).

   when('/gestion_curso', {
	      templateUrl: 'resources/views/gestion_curso.jsp',
	      controller: 'gestion_curso_controller',
	      controllerAs: 'gestion_curso_ctrl'
   }).

   when('/editar_tema', {
	      templateUrl: 'resources/views/editar_tema.jsp',
	      controller: 'editar_tema_controller',
	      controllerAs: 'editar_tema_ctrl'
   }).

   when('/editar_ejercicio', {
	      templateUrl: 'resources/views/editar_ejercicio_texto.jsp',
	      controller: 'ejercicio_controller',
	      controllerAs: 'ejercicio_ctrl'
   }).

   when('/gestion_cursos', {
	      templateUrl: 'resources/views/gestion_cursos_admin.jsp',
	      controller: 'gestion_cursos_admin_controller',
	      controllerAs: 'gestion_cursos_ctrl'
   }).

   when('/gestion_alumnos', {
	      templateUrl: 'resources/views/gestion_usuarios.jsp',
	      controller: 'gestion_alumno_controller',
	      controllerAs: 'gestion_usuario_ctrl'
   }).

   when('/gestion_profesores', {
	      templateUrl: 'resources/views/gestion_usuarios.jsp',
	      controller: 'gestion_profesor_controller',
	      controllerAs: 'gestion_usuario_ctrl'
   }).

   when('/gestion_administradores', {
	      templateUrl: 'resources/views/gestion_usuarios.jsp',
	      controller: 'gestion_admin_controller',
	      controllerAs: 'gestion_usuario_ctrl'
   }).

   when('/editar_alumno', {
	      templateUrl: 'resources/views/editar_alumno.jsp',
	      controller: 'alumno_controller',
	      controllerAs: 'alumno_ctrl'
   }).

   when('/editar_profesor', {
	      templateUrl: 'resources/views/editar_profesor.jsp',
	      controller: 'profesor_controller',
	      controllerAs: 'profesor_ctrl'
   }).

   when('/editar_admin', {
	      templateUrl: 'resources/views/editar_admin.jsp',
	      controller: 'admin_controller',
	      controllerAs: 'admin_ctrl'
   }).

   when('/evaluar_alumno_listado', {
	      templateUrl: 'resources/views/evaluar_alumno_listado.jsp',
	      controller: 'evaluacion_controller',
	      controllerAs: 'evaluacion_ctrl'
   }).

   when('/evaluar_alumno', {
	      templateUrl: 'resources/views/evaluar_alumno.jsp',
	      controller: 'evaluacion_controller',
	      controllerAs: 'evaluacion_ctrl'
   }).

   otherwise({
      redirectTo: '/login'
   });
}]);


angular.module('home').run(function($rootScope) {
    $rootScope.authenticated = false;
    $rootScope.error = false;
});


//Create the factory that share the object  - Variables de CLIENTE

// Usuario - usuario activo
angular.module('home').factory('Usuario', function () {

    var usuario = {
        id: ''
    };

    return {
        getId: function () {
            return usuario.id;
        },
        setId: function (id) {
        	usuario.id = id;
        }
    };
});

//Curso - curso activo
angular.module('home').factory('Curso', function () {

  var curso = {
      id: ''
  };

  return {
      getId: function () {
          return curso.id;
      },
      setId: function (id) {
      	curso.id = id;
      }
  };
});

//Tema - tema activo
angular.module('home').factory('Tema', function () {

var tema = {
    id: ''
};

return {
    getId: function () {
        return tema.id;
    },
    setId: function (id) {
    	tema.id = id;
    }
};
});


//Ejercicio - ejercicio activo
angular.module('home').factory('Ejercicio', function () {

var ejercicio = {
    id: ''
};

return {
    getId: function () {
        return ejercicio.id;
    },
    setId: function (id) {
    	ejercicio.id = id;
    }
};
});


//Examen - examen activo
angular.module('home').factory('Examen', function () {

var examen = {
    id: ''
};

return {
    getId: function () {
        return examen.id;
    },
    setId: function (id) {
    	examen.id = id;
    }
};
});


//Alumno - alumno activo
angular.module('home').factory('Alumno', function () {

var alumno = {
  id: ''
};

return {
  getId: function () {
      return alumno.id;
  },
  setId: function (id) {
	  alumno.id = id;
  }
};
});

//Profesor - profesor activo
angular.module('home').factory('Profesor', function () {

var profesor = {
id: ''
};

return {
getId: function () {
    return profesor.id;
},
setId: function (id) {
	profesor.id = id;
	}
};
});

//Admin - admin activo
angular.module('home').factory('Admin', function () {

var admin = {
id: ''
};

return {
getId: function () {
    return admin.id;
},
setId: function (id) {
	admin.id = id;
	}
};
});



// URIs para el servidor

angular.module('home').value('REST_SERVICE_URI', {
    admin: 	  host + 'proyecto_app/admin/',   	// peticion admin - CUD usuarios, cursos
    auth: 	  host + 'proyecto_app/auth/',   	// peticion admin/profesor - CUD temas, evaluacion, metricas
    resource: host + 'proyecto_app/resource/',  // peticion admin/profesor/alumno - acceso a contenido
    login: 	  host + 'proyecto_app/login/',		// peticion abiertas a todos
    logout:   host + 'proyecto_app/logout/',	// peticion abiertas a todos
});

