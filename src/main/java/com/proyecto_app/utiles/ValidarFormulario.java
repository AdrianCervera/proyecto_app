package com.proyecto_app.utiles;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.stereotype.Service;

import com.aeat.valida.Validador;
import com.proyecto_app.model.Usuario;

@Service
public class ValidarFormulario {
    private String respuesta;
    private boolean validado;
    
    public ValidarFormulario(){}

    public ValidarFormulario(String respuesta, boolean validado) {
        this.respuesta = respuesta;
        this.validado = validado;
    }

    public String getRespuesta() {
		return respuesta;
	}

	public boolean isValidado() {
		return validado;
	}

	public ValidarFormulario Validar(Usuario usuario){
		
	  // login
	  if (!loginPermitido(usuario.getLogin())){
		    System.out.println(" LOGIN KO");
		    return new ValidarFormulario("Login no válido.", false);
	  }
	
	  // password
	  if (usuario.getPassword().length() < 6){
		    System.out.println(" PASS KO");
		    return new ValidarFormulario("Contraseña demasiado corta.", false);	
	  }
    	
	  // dni
	  if (usuario.getDni().length() < 9 || (new Validador()).checkNif(usuario.getDni()) <= 0){

	      System.out.println(" DNI KO");
	      return new ValidarFormulario("DNI no válido.", false);
		  //return new ResponseEntity<String>("{\"respuesta\":\"DNI no válido.\"}", HttpStatus.CONFLICT);
	  }
	  
	  // telefono
      if(!usuario.getTelefono().matches("^[69][0-9]{8,11}$")){  // digito empieza por 6 o 9, seguido de entre ocho y once digitos mas
	      System.out.println(" TLF KO");
	      return new ValidarFormulario("Teléfono no válido.", false);
		  //return new ResponseEntity<String>("{\"respuesta\":\"Teléfono no válido.\"}", HttpStatus.CONFLICT);
      }
	  	  
	  // email
	  String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
	            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

      System.out.println(PATTERN_EMAIL);
	  Pattern pattern = Pattern.compile(PATTERN_EMAIL);
	  Matcher matcher = pattern.matcher(usuario.getEmail());
	  if (!matcher.matches()){
	      System.out.println(" EMAIL KO");
	      return new ValidarFormulario("Email no válido.", false);
		  //return new ResponseEntity<String>("{\"respuesta\":\"Email no válido.\"}", HttpStatus.CONFLICT); 
	  }

	  return new ValidarFormulario("", true);
    	
    }
    
    public boolean loginPermitido(String cadena) {
	    System.out.println("CADENA loginPermitido");
	    System.out.println(cadena);
    	
    	if(cadena.length() >= 6 && cadena.length() < 25){
    	    System.out.println(cadena.length());
	        for(int i = 0; i < cadena.length(); i++) {     
	            if(!Character.isLetterOrDigit(cadena.charAt(i)) && cadena.charAt(i) != '_') {
	        	    System.out.println(cadena.charAt(i));
	                return false;
	            }
	        }
    	    System.out.println("es true");
	        
	        return true;
    	}
    
    	return false;
    }
    

}
