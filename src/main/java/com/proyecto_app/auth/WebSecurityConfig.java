package com.proyecto_app.auth;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.proyecto_app.auth.UserDetailsServiceImpl;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
    @Autowired
    private UserDetailsService userDetailsService;
    
	@Autowired
	DataSource dataSource;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	System.out.println("CONFIGURATION configure WebSecurityConfigurerAdapter");
    	System.out.println("---------------------\n---------------------");
    	/*
        http.csrf().disable()
        .authorizeRequests()
        .antMatchers("/resources/**").hasRole("ADMIN")
        .and().httpBasic().realmName(REALM).authenticationEntryPoint(getBasicAuthEntryPoint())
        .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);//We don't need sessions to be created.
*/
    	
    	// ROLE_  : http://www.journaldev.com/8748/spring-security-role-based-access-authorization-example
    	
        http.csrf().disable()
        .authorizeRequests()
      //  	.anyRequest().permitAll();
        
	        .antMatchers("/admin/**").access("hasRole('ROLE_ADMINISTRADOR')")
	        
	        .antMatchers("/auth/**").access("hasRole('ROLE_ADMINISTRADOR') or hasRole('ROLE_PROFESOR')")
	        //.antMatchers("/resource/**").access("hasRole('ROLE_ALUMNO')")
	        .antMatchers("/resource/**").authenticated()
	        //.antMatchers("/login/**").permitAll()
	        .antMatchers("/resources/**").permitAll()  // librerias js y css
	       // .anyRequest().permitAll()
	        
       // .anyRequest().authenticated();
	    //.and().httpBasic().authenticationEntryPoint(getBasicAuthEntryPoint())
	    .and().formLogin()
        	.loginPage("/login")
			.permitAll();
        //.and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    	
        
        /*
        http
                .authorizeRequests()
                	//.antMatchers("/usuario/**", "/resources/**").access("hasRole('ALUMNO')")
                    //.antMatchers("/usuario/**", "/resources/**").permitAll()
                    .anyRequest().authenticated()
                    .and()
                .formLogin();*/
                 /*   .loginPage("/login")
                    .permitAll()
                    .and()
                .logout()
                    .permitAll();
                */
        
    }  
    @Bean
    public CustomBasicAuthenticationEntryPoint getBasicAuthEntryPoint(){
        return new CustomBasicAuthenticationEntryPoint();
    }
    
    /*
	@Autowired
	public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {

      System.out.println("CONFIGURATION configAuthentication QUERYS AUTH");
	  auth.jdbcAuthentication().dataSource(dataSource)
		.usersByUsernameQuery(
			"select login, password from usuario where login = ?")
		.authoritiesByUsernameQuery(
			"select u.login, r.rol from usuario u, rol r, usuariorol ur where u.login = ? and ur.usuarioId = u.id and r.id = ur.rolId");
	  
	}
	*/

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    	System.out.println("CONFIGURATION configureGlobal UserDetailsServiceImpl");
    	
    	// AUTH - EXPLICAR IN MEMORY, 	jdbcAuthentication (arriba) y el modo fetén -> userDetailsService
    	// https://stackoverflow.com/questions/32035703/security-config-configureglobal
    	// The most flexible option is to implement a UserDetailsService. You can then look up users in any way you want
  
    	/*
        auth.inMemoryAuthentication().withUser("prueba")
        .password("prueba").roles("ALUMNO");
        auth.inMemoryAuthentication().withUser("profe")
        .password("profe").roles("PROFESOR");
        auth.inMemoryAuthentication().withUser("admin")
        .password("admin").roles("ADMINISTRADOR");
         */  
    	
        auth.userDetailsService(userDetailsService);//.passwordEncoder(bCryptPasswordEncoder());
                
    	// select login, password from usuario where login = 'prueba';
    	// select u.login, r.rol from usuario u, rol r, usuariorol ur where u.login = 'prueba' and ur.usuarioId = u.id and r.id = ur.rolId
    	System.out.println("---------------------\n---------------------");
    } 
	
    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
    
}
