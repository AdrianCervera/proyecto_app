package com.proyecto_app.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import com.proyecto_app.service.AlumnoService;

@Service
public class SecurityServiceImpl implements SecurityService{
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;
    private AlumnoService alumnoService;

    private static final Logger logger = LoggerFactory.getLogger(SecurityServiceImpl.class);

    @Override
    public String findLoggedInUsername() {
        Object userDetails = SecurityContextHolder.getContext().getAuthentication().getDetails();
        if (userDetails instanceof UserDetails) {
        	System.out.println("================================================");       	
        	System.out.println("SECURITYSERVICE - findLoggedInUsername");
            return ((UserDetails)userDetails).getUsername();
        }

        return null;
    }

    @Override
    public void autologin(String username, String password) {
    	System.out.println("================================================"); 
    	System.out.println("SECURITYSERVICE - AUTOLOGIN");
        //UserDetails userDetails = alumnoService.get(username);
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());

    	System.out.println("usernamePasswordAuthenticationToken:");
    	System.out.println(userDetails.getAuthorities());

    	System.out.println("AUTHORITIES DEL TOKEN:");
    	System.out.println();
    	
    	try{

        	System.out.println("TRY:");
        	System.out.println(username);
        	System.out.println(password);
        	
            authenticationManager.authenticate(usernamePasswordAuthenticationToken);
    	} catch (Exception e) {
        	System.out.println("CATCH:");
        	System.out.println(e.getCause().getMessage());
            logger.error(e.getCause().getMessage());
    	}
    	
        //authenticationManager.authenticate(usernamePasswordAuthenticationToken);

    	System.out.println("ALALALALALALALALALALAAAAAAA");
    	System.out.println(usernamePasswordAuthenticationToken.isAuthenticated());
        

        if (usernamePasswordAuthenticationToken.isAuthenticated()) {
        	System.out.println("usernamePasswordAuthenticationToken.isAuthenticated():");
        	System.out.println(usernamePasswordAuthenticationToken.isAuthenticated());
            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            logger.debug(String.format("Auto login %s successfully!", username));
        } else {

        	System.out.println("NO ENTRA. usernamePasswordAuthenticationToken.isAuthenticated():");
        	System.out.println(usernamePasswordAuthenticationToken.isAuthenticated());
        }
    }
}
