package com.proyecto_app.auth;

import com.proyecto_app.model.Rol;
import com.proyecto_app.model.Usuario;
import com.proyecto_app.service.UsuarioService;
import com.proyecto_app.service.UsuarioRolService;
import com.proyecto_app.model.Alumno;
import com.proyecto_app.service.AlumnoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{
	
	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private UsuarioRolService usuarioRolService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    	System.out.println("= UserDetailsServiceImpl - loadUserByUsername");
        Usuario usuario = usuarioService.get(username);

        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        List<Rol> roles = usuarioRolService.getRoles(usuario.getId());
    	System.out.println(usuario.getLogin());
    	System.out.println(roles);
    	System.out.println("=   que roles ---^ ======");
        for(Rol rol : roles){
        	System.out.println(rol.getRol());
        	grantedAuthorities.add(new SimpleGrantedAuthority(rol.getRol()));  // TODO
        }
        
        //for (Rol rol : usuario.getRol()){
        //    grantedAuthorities.add(new SimpleGrantedAuthority(rol.getRol()));
        //}

    	System.out.println(grantedAuthorities);
    	System.out.println("==retornamos userdetails user==");

        return new org.springframework.security.core.userdetails.User(usuario.getLogin(), usuario.getPassword(), grantedAuthorities);
    }
}
