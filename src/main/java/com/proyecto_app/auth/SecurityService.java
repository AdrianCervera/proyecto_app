package com.proyecto_app.auth;

public interface SecurityService {
    String findLoggedInUsername();

    void autologin(String username, String password);
}
