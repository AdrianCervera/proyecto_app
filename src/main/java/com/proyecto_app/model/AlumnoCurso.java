package com.proyecto_app.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

// TABLA INTERMEDIA DE ALUMNOS INSCRITOS EN CURSOS


@Entity
@Table(name = "alumno_curso_rel", uniqueConstraints = @UniqueConstraint(columnNames = { "alumno_id", "curso_id" }))
public class AlumnoCurso {
	
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.AUTO)	
    private long id;
    
    @Column(name="alumno_id")
    private long alumno_id;
    
    @Column(name="curso_id")
    private long curso_id;

    public long getId() {
        return id;
    }

	public long getAlumno_id() {
		return alumno_id;
	}

	public long getCurso_id() {
		return curso_id;
	}
	
	public void setAlumnoId(long alumno_id) {
		this.alumno_id = alumno_id;
	}
	
	public void setCursoId(long curso_id) {
		this.curso_id = curso_id;
	}
    
}
