package com.proyecto_app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

//@MappedSuperclass
//@Entity
//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Entity
@Table(name = "usuario")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Usuario{  // implements Serializable{

  //private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id")
  protected long id;
  
  //@NotNull
  @Column(name="login")
  protected String login;
  
  //@NotNull
  @Column(name="password")
  protected String password;
  

  @Column(name="nombre")
  protected String nombre;
	
  @Column(name="apellidos")
  protected String apellidos;
	
  @Column(name="dni")
  protected String dni;
	
  @Column(name="email")
  protected String email;
	
  @Column(name="telefono")
  protected String telefono;
  
  @GeneratedValue
  @Column(name="create_date")
  protected String create_date;

  @Column(name="update_date")
  protected String update_date;


  
  /*
  protected Usuario() { }

  protected Usuario(long id) { 
    this.id = id;
  }
  
  protected Usuario(String login, String password) {
    this.login = login;
    this.password = password;
	System.out.println("en constructor Usuario");
    
    Date fecha = new Date();
    this.create_date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(fecha);
  }*/


	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getCreate_date() {
		return create_date;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getUpdate_date() {
		return update_date;
	}
	
	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}
	
	public long getId() {
		return id;
	}


}