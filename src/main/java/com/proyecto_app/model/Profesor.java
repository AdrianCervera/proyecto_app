package com.proyecto_app.model;

import javax.persistence.Column;
//import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

//import org.springframework.stereotype.Service;


@Entity
@Table(name = "profesor")
@PrimaryKeyJoinColumn(referencedColumnName = "id")
public class Profesor extends Alumno{
	

	@Column(name="departamento")
	protected String departamento;

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}


	
}
