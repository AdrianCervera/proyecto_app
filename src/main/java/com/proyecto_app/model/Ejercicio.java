package com.proyecto_app.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "ejercicio")
public class Ejercicio{

  @Id
  @Column(name="id")
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @Column(name="pregunta")
  private String pregunta;

  @Column(name="respuesta")
  private String respuesta;
  
  @Column(name="tema_id")
  private long tema_id;

  @Column(name="examen")
  private boolean examen;

  @Column(name="secuencia")
  private int secuencia;
  
  @GeneratedValue
  @Column(name="create_date")
  private String create_date;

  @Column(name="update_date")
  private String update_date;


	public Ejercicio() { }
	  
	public Ejercicio(String pregunta, String respuesta, long tema_id, boolean examen, int secuencia) {
	  this.pregunta = pregunta;
	  this.respuesta = respuesta;
	  this.tema_id = tema_id;
	  this.examen = examen;	  
	  this.secuencia = secuencia;

	  Date fecha = new Date();
	  this.create_date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(fecha);
	}
	
	public long getId() {
		return id;
	}
	
	public long getTema_id() {
		return tema_id;
	}

	public void setTema_id(long tema_id) {
		this.tema_id = tema_id;
	}
	  
	public String getPregunta() {
		return pregunta;
	}

	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public boolean isExamen() {
		return examen;
	}

	public void setExamen(boolean examen) {
		this.examen = examen;
	}

	public int getSecuencia() {
		return secuencia;
	}

	public void setSecuencia(int secuencia) {
		this.secuencia = secuencia;
	}

	public String getCreate_date() {
		return create_date;
	}
	
	public String getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}
	

}