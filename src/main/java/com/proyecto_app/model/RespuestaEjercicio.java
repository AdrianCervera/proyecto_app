package com.proyecto_app.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "respuesta_ejercicio")
public class RespuestaEjercicio{

  @Id
  @Column(name="id")
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @Column(name="alumno_id")
  private long alumno_id;

  @Column(name="ejercicio_id")
  private long ejercicio_id;

  @Column(name="respuesta")
  private String respuesta;

  @GeneratedValue
  @Column(name="create_date")
  private String create_date;
  
  @Column(name="update_date")
  private String update_date;


	public RespuestaEjercicio() { }
	  
	public RespuestaEjercicio(long alumno_id, long ejercicio_id, String respuesta) {
	  this.alumno_id = alumno_id;
	  this.ejercicio_id = ejercicio_id;
	  this.respuesta = respuesta;
	    
	  Date fecha = new Date();
	  this.create_date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(fecha);
	}
	
	public long getId() {
		return id;
	}
	
	public long getAlumno_id() {
		return alumno_id;
	}

	public long getEjercicio_id() {
		return ejercicio_id;
	}
	
	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public String getCreate_date() {
		return create_date;
	}
	
	public String getUpdate_date() {
		return update_date;
	}

}
