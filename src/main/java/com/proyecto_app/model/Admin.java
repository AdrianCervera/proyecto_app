package com.proyecto_app.model;

import javax.persistence.Column;
//import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

//import org.springframework.stereotype.Service;


@Entity
@Table(name = "administrador")
@PrimaryKeyJoinColumn(referencedColumnName = "id")
public class Admin extends Usuario{
	

	@Column(name="principal")
	protected boolean principal;

	public boolean isPrincipal() {
		return principal;
	}

	public void setPrincipal(boolean principal) {
		this.principal = principal;
	}

	  
	
}
