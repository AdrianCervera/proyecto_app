package com.proyecto_app.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

// TABLA INTERMEDIA DE ALUMNOS INSCRITOS EN CURSOS

@Entity
@Table(name = "evaluacion", uniqueConstraints = @UniqueConstraint(columnNames = { "alumno_id", "ejercicio_id" }))
public class Evaluacion {
	
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.AUTO)	
    private long id;
    
    @Column(name="alumno_id")
    private long alumno_id;
    
    @Column(name="ejercicio_id")
    private long ejercicio_id;

    @Column(name="respuesta")
    private String respuesta;

    @Column(name="calificacion")
    private int calificacion;
    
    @GeneratedValue
    @Column(name="create_date")
    private String create_date;

    @Column(name="update_date")
    private String update_date;

	public long getAlumno_id() {
		return alumno_id;
	}

	public void setAlumno_id(long alumno_id) {
		this.alumno_id = alumno_id;
	}

	public long getEjercicio_id() {
		return ejercicio_id;
	}

	public void setEjercicio_id(long ejercicio_id) {
		this.ejercicio_id = ejercicio_id;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public int getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(int calificacion) {
		this.calificacion = calificacion;
	}

	public String getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}

	public long getId() {
		return id;
	}

	public String getCreate_date() {
		return create_date;
	}
    
}
