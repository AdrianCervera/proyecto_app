package com.proyecto_app.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "examen")
public class Examen{

  @Id
  @Column(name="id")
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;
  
  @Column(name="contenido")
  private String contenido;
  
  //@NotNull
  @Column(name="tema_id")
  private long tema_id;
  
  @GeneratedValue
  @Column(name="create_date")
  private String create_date;

  @Column(name="update_date")
  private String update_date;

  
  public Examen() { }
  
  public Examen(long tema_id) {
    this.tema_id = tema_id;
    
    Date fecha = new Date();
    this.create_date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(fecha);
  }

  public Examen(String contenido, long tema_id) {
	this.contenido = contenido;
	this.tema_id = tema_id;
    
    Date fecha = new Date();
    this.create_date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(fecha);
  }
	
	public long getId() {
		return id;
	}
		
	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public long getTema_id() {
		return tema_id;
	}

	public void setTema_id(long tema_id) {
		this.tema_id = tema_id;
	}

	public String getCreate_date() {
		return create_date;
	}
	
	public String getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}



}