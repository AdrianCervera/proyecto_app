package com.proyecto_app.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

// TABLA INTERMEDIA QUE INDICA LA RELACION USUARIO - ROL 


@Entity
@Table(name = "usuariorol", uniqueConstraints = @UniqueConstraint(columnNames = { "usuarioId", "rolId" }))
public class UsuarioRol {
	
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.AUTO)	
    private long id;
    
    @Column(name="usuarioId")
    private long usuarioId;
    
    @Column(name="rolId")
    private long rolId;
    
	public UsuarioRol() {}   
	
	public UsuarioRol(long usuarioId, long rolId) { 
	  this.usuarioId = usuarioId;
	  this.rolId = rolId;
	}

    public long getId() {
        return id;
    }

	public long getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(long usuarioId) {
		this.usuarioId = usuarioId;
	}

	public long getRolId() {
		return rolId;
	}

	public void setRolId(long rolId) {
		this.rolId = rolId;
	}


	/*@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "login", nullable = false)
    public Usuario getUsuario() {
        return this.usuarioId;
    }

    public void setUsuario(Usuario usuarioId) {
        this.usuarioId = usuarioId;
    }*/
}