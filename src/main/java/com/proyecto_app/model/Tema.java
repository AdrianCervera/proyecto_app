package com.proyecto_app.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "tema")
public class Tema{

  @Id
  @Column(name="id")
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;
  
  @Column(name="titulo")
  private String titulo;
  
  @Column(name="curso_id")
  private long curso_id;

  @Column(name="secuencia")
  private int secuencia;
  
  @GeneratedValue
  @Column(name="create_date")
  private String create_date;

  @Column(name="update_date")
  private String update_date;

  
  public Tema() { }

  public Tema(long id) { 
    this.id = id;
  }
  
  public Tema(String titulo, long curso_id, int secuencia) {
    this.titulo = titulo;
    this.curso_id = curso_id;
    this.secuencia = secuencia;
    
    Date fecha = new Date();
    this.create_date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(fecha);
  }
	
	public long getId() {
		return id;
	}
	
	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}

	public String getTitulo() {
		return titulo;
	}
	
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public long getCurso_id() {
		return curso_id;
	}
	
	public void setCurso_id(long curso_id) {
		this.curso_id = curso_id;
	}

	public int getSecuencia() {
		return secuencia;
	}

	public void setSecuencia(int secuencia) {
		this.secuencia = secuencia;
	}

	public String getCreate_date() {
		return create_date;
	}

	public String getUpdate_date() {
		return update_date;
	}
	

}