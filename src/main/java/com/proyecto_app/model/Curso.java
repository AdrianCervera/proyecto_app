package com.proyecto_app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "curso")
public class Curso{

  @Id
  @Column(name="id")
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;
  
  @Column(name="nombre")
  private String nombre;
  
  @Column(name="profesor_id")
  private long profesor_id;

  @Column(name="fecha_inicio")
  private String fecha_inicio;

  @Column(name="fecha_fin")
  private String fecha_fin;
  
  @GeneratedValue
  @Column(name="create_date")
  private String create_date;

  @Column(name="update_date")
  private String update_date;

	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public long getProfesor_id() {
		return profesor_id;
	}
	
	public void setProfesor_id(long profesor_id) {
		this.profesor_id = profesor_id;
	}
	
	public String getFecha_inicio() {
		return fecha_inicio;
	}
	
	public void setFecha_inicio(String fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}
	
	public String getFecha_fin() {
		return fecha_fin;
	}
	
	public void setFecha_fin(String fecha_fin) {
		this.fecha_fin = fecha_fin;
	}
	
	public String getUpdate_date() {
		return update_date;
	}
	
	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}
	
	public long getId() {
		return id;
	}
	
	public String getCreate_date() {
		return create_date;
	}

  
  
}