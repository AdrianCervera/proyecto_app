package com.proyecto_app.model;

import javax.persistence.Column;
//import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

//import org.springframework.stereotype.Service;


@Entity
@Table(name = "alumno")
@PrimaryKeyJoinColumn(referencedColumnName = "id")
public class Alumno extends Usuario{
	

	@Column(name="habilitado")
	protected boolean habilitado;

	public boolean isHabilitado() {
		return habilitado;
	}

	public void setHabilitado(boolean habilitado) {
		this.habilitado = habilitado;
	}
	  
	  
	  
	  /*
	
	  protected Alumno() { }
	  
	  public Alumno(long id) { 
	    this.id = id;
	  }
	  
	  public Alumno(String login, String password, boolean habilitado) {
		  super(login, password, habilitado);
		  System.out.println("constructor Alumno");
		  
	  }*/
	  /*
	  public Alumno(String login, String password, String nombre, boolean habilitado, String apellidos, String dni, String email, String telefono) {

		  super(login, password, habilitado);
		  System.out.println(nombre);
		  this.nombre = nombre;
		  this.apellidos = apellidos;
		  this.dni = dni;
		  this.email = email;
		  this.telefono = telefono;
		  
	  }*/

	
}
