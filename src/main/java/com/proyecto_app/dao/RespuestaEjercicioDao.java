package com.proyecto_app.dao;

import java.util.List;

import com.proyecto_app.model.RespuestaEjercicio;

public interface RespuestaEjercicioDao{

	List<RespuestaEjercicio> obtenerRespuestas(long tema_id, long alumno_id, boolean examen);
	
	RespuestaEjercicio insert(RespuestaEjercicio respuesta);

	void update(RespuestaEjercicio respuesta);

	void delete(long id);

	RespuestaEjercicio get(long tema_id, long alumno_id);
	
}
