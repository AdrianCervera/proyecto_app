package com.proyecto_app.dao;

import com.proyecto_app.model.Usuario;
import java.util.List;

public interface UsuarioDao{

	List<Usuario> findAllUsers();
	
	Usuario insert(Usuario usuario);

	void update(Usuario usuario);

	void delete(long id);

	Usuario get(long id);
	
	Usuario get(String login);

}
