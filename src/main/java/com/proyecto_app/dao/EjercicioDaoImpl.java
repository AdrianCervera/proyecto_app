package com.proyecto_app.dao;

import com.proyecto_app.model.Ejercicio;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class EjercicioDaoImpl implements EjercicioDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public Ejercicio insert(Ejercicio ejercicio) {
		
		Session session = this.sessionFactory.getCurrentSession();
		System.out.println(ejercicio.getTema_id());
		System.out.println(ejercicio.getPregunta());
		session.persist(ejercicio);
		return ejercicio;
		
	}

	@Override
	public void update(Ejercicio ejercicio) {
		
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentTime = sdf.format(new Date());
		ejercicio.setUpdate_date(currentTime);
		
		Session session = this.sessionFactory.getCurrentSession();
		session.update(ejercicio);
		
	}

	@Override
	public void delete(long id) {

		Session session = this.sessionFactory.getCurrentSession();
		Ejercicio ejercicio = (Ejercicio) session.load(Ejercicio.class, new Long(id));
		if (ejercicio != null) {
			session.delete(ejercicio);
		}
		
	}

	@Override
	public Ejercicio get(long id) {

		Session session = this.sessionFactory.getCurrentSession();
		Ejercicio ejercicio = (Ejercicio) session.get(Ejercicio.class, new Long(id));
		return ejercicio;
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Ejercicio> obtenerEjercicios(long tema_id, boolean examen) {
		
		Session session = this.sessionFactory.getCurrentSession();
	
		String hql = "from Ejercicio WHERE tema_id = :tema_id AND examen = :examen order by secuencia";
		Query query = session.createQuery(hql);
		query.setParameter("tema_id", tema_id);
		query.setParameter("examen", examen);
		
		List<Ejercicio> ejercicios = query.list();
		return ejercicios;
	}

	
}
