package com.proyecto_app.dao;

import java.util.Date;
import java.util.List;
import com.proyecto_app.model.Tema;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TemaDaoImpl implements TemaDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@SuppressWarnings("unchecked")
	public List<Tema> findAllTemas() {
		
		Session session = this.sessionFactory.getCurrentSession();		
		List<Tema> lista_temas = session.createQuery("from Tema").list();
		return lista_temas;
		
	}

	@SuppressWarnings("unchecked")
	public List<Tema> obtenerTemasCurso(long curso_id) {
		
		Session session = this.sessionFactory.getCurrentSession();		
	
		String hql = "from Tema WHERE curso_id = :curso_id order by secuencia";
		Query query = session.createQuery(hql);
		query.setParameter("curso_id", curso_id);
		
		List<Tema> lista_temas = query.list();
		return lista_temas;
	}

	public Tema insert(Tema tema) {
		
		Session session = this.sessionFactory.getCurrentSession();
		System.out.println(tema.getCurso_id());
		System.out.println(tema.getTitulo());
		session.persist(tema);
		return tema;
		
	}

	@Override
	public void update(Tema tema) {
		
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentTime = sdf.format(new Date());
		tema.setUpdate_date(currentTime);

		Session session = this.sessionFactory.getCurrentSession();
		session.update(tema);
		
	}

	@Override
	public void delete(long id) {

		Session session = this.sessionFactory.getCurrentSession();
		Tema tema = (Tema) session.load(Tema.class, new Long(id));
		if (tema != null) {
			session.delete(tema);
		}
		
	}

	@Override
	public Tema get(long id) {

		Session session = this.sessionFactory.getCurrentSession();
		Tema tema = (Tema) session.get(Tema.class, new Long(id));
		return tema;
		
	}

	
}
