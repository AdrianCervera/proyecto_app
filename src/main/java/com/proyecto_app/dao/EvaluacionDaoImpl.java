package com.proyecto_app.dao;

import java.util.Date;
import java.util.List;

import com.proyecto_app.model.Evaluacion;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class EvaluacionDaoImpl implements EvaluacionDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public void insert(Evaluacion evaluacion) {
		
		// aseguramos por defecto ejercicio no esta evaluado (no deberia ser necesario)
		evaluacion.setCalificacion(-1);
		
		Session session = this.sessionFactory.getCurrentSession();
		System.out.println(evaluacion);
		session.persist(evaluacion);
		
	}

	@Override
	public Evaluacion get(long alumno_id, long ejercicio_id) {

		Session session = this.sessionFactory.getCurrentSession();
		
		String hql = "from Evaluacion where alumno_id = :alumno_id and ejercicio_id = :ejercicio_id";
		Query query = session.createQuery(hql);
		query.setParameter("alumno_id", alumno_id);
		query.setParameter("ejercicio_id", ejercicio_id);
		List<Evaluacion> evaluacion = query.list();

		System.out.println(evaluacion);

		if (evaluacion.isEmpty()){
			System.out.println("No existe la evaluacion");
			return null;
		}

		return evaluacion.get(0);
	}
	
	
	@Override
	public void update(Evaluacion evaluacion) {

		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentTime = sdf.format(new Date());
		evaluacion.setUpdate_date(currentTime);
		
		Session session = this.sessionFactory.getCurrentSession();
		session.update(evaluacion);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void delete(long alumno_id, long ejercicio_id) {
		/*
		Session session = this.sessionFactory.getCurrentSession();
		Evaluacion alumno = (Evaluacion) session.load(Evaluacion.class, new Long(ejercicio_id));
		if (alumno != null) {
			session.delete(alumno);
		}*/
		
		Session session = this.sessionFactory.getCurrentSession();
		
		String hql = "from Evaluacion where alumno_id = :alumno_id and ejercicio_id = :ejercicio_id";
		Query query = session.createQuery(hql);
		query.setParameter("alumno_id", alumno_id);
		query.setParameter("ejercicio_id", ejercicio_id);
		System.out.println(query);
		List<Evaluacion> evaluaciones = query.list();

		System.out.println(evaluaciones);
		
		if(!evaluaciones.isEmpty()){
			System.out.println("Alumno curso a eliminar:");
			System.out.println(evaluaciones.get(0).getAlumno_id());
			System.out.println(evaluaciones.get(0).getEjercicio_id());
			session.delete(evaluaciones.get(0));
			System.out.println("registro evaluacion eliminado");
		}
		
	}

	@SuppressWarnings("unchecked")
	public List<Evaluacion> getEvaluaciones(long alumno_id) {
		
		Session session = this.sessionFactory.getCurrentSession();		
		List<Evaluacion> lista_alumnos = session.createQuery("from Evaluacion").list();

		return lista_alumnos;
		
	}
	
}
