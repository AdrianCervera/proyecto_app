package com.proyecto_app.dao;

import com.proyecto_app.model.Evaluacion;
import java.util.List;

public interface EvaluacionDao{

	Evaluacion get(long alumno_id, long ejercicio_id);

	void insert(Evaluacion evaluacion);
	
	void update(Evaluacion evaluacion);
	
	void delete(long alumno_id, long ejercicio_id);

	List<Evaluacion> getEvaluaciones(long alumno_id);  // devuelve evaluaciones 
	
}
