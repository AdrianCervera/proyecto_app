package com.proyecto_app.dao;

import com.proyecto_app.model.Examen;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ExamenDaoImpl implements ExamenDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public Examen insert(Examen examen) {
		
		Session session = this.sessionFactory.getCurrentSession();
		System.out.println(examen.getTema_id());
		System.out.println(examen.getContenido());
		session.persist(examen);
		return examen;
		
	}

	@Override
	public void update(Examen examen) {

		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentTime = sdf.format(new Date());
		examen.setUpdate_date(currentTime);
		
		Session session = this.sessionFactory.getCurrentSession();
		session.update(examen);
		
	}

	@Override
	public void delete(long id) {

		Session session = this.sessionFactory.getCurrentSession();
		Examen examen = (Examen) session.load(Examen.class, new Long(id));
		if (examen != null) {
			session.delete(examen);
		}
		
	}

	@Override
	public Examen get(long id) {

		Session session = this.sessionFactory.getCurrentSession();
		Examen examen = (Examen) session.get(Examen.class, new Long(id));
		return examen;
		
	}

	
}
