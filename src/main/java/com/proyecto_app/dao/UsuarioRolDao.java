package com.proyecto_app.dao;

import com.proyecto_app.model.Rol;
import com.proyecto_app.model.UsuarioRol;
import java.util.List;

public interface UsuarioRolDao{

	List<UsuarioRol> findAll();

	void insert(UsuarioRol rol);
	
	void delete(UsuarioRol rol);

	List<Rol> getRoles(long id);  // devuelve roles de un usuario
}
