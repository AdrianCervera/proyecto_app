package com.proyecto_app.dao;

import com.proyecto_app.model.Admin;
import java.util.List;

public interface AdminDao{

	List<Admin> obtenerAdmins();
	
	Admin insert(Admin admin);

	void update(Admin admin);

	void delete(long id);

	Admin get(long id);
	
	Admin get(String login);

}
