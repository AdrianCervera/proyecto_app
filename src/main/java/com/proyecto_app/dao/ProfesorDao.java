package com.proyecto_app.dao;

import com.proyecto_app.model.Profesor;
import java.util.List;

public interface ProfesorDao{

	List<Profesor> obtenerProfesors();
	
	Profesor insert(Profesor profesor);

	void update(Profesor profesor);

	void delete(long id);

	Profesor get(long id);
	
	Profesor get(String login);

}
