package com.proyecto_app.dao;

import java.util.Date;
import java.util.List;
import com.proyecto_app.model.Admin;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AdminDaoImpl implements AdminDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@SuppressWarnings("unchecked")
	public List<Admin> obtenerAdmins() {
		
		Session session = this.sessionFactory.getCurrentSession();		
		List<Admin> lista_admins = session.createQuery("from Admin").list();
		System.out.println("Lista Admins:\n" + lista_admins);
		return lista_admins;
		
	}

	public Admin insert(Admin admin) {
		
		Session session = this.sessionFactory.getCurrentSession();
		System.out.println(admin);
		session.persist(admin);
		return admin;
		
	}

	@Override
	public void update(Admin admin) {

		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentTime = sdf.format(new Date());
		admin.setUpdate_date(currentTime);
		
		Session session = this.sessionFactory.getCurrentSession();
		session.update(admin);
		
	}

	@Override
	public void delete(long id) {

		Session session = this.sessionFactory.getCurrentSession();
		Admin admin = (Admin) session.load(Admin.class, new Long(id));
		if (admin != null) {
			session.delete(admin);
		}
		
	}

	@Override
	public Admin get(long id) {

		Session session = this.sessionFactory.getCurrentSession();
		Admin admin = (Admin) session.get(Admin.class, new Long(id));
		return admin;
		
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public Admin get(String login) {

		Session session = this.sessionFactory.getCurrentSession();
		
		String hql = "from Admin where login = :login";
		Query query = session.createQuery(hql);
		query.setParameter("login", login);
		List<Admin> admins = query.list();
		
		System.out.println(admins);
		
		if (admins.isEmpty()){
			System.out.println("Admin dao get login. Lista de admins vacía");
			return null;
		}
		
		return admins.get(0);
		
	}
	
}
