package com.proyecto_app.dao;

import java.util.Date;
import java.util.List;
import com.proyecto_app.model.Curso;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CursoDaoImpl implements CursoDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@SuppressWarnings("unchecked")
	public List<Curso> obtenerCursos() {
		
		Session session = this.sessionFactory.getCurrentSession();		
		List<Curso> lista_cursos = session.createQuery("from Curso").list();
		return lista_cursos;
		
	}

	@SuppressWarnings("unchecked")
	public List<Curso> obtenerCursosProfesor(long profesor_id) {  // TODO
				
		Session session = this.sessionFactory.getCurrentSession();
				
		String hql = "from Curso where profesor_id = :profesor_id";
		Query query = session.createQuery(hql);
		query.setParameter("profesor_id", profesor_id);
		List<Curso> lista_cursos = query.list();
		
		return lista_cursos;
	}

	public Curso insert(Curso curso) {
		
		Session session = this.sessionFactory.getCurrentSession();
		System.out.println(curso.getId());
		System.out.println(curso.getNombre());
		session.persist(curso);
		return curso;
		
	}

	@Override
	public void update(Curso curso) {

		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentTime = sdf.format(new Date());
		curso.setUpdate_date(currentTime);
		
		Session session = this.sessionFactory.getCurrentSession();
		session.update(curso);
		
	}

	@Override
	public void delete(long id) {

		Session session = this.sessionFactory.getCurrentSession();
		Curso curso = (Curso) session.load(Curso.class, new Long(id));
		if (curso != null) {
			session.delete(curso);
		}
		
	}

	@Override
	public Curso get(long id) {

		Session session = this.sessionFactory.getCurrentSession();
		Curso curso = (Curso) session.get(Curso.class, new Long(id));
		return curso;
		
	}

	
}
