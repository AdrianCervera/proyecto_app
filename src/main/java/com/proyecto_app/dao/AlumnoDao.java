package com.proyecto_app.dao;

import com.proyecto_app.model.Alumno;
import java.util.List;

public interface AlumnoDao{

	List<Alumno> obtenerAlumnos();
	
	Alumno insert(Alumno alumno);

	void update(Alumno alumno);

	void delete(long id);

	Alumno get(long id);
	
	Alumno get(String login);

}
