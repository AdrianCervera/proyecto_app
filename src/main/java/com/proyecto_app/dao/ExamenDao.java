package com.proyecto_app.dao;

import com.proyecto_app.model.Examen;

public interface ExamenDao{

	Examen insert(Examen examen);

	void update(Examen examen);

	void delete(long id);

	Examen get(long id);

}
