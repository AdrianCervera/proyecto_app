package com.proyecto_app.dao;

import com.proyecto_app.model.Tema;
import java.util.List;

public interface TemaDao{

	List<Tema> findAllTemas();

	List<Tema> obtenerTemasCurso(long curso_id);
	
	Tema insert(Tema tema);

	void update(Tema tema);

	void delete(long id);

	Tema get(long id);

}
