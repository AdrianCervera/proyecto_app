package com.proyecto_app.dao;

import java.util.List;

import com.proyecto_app.model.Ejercicio;

public interface EjercicioDao{

	List<Ejercicio> obtenerEjercicios(long tema_id, boolean examen);
	
	Ejercicio insert(Ejercicio ejercicio);

	void update(Ejercicio ejercicio);

	void delete(long id);

	Ejercicio get(long id);

}
