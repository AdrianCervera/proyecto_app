package com.proyecto_app.dao;

import java.util.List;

import com.proyecto_app.model.Usuario;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UsuarioDaoImpl implements UsuarioDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@SuppressWarnings("unchecked")
	public List<Usuario> findAllUsers() {
		
		Session session = this.sessionFactory.getCurrentSession();		
		List<Usuario> lista_usuarios = session.createQuery("from Usuario").list();
		System.out.println("Lista usuarios:\n" + lista_usuarios);
		return lista_usuarios;
		
	}

	public Usuario insert(Usuario usuario) {
		
		Session session = this.sessionFactory.getCurrentSession();
		System.out.println(usuario);
		session.persist(usuario);
		return usuario;
		
	}

	@Override
	public void update(Usuario usuario) {

		Session session = this.sessionFactory.getCurrentSession();
		session.update(usuario);
		
	}

	@Override
	public void delete(long id) {

		Session session = this.sessionFactory.getCurrentSession();
		Usuario usuario = (Usuario) session.load(Usuario.class, new Long(id));
		if (usuario != null) {
			session.delete(usuario);
		}
		
	}

	@Override
	public Usuario get(long id) {

		Session session = this.sessionFactory.getCurrentSession();
		Usuario usuario = (Usuario) session.get(Usuario.class, new Long(id));
		return usuario;
		
	}
	
	// SE USA PARA EL LOGIN - IDENTIFICA CUALQUEIR TIPO DE USUARIO

	@SuppressWarnings("unchecked")
	@Override
	public Usuario get(String login) {
		
		// opcion podria identif. por DNI -> comprobar si string es dni valido - numeric, car final, validator - esto en controller no en DAO

		Session session = this.sessionFactory.getCurrentSession();
		
		String hql = "from Usuario where login = :login";
		Query query = session.createQuery(hql);
		query.setParameter("login", login);
		List<Usuario> usuarios = query.list();
		
		System.out.println(usuarios);
		
		if (usuarios.isEmpty()){
			System.out.println("No existe usuario");
			return null;
		}
		
		return usuarios.get(0); // solo deberia haber un usuario con un login -> un indice en la lista
		
		
	}
	
}
