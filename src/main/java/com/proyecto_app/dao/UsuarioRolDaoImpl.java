package com.proyecto_app.dao;

import java.util.ArrayList;
import java.util.List;
import com.proyecto_app.model.Rol;
import com.proyecto_app.model.UsuarioRol;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UsuarioRolDaoImpl implements UsuarioRolDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UsuarioRol> findAll() {

		Session session = this.sessionFactory.getCurrentSession();		
		List<UsuarioRol> roles = session.createQuery("from Rol").list();
		System.out.println("Lista roles:\n" + roles);
		return roles;
		
	}

	@Override
	public void insert(UsuarioRol rol) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(rol);		
	}

	@Override
	public void delete(UsuarioRol rol) {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(rol);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Rol> getRoles(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		
		System.out.println("usuariorol dao");
		
		String hql = "from UsuarioRol where usuarioId = :usuarioId"; // TODO
		Query query = session.createQuery(hql);
		query.setParameter("usuarioId", id); // TODO
		List<UsuarioRol> usuarioRoles = query.list();


		List<Rol> roles = new ArrayList<Rol>();
		List<Rol> rolObtenido = null;
		System.out.println(usuarioRoles);
		
		for(UsuarioRol usuarioRol : usuarioRoles){
			hql = "from Rol where id = :id";
			query = session.createQuery(hql);
			query.setParameter("id", usuarioRol.getRolId());
			rolObtenido = (List<Rol>)query.list();
			System.out.println(rolObtenido.get(0));
			if (!rolObtenido.isEmpty()){
				roles.add((Rol)rolObtenido.get(0)); // id unico para cada rol
			}
			
		}
		System.out.println("Roles return:");

		System.out.println(roles);
		return roles;
	}
	
}
