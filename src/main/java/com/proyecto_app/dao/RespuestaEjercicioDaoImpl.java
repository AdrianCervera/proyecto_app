package com.proyecto_app.dao;

import com.proyecto_app.model.RespuestaEjercicio;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RespuestaEjercicioDaoImpl implements RespuestaEjercicioDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public RespuestaEjercicio insert(RespuestaEjercicio respuesta) {
		
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(respuesta);
		return respuesta;
		
	}

	@Override
	public void update(RespuestaEjercicio respuesta) {

		Session session = this.sessionFactory.getCurrentSession();
		session.update(respuesta);
		
	}

	@Override
	public void delete(long id) {

		Session session = this.sessionFactory.getCurrentSession();
		RespuestaEjercicio respuesta = (RespuestaEjercicio) session.load(RespuestaEjercicio.class, new Long(id));
		if (respuesta != null) {
			session.delete(respuesta);
		}
		
	}

	@Override
	public RespuestaEjercicio get(long alumno_id, long ejercicio_id) {

		Session session = this.sessionFactory.getCurrentSession();
		//RespuestaEjercicio respuesta = (RespuestaEjercicio) session.get(RespuestaEjercicio.class, new Long(id));
		
		
		String hql = "from RespuestaEjercicio WHERE alumno_id = :alumno_id AND ejercicio_id = :ejercicio_id";
		Query query = session.createQuery(hql);
		query.setParameter("alumno_id", alumno_id);
		query.setParameter("ejercicio_id", ejercicio_id);
		
		RespuestaEjercicio respuesta = (RespuestaEjercicio)query.uniqueResult();
		
		System.out.println("=========       RespuestaEjercicio get     ==========================");
		System.out.println("===================================================================\n");
		System.out.println(respuesta);
		System.out.println("\n===================================================================");
		System.out.println("=====================================================================");
			
		return respuesta;
		
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<RespuestaEjercicio> obtenerRespuestas(long tema_id, long alumno_id, boolean examen) {
		
		Session session = this.sessionFactory.getCurrentSession();
			
		String hql = "from RespuestaEjercicio R, Ejercicio E, Tema T WHERE R.tema_id = E.tema:id AND E.tema_id = :tema_id AND E.examen = :examen";
		Query query = session.createQuery(hql);
		query.setParameter("tema_id", tema_id);
		query.setParameter("examen", examen);
		
		List<RespuestaEjercicio> respuestas = query.list();
		
		System.out.println("=====   List<RespuestaEjercicio> obtenerRespuestas   ================");
		System.out.println("===================================================================\n");
		System.out.println(respuestas);
		System.out.println("\n===================================================================");
		System.out.println("=====================================================================");

		return respuestas;
	}
	
}
