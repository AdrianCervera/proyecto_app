package com.proyecto_app.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.proyecto_app.model.Alumno;
import com.proyecto_app.model.Curso;
import com.proyecto_app.service.AlumnoService;
import com.proyecto_app.service.CursoService;
import com.proyecto_app.model.AlumnoCurso;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AlumnoCursoDaoImpl implements AlumnoCursoDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private CursoService cursoService;
	
	@Autowired
	private AlumnoService alumnoService;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public void insert(AlumnoCurso alumnoCurso) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(alumnoCurso);		
	}


	@SuppressWarnings("unchecked")
	@Override
	public void delete(long alumno_id, long curso_id) {
		Session session = this.sessionFactory.getCurrentSession();
		
		String hql = "from AlumnoCurso where alumno_id = :alumno_id and curso_id = :curso_id";
		Query query = session.createQuery(hql);
		query.setParameter("alumno_id", alumno_id);
		query.setParameter("curso_id", curso_id);
		System.out.println(query);
		List<AlumnoCurso> alumnoCursos = query.list();

		System.out.println(alumnoCursos);
		
		if(!alumnoCursos.isEmpty()){
			System.out.println("Alumno curso a eliminar:");
			System.out.println(alumnoCursos.get(0).getAlumno_id());
			System.out.println(alumnoCursos.get(0).getCurso_id());
			session.delete(alumnoCursos.get(0));
			System.out.println("Alumno curso eliminado");
		}
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Curso> getCursos(long alumno_id) {
		// obtener lista cursos en los que esta inscrito un alumno

		System.out.println("dao cursos");
		Session session = this.sessionFactory.getCurrentSession();
		
		String hql = "from AlumnoCurso where alumno_id = :alumno_id";
		Query query = session.createQuery(hql);
		System.out.println(hql);
		query.setParameter("alumno_id", alumno_id);
		System.out.println(query);
		List<AlumnoCurso> alumnoCursos = query.list();
		
		Curso curso = null;
		List<Curso> cursos = new ArrayList<Curso>();
		for(AlumnoCurso alumnoCurso : alumnoCursos){
			curso = cursoService.get(alumnoCurso.getCurso_id());
			if(curso != null){
				cursos.add(curso);
			}
		}
		
		return cursos;
	}	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Curso> getCursosNoInscrito(long alumno_id) {

		Session session = this.sessionFactory.getCurrentSession();
		
		List<Curso> cursos_alumno = getCursos(alumno_id);
		List<Long> lista_id_cursos = new ArrayList<>();
		List<Curso> cursos_no_inscritos = new ArrayList<>();
		

		// obtenemos el listado de IDs de cursos en los que esta inscrito el alumno	
		for(Curso curso : cursos_alumno){
			lista_id_cursos.add(curso.getId());  // array de IDs de cursos inscritos
			System.out.println("CURSOS INSCRITOS:");
            System.out.println(curso.getId() + "- " + curso.getNombre());
		}	
		
		if(!lista_id_cursos.isEmpty()){
			// buscamos todos los cursos que no esten en la lista de IDs
			String hql = "from Curso where id not in (:lista_id_cursos)";
			Query query = session.createQuery(hql);
			query.setParameterList("lista_id_cursos", lista_id_cursos);
			cursos_no_inscritos = query.list();
			
			for(Curso curso: cursos_no_inscritos){  // para pruebas
				System.out.println("CURSOS NO INSCRITOS:");
				System.out.println(curso.getId() + ". " + curso.getNombre());
			}
		} else {
			// si la lista de cursos inscritos esta vacia, se retornan todos (si el array esta vacio, hibernate peta)
			String hql = "from Curso";
			Query query = session.createQuery(hql);
			cursos_no_inscritos = query.list();
			
		}
		
		return cursos_no_inscritos;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Alumno> getAlumnos(long curso_id) {
		// obtener lista alumnos inscritos en un curso

		Session session = this.sessionFactory.getCurrentSession();
		
		String hql = "from AlumnoCurso where curso_id = :curso_id";
		Query query = session.createQuery(hql);
		System.out.println(hql);
		query.setParameter("curso_id", curso_id);
		System.out.println(query);
		List<AlumnoCurso> alumnoCursos = query.list();
		
		Alumno alumno = null;
		List<Alumno> alumnos = new ArrayList<Alumno>();
		for(AlumnoCurso alumnoCurso : alumnoCursos){
			alumno = alumnoService.get(alumnoCurso.getAlumno_id());
			if(alumno != null){
				alumnos.add(alumno);				
			}
		}
		
		return alumnos;

		
	}
	
	
}
