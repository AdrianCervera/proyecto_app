package com.proyecto_app.dao;

import com.proyecto_app.model.Curso;
import java.util.List;

public interface CursoDao{
	
	List<Curso> obtenerCursos();

	List<Curso> obtenerCursosProfesor(long profesor_id);
	
	Curso insert(Curso curso);

	void update(Curso curso);

	void delete(long id);

	Curso get(long id);

}
