package com.proyecto_app.dao;

import java.util.Date;
import java.util.List;
import com.proyecto_app.model.Alumno;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AlumnoDaoImpl implements AlumnoDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@SuppressWarnings("unchecked")
	public List<Alumno> obtenerAlumnos() {
		
		Session session = this.sessionFactory.getCurrentSession();		
		List<Alumno> lista_alumnos = session.createQuery("from Alumno").list();
		System.out.println("Lista Alumnos:\n" + lista_alumnos);
		return lista_alumnos;
		
	}

	public Alumno insert(Alumno alumno) {
		
		Session session = this.sessionFactory.getCurrentSession();
		System.out.println(alumno);
		session.persist(alumno);
		return alumno;
		
	}

	@Override
	public void update(Alumno alumno) {

		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentTime = sdf.format(new Date());
		alumno.setUpdate_date(currentTime);
		
		Session session = this.sessionFactory.getCurrentSession();
		session.update(alumno);
		
	}

	@Override
	public void delete(long id) {

		Session session = this.sessionFactory.getCurrentSession();
		Alumno alumno = (Alumno) session.load(Alumno.class, new Long(id));
		if (alumno != null) {
			session.delete(alumno);
		}
		
	}

	@Override
	public Alumno get(long id) {

		Session session = this.sessionFactory.getCurrentSession();
		Alumno alumno = (Alumno) session.get(Alumno.class, new Long(id));
		return alumno;
		
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public Alumno get(String login) {

		System.out.println("alumno dao get login");
		
		Session session = this.sessionFactory.getCurrentSession();
		
		String hql = "from Alumno where login = :login";    // TODO tabla Usuario
		Query query = session.createQuery(hql);
		System.out.println(hql);
		query.setParameter("login", login);
		System.out.println(query);
		List<Alumno> alumnos = query.list();
		
		System.out.println(alumnos);
		
		if (alumnos.isEmpty()){
			System.out.println("Alumno dao get login. Lista de alumnos vacía");
			return null;
		}
		
		return alumnos.get(0); // solo deberia haber un usuario con un login -> un indice en la lista
		
	}
	
}
