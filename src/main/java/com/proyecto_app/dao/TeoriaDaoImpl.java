package com.proyecto_app.dao;

import com.proyecto_app.model.Teoria;
//import com.proyecto_app.model.Usuario;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TeoriaDaoImpl implements TeoriaDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public Teoria insert(Teoria teoria) {
		
		Session session = this.sessionFactory.getCurrentSession();
		System.out.println(teoria.getTema_id());
		System.out.println(teoria.getContenido());
		session.persist(teoria);
		return teoria;
		
	}

	@Override
	public void update(Teoria teoria) {

		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentTime = sdf.format(new Date());
		teoria.setUpdate_date(currentTime);

		Session session = this.sessionFactory.getCurrentSession();
		session.update(teoria);
		
	}

	@Override
	public void delete(long id) {

		Session session = this.sessionFactory.getCurrentSession();
		Teoria teoria = (Teoria) session.load(Teoria.class, new Long(id));
		if (teoria != null) {
			session.delete(teoria);
		}
		
	}

	@Override
	public Teoria get(long id) {
		
		System.out.println("============\nEn teoria dao get. Id:");
		System.out.println(id);

		Session session = this.sessionFactory.getCurrentSession();
		Teoria teoria = (Teoria) session.get(Teoria.class, new Long(id));
		System.out.println(teoria);
		return teoria;
		
	}

	@SuppressWarnings("unchecked")
	public Teoria get_tema(long id_tema) {
		
		System.out.println("Obtener teoria por ID del tema, id_tema:");
		System.out.println(id_tema);

		Session session = this.sessionFactory.getCurrentSession();
		
		String hql = "from Teoria where tema_id = :id_tema";
		Query query = session.createQuery(hql);
		query.setParameter("id_tema", id_tema);
		List<Teoria> teoria = query.list();

		System.out.println(teoria);

		if (teoria.isEmpty()){
			System.out.println("No existe teoria");
			return null;
		}

		return teoria.get(0); // solo deberia haber una registro de teoria, retornamos el primero
		
	}
	
}
