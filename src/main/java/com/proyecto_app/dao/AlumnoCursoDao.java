package com.proyecto_app.dao;

import com.proyecto_app.model.Alumno;
import com.proyecto_app.model.AlumnoCurso;
import com.proyecto_app.model.Curso;
import java.util.List;

public interface AlumnoCursoDao{

	void insert(AlumnoCurso alumnoCurso);
	
	void delete(long alumno_id, long curso_id);

	List<Curso> getCursos(long alumno_id);  // devuelve cursos en los que esta inscrito un alumno

	List<Curso> getCursosNoInscrito(long alumno_id);  // devuelve cursos en los que no esta inscrito un alumno

	List<Alumno> getAlumnos(long curso_id);  // devuelve alumnos inscritos en un curso
	
}
