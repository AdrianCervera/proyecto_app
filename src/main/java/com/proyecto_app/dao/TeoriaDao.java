package com.proyecto_app.dao;

import com.proyecto_app.model.Teoria;

public interface TeoriaDao{

	Teoria insert(Teoria teoria);

	void update(Teoria teoria);

	void delete(long id);

	Teoria get(long id);

	Teoria get_tema(long id_tema);
}
