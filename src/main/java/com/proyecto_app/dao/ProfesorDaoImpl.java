package com.proyecto_app.dao;

import java.util.Date;
import java.util.List;
import com.proyecto_app.model.Profesor;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ProfesorDaoImpl implements ProfesorDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@SuppressWarnings("unchecked")
	public List<Profesor> obtenerProfesors() {
		
		Session session = this.sessionFactory.getCurrentSession();		
		List<Profesor> lista_profesors = session.createQuery("from Profesor").list();
		System.out.println("Lista Profesors:\n" + lista_profesors);
		return lista_profesors;
		
	}

	public Profesor insert(Profesor profesor) {
		
		Session session = this.sessionFactory.getCurrentSession();
		System.out.println(profesor);
		session.persist(profesor);
		return profesor;
		
	}

	@Override
	public void update(Profesor profesor) {

		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentTime = sdf.format(new Date());
		profesor.setUpdate_date(currentTime);
		
		Session session = this.sessionFactory.getCurrentSession();
		session.update(profesor);
		
	}

	@Override
	public void delete(long id) {

		Session session = this.sessionFactory.getCurrentSession();
		Profesor profesor = (Profesor) session.load(Profesor.class, new Long(id));
		if (profesor != null) {
			session.delete(profesor);
		}
		
	}

	@Override
	public Profesor get(long id) {

		Session session = this.sessionFactory.getCurrentSession();
		Profesor profesor = (Profesor) session.get(Profesor.class, new Long(id));
		return profesor;
		
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public Profesor get(String login) {

		Session session = this.sessionFactory.getCurrentSession();
		
		String hql = "from Profesor where login = :login";
		Query query = session.createQuery(hql);
		query.setParameter("login", login);
		List<Profesor> profesors = query.list();
		
		System.out.println(profesors);
		
		if (profesors.isEmpty()){
			System.out.println("Profesor dao get login. Lista de profesores vacía");
			return null;
		}
		
		return profesors.get(0);
		
	}
	
}
