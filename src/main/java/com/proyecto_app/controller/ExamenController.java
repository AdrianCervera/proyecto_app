package com.proyecto_app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.proyecto_app.model.Examen;
import com.proyecto_app.service.ExamenService;

@Controller
public class ExamenController {
	
	@Autowired
	private ExamenService examenService;
	
	
	// OBTENER EXAMEN POR ID
	@RequestMapping(value = "/resource/examen/{id}", method = RequestMethod.GET, produces = "application/json") 
	public ResponseEntity <Examen> obtenerExamen(@PathVariable String id){
		System.out.println("Examencontroller java, id recibido:");
		System.out.println(id);
		
		long id_examen = Long.parseLong(id);
		System.out.println(id_examen);

		Examen examen = null;
		
		try{
			examen = examenService.get(id_examen);
			System.out.println("============\nExamen getao controller");
            System.out.println(examen.getContenido());
            return new ResponseEntity<Examen>(examen, HttpStatus.OK);
            
		} catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<Examen>(examen, HttpStatus.NOT_FOUND);
        }
	}

	
	// CREAR Examen
  @RequestMapping(value = "/auth/examen/", method = RequestMethod.POST)
  public ResponseEntity<Void> crearExamen(@RequestBody Examen examen, UriComponentsBuilder ucBuilder) {
      System.out.println("Creating Examen ");
      System.out.println("Creating Examen " + examen.getContenido());

      examenService.insert(examen);

      HttpHeaders headers = new HttpHeaders();
      headers.setLocation(ucBuilder.path("/examen/{id}").buildAndExpand(examen.getId()).toUri());
      return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
  }
  
  

  
}