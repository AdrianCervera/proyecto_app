package com.proyecto_app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.proyecto_app.model.Tema;
import com.proyecto_app.model.Teoria;
import com.proyecto_app.service.TemaService;
import com.proyecto_app.service.TeoriaService;

@Controller
public class TemaController {
	
	@Autowired
	private TemaService temaService;

	@Autowired
	private TeoriaService teoriaService;
		
	/*@RequestMapping(value = "/json", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<String> bar() {
	    final HttpHeaders httpHeaders= new HttpHeaders();
	    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
	    return new ResponseEntity<String>("{\"test\": \"jsonResponseExample\"}", httpHeaders, HttpStatus.OK);
	}*/

	
	// OBTENER LISTADO DE TEMAS USUARIO

	@RequestMapping(value = "/resource/temas/{curso_id}", method = RequestMethod.GET, produces = "application/json") 
	public @ResponseBody List<Tema> obtenerListadoTemas(@PathVariable long curso_id) throws JsonProcessingException {
		System.out.println("resources temas get, temas para curso_id:");
		System.out.println(curso_id);
		
		try{
			List<Tema> listadoTemas = temaService.obtenerTemasCurso(curso_id);
            System.out.println(listadoTemas);
			return listadoTemas;
			
			
		} catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
	}
	
	
	// OBTENER TEMA POR ID
	@RequestMapping(value = "/resource/tema/{id_tema}", method = RequestMethod.GET, produces = "application/json") 
	public ResponseEntity <Tema> obtenerTema(@PathVariable long id_tema){
		
		System.out.println(id_tema);

		Tema tema = null;
		
		try{
			tema = temaService.get(id_tema);
            System.out.println(tema.getTitulo());
            return new ResponseEntity<Tema>(tema, HttpStatus.OK);
            
		} catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<Tema>(tema, HttpStatus.NOT_FOUND);
        }
	}	

	
  // CREAR TEMA
    
  @RequestMapping(value = "/auth/tema/", method = RequestMethod.POST)
  public ResponseEntity<Tema> crearTema(@RequestBody Tema tema) {
      System.out.println("Creating tema " + tema.getTitulo());

      System.out.println(tema.getTitulo());
      System.out.println(tema.getSecuencia());
      
      temaService.insert(tema);
      
      // al crear un tema, se crea la teoría y se asocia
      Teoria teoria = new Teoria();
      teoria.setTema_id(tema.getId());
      teoria.setContenido("Contenido del tema...");
      teoriaService.insert(teoria);

      return new ResponseEntity<Tema>(tema, HttpStatus.CREATED);
  }
  
	
  	// BORRAR TEMA
  
	@RequestMapping(value = "/auth/tema/{tema_id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> borrarTema(@PathVariable long tema_id) {
	    System.out.println("borrar tema " + tema_id);
	    
	    Tema tema = temaService.get(tema_id);
	    if(tema != null){
	    	temaService.delete(tema_id);
	    }

	    return new ResponseEntity<Void>(HttpStatus.OK);
	}

	
	// EDITAR TEMA
  
	@RequestMapping(value = "/auth/tema/", method = RequestMethod.PUT)
	public ResponseEntity<Void> editarTema(@RequestBody Tema tema) {
	    System.out.println("editarTema" + tema.getTitulo());

	      System.out.println(tema.getSecuencia());
	      
	    // comprobamos si existe un tema con el ID en la base de datos, y se actualiza el registro
		Tema tema_bd = temaService.get(tema.getId());
		if(tema_bd != null){
			temaService.update(tema);
		}	      

	    return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
}

