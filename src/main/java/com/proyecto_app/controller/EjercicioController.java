package com.proyecto_app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.proyecto_app.model.Ejercicio;
import com.proyecto_app.model.Evaluacion;
import com.proyecto_app.service.EjercicioService;
import com.proyecto_app.service.EvaluacionService;


@Controller
public class EjercicioController {
	
	@Autowired
	private EjercicioService ejercicioService;

	@Autowired
	private EvaluacionService evaluacionService;
	
	// OBTENER LISTADO EJERCICIOS DE UN TEMA
	@RequestMapping(value = "/resource/ejercicio/tema/{tema_id}", method = RequestMethod.GET)
	public ResponseEntity<List<Ejercicio>> obtenerEjercicios(@PathVariable long tema_id) {
		System.out.println(tema_id);
		
		List<Ejercicio> ejercicios = ejercicioService.obtenerEjerciciosTema(tema_id, false);

		return new ResponseEntity<List<Ejercicio>>(ejercicios, HttpStatus.OK);
	}	
	
	// OBTENER LISTADO EJERCICIOS DE UN TEMA PARA ALUMNO - SIN LAS RESPUESTAS
	@RequestMapping(value = "/resource/ejercicio/alumno/tema/{tema_id}", method = RequestMethod.GET)
	public ResponseEntity<List<Ejercicio>> obtenerEjerciciosAlumno(@PathVariable long tema_id) {
		System.out.println(tema_id);
		
		List<Ejercicio> ejercicios = ejercicioService.obtenerEjerciciosTema(tema_id, false);
		
		for(Ejercicio ejercicio: ejercicios){
			ejercicio.setRespuesta(null);
		}
		
		return new ResponseEntity<List<Ejercicio>>(ejercicios, HttpStatus.OK);
	}
	

	// OBTENER LISTADO EJERCICIOS DE EXAMEN DE UN TEMA
	@RequestMapping(value = "/resource/ejercicio/tema/examen/{tema_id}", method = RequestMethod.GET)
	public ResponseEntity<List<Ejercicio>> obtenerEjerciciosExamen(@PathVariable long tema_id) {
		System.out.println(tema_id);
		
		List<Ejercicio> ejercicios = ejercicioService.obtenerEjerciciosTema(tema_id, true);
		
		return new ResponseEntity<List<Ejercicio>>(ejercicios, HttpStatus.OK);
	}
	

	// OBTENER LISTADO EJERCICIOS DE EXAMEN DE UN TEMA - SIN LAS RESPUESTAS
	@RequestMapping(value = "/resource/ejercicio/alumno/tema/examen/{tema_id}", method = RequestMethod.GET)
	public ResponseEntity<List<Ejercicio>> obtenerEjerciciosExamenAlumno(@PathVariable long tema_id) {
		System.out.println(tema_id);
		
		List<Ejercicio> ejercicios = ejercicioService.obtenerEjerciciosTema(tema_id, true);
		
		for(Ejercicio ejercicio: ejercicios){
			ejercicio.setRespuesta(null);
		}
		
		return new ResponseEntity<List<Ejercicio>>(ejercicios, HttpStatus.OK);
	}
	
	
	// OBTENER EJERCICIO POR ID - alumno (no retorna la respuesta)
	@RequestMapping(value = "/resource/ejercicio/alumno/{id_ejercicio}", method = RequestMethod.GET, produces = "application/json") 
	public ResponseEntity <Ejercicio> obtenerEjercicioAlumno(@PathVariable long id_ejercicio){
		System.out.println("Obtener ejercicio java, id recibido:");
		System.out.println(id_ejercicio);

		Ejercicio ejercicio = null;
		
		try{
			ejercicio = ejercicioService.get(id_ejercicio);
						
			if(ejercicio != null){
				ejercicio.setRespuesta(null);
	            return new ResponseEntity<Ejercicio>(ejercicio, HttpStatus.OK);
			}
		} catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<Ejercicio>(ejercicio, HttpStatus.CONFLICT);
        }
		
        return new ResponseEntity<Ejercicio>(ejercicio, HttpStatus.NOT_FOUND);
	}
	
	
	// OBTENER EJERCICIO POR ID - profesor
	@RequestMapping(value = "/resource/ejercicio/{id_ejercicio}", method = RequestMethod.GET, produces = "application/json") 
	public ResponseEntity <Ejercicio> obtenerEjercicio(@PathVariable long id_ejercicio){
		System.out.println("Obtener ejercicio java, id recibido:");
		System.out.println(id_ejercicio);

		Ejercicio ejercicio = null;
		
		try{
			ejercicio = ejercicioService.get(id_ejercicio);
            return new ResponseEntity<Ejercicio>(ejercicio, HttpStatus.OK);
            
		} catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<Ejercicio>(ejercicio, HttpStatus.NOT_FOUND);
        }
	}
	
	
	// OBTENER SOLUCION EJERCICIO PARA ALUMNO
	@RequestMapping(value = "/resource/ejercicio/alumno/solucion/{id_ejercicio}/{id_alumno}", method = RequestMethod.GET, produces = "application/json") 
	public ResponseEntity <String> obtenerEjercicioAlumnoSolucion(@PathVariable long id_ejercicio, @PathVariable long id_alumno){

		String respuesta = null;
		
		try{
			Ejercicio ejercicio = ejercicioService.get(id_ejercicio);
						
			if(ejercicio != null){
				
				// comprobamos si existe una evaluacion calificada para este alumno y ejercicio para permitir mostrar la respuesta
				Evaluacion evaluacion = evaluacionService.obtenerEvaluacion(id_alumno, id_ejercicio);
				if(evaluacion.getCalificacion() >= 0){
					respuesta = "{\"respuesta\":\"" + ejercicio.getRespuesta() + "\"}";
				} else{
					respuesta = "{\"respuesta\":\"(EJERCICIO PENDIENTE DE EVALUACION)\"}";
				}

		        return new ResponseEntity<String>(respuesta, HttpStatus.OK);
			}
			
		} catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<String>(respuesta, HttpStatus.CONFLICT);
        }
		
        return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
	}

	
	// CREAR Ejercicio
  @RequestMapping(value = "/auth/ejercicio/", method = RequestMethod.POST)
  public ResponseEntity<Void> crearEjercicio(@RequestBody Ejercicio ejercicio, UriComponentsBuilder ucBuilder) {
	  
      ejercicioService.insert(ejercicio);

      HttpHeaders headers = new HttpHeaders();
      headers.setLocation(ucBuilder.path("/ejercicio/{id}").buildAndExpand(ejercicio.getId()).toUri());
      return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
  }
  

	// BORRAR Ejercicio
	@RequestMapping(value = "/auth/ejercicio/{id_ejercicio}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> borrarEjercicio(@PathVariable long id_ejercicio) {
	    System.out.println("borrar ejercicio " + id_ejercicio);

	    Ejercicio ejercicio = ejercicioService.get(id_ejercicio);
	    if(ejercicio != null){
	    	ejercicioService.delete(id_ejercicio);
	    }
	    
	    return new ResponseEntity<Void>(HttpStatus.OK);
	}

	// EDITAR EJERCICIO
	  
	@RequestMapping(value = "/auth/ejercicio/", method = RequestMethod.PUT)
	public ResponseEntity<Void> editarTema(@RequestBody Ejercicio ejercicio) {
	      
	    // comprobamos si existe un ejercicio con el ID en la base de datos, y se actualiza el registro
		Ejercicio ejercicio_bd = ejercicioService.get(ejercicio.getId());
		if(ejercicio_bd != null){
			ejercicioService.update(ejercicio);
		}	      

	    return new ResponseEntity<Void>(HttpStatus.OK);
	}

  
}