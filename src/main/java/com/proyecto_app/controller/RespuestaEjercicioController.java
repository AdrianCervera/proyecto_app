package com.proyecto_app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.proyecto_app.model.RespuestaEjercicio;
import com.proyecto_app.service.RespuestaEjercicioService;


@Controller
public class RespuestaEjercicioController {
	
  @Autowired
  private RespuestaEjercicioService respuestaEjercicioService;
	
  // GUARDAR RESPUESTA DE UN EJERCICIO EN TABLA INTERMEDIA
  @RequestMapping(value = "/resource/ejercicio/respuesta/", method = RequestMethod.POST)
  public ResponseEntity<Void> guardarRespuesta(@RequestBody RespuestaEjercicio respuesta, UriComponentsBuilder ucBuilder) {
      System.out.println("Creating respuesta ");

      respuestaEjercicioService.insert(respuesta);

      HttpHeaders headers = new HttpHeaders();
      headers.setLocation(ucBuilder.path("/ejercicio/{id}").buildAndExpand(respuesta.getId()).toUri()); // TODO que es esto
      return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
  }
  
  // OBTENER RESPUESTAS DE EJERCICIOS DE UN TEMA
  @RequestMapping(value = "/resource/ejercicio/respuesta/{tema_id}/{alumno_id}/{examen}", method = RequestMethod.GET)
  public ResponseEntity <List<RespuestaEjercicio>> obtenerRespuesta(@PathVariable long tema_id, @PathVariable long alumno_id, @PathVariable boolean examen) {

	  System.out.println("Obtener respuestas ");
      //RespuestaEjercicio respuesta = respuestaEjercicioService.get(tema_id, alumno_id);
      List<RespuestaEjercicio> respuestas = respuestaEjercicioService.obtenerEjerciciosTema(tema_id, alumno_id, examen);
	  return new ResponseEntity<List<RespuestaEjercicio>>(respuestas, HttpStatus.OK);

  }
    
}
