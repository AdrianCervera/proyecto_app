package com.proyecto_app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.proyecto_app.model.Teoria;
import com.proyecto_app.service.TeoriaService;

@Controller
public class TeoriaController {
	
	@Autowired
	private TeoriaService teoriaService;

	
	// OBTENER TEORIA POR ID DEL TEMA AL QUE PERTENECE
	@RequestMapping(value = "/resource/teoria/tema/{id_tema}", method = RequestMethod.GET, produces = "application/json") 
	public ResponseEntity <Teoria> obtenerTeoria(@PathVariable long id_tema){
		
		System.out.println(id_tema);

		Teoria teoria = null;
		
		try{

            System.out.println("ENTRAMOS EN GET TEMA");
			teoria = teoriaService.get_tema(id_tema);
            System.out.println("VUELTA DE GET TEMA, CONTENIDO:");
            System.out.println(teoria.getContenido());
            return new ResponseEntity<Teoria>(teoria, HttpStatus.OK);
            
		} catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<Teoria>(teoria, HttpStatus.NOT_FOUND);
        }
	}
	
	
	// CREAR TEORIA
  @RequestMapping(value = "/auth/teoria/", method = RequestMethod.POST)
  public ResponseEntity<Void> crearTeoria(@RequestBody Teoria teoria, UriComponentsBuilder ucBuilder) {
      System.out.println("Creating teoria ");
      System.out.println("Creating teoria " + teoria.getContenido());
 
      teoriaService.insert(teoria);

      HttpHeaders headers = new HttpHeaders();
      headers.setLocation(ucBuilder.path("/teoria/{id}").buildAndExpand(teoria.getId()).toUri());
      return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
  }
  
	
	// EDITAR TEMA

	@RequestMapping(value = "/auth/teoria/", method = RequestMethod.PUT)
	public ResponseEntity<Void> editarTeoria(@RequestBody Teoria teoria) {
	    System.out.println("editarTeoria ID: " + teoria.getId());
	      
	    // comprobamos si existe la teoria con el ID en la base de datos, y se actualiza el registro
	    Teoria teoria_bd = teoriaService.get(teoria.getId());
		if(teoria_bd != null){

		    System.out.println("teoriabd ID: " + teoria_bd.getId());
			teoriaService.update(teoria);
		}	      

	    return new ResponseEntity<Void>(HttpStatus.OK);
	}
  
	
	
}