package com.proyecto_app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.proyecto_app.model.Evaluacion;
import com.proyecto_app.service.EvaluacionService;


@Controller
public class EvaluacionController {
	
	@Autowired
	private EvaluacionService evaluacionService;
	

	// OBTENER LISTADO EVALUACIONES DE UN ALUMNO DE UN TEMA - vista lista
	@RequestMapping(value = "/resource/evaluacion/lista/{id_alumno}/{id_tema}", method = RequestMethod.GET)
	public ResponseEntity<List<List<Evaluacion>>> obtenerListadoEvaluaciones(@PathVariable long id_alumno, @PathVariable long id_tema) {
		
		List<List<Evaluacion>> evaluaciones = evaluacionService.obtenerEvaluaciones(id_alumno, id_tema);
		return new ResponseEntity<List<List<Evaluacion>>>(evaluaciones, HttpStatus.OK);
	}
	
	
	// OBTENER EVALUACION POR ID - cargar vista form
	@RequestMapping(value = "/resource/evaluacion/{id_alumno}/{id_ejercicio}", method = RequestMethod.GET, produces = "application/json") 
	public ResponseEntity <Evaluacion> obtenerEvaluacion(@PathVariable long id_alumno, @PathVariable long id_ejercicio){

		Evaluacion evaluacion = null;
		
		try{
			evaluacion = evaluacionService.obtenerEvaluacion(id_alumno, id_ejercicio);
            return new ResponseEntity<Evaluacion>(evaluacion, HttpStatus.OK);
            
		} catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<Evaluacion>(evaluacion, HttpStatus.NOT_FOUND);
        }
	}

	
	// CREAR EVALUACION - respuesta alumno
	@RequestMapping(value = "/resource/evaluacion/", method = RequestMethod.POST)
	public ResponseEntity<Void> crearEvaluacion(@RequestBody Evaluacion evaluacion) {
		  
		// comprobamos si ya existe
		Evaluacion evaluacion_bd = evaluacionService.obtenerEvaluacion(evaluacion.getAlumno_id(), evaluacion.getEjercicio_id());
		if(evaluacion_bd != null){
			System.out.println("Ya existe una evaluacion con IDs de alumno: " + evaluacion.getAlumno_id() + " y ejercicio: " + evaluacion.getEjercicio_id());
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
	
		evaluacionService.crearEvaluacion(evaluacion);
        return new ResponseEntity<Void>(HttpStatus.OK);
	
	}
  

	// EDITAR EVALUACION - evaluacion profesor
	@RequestMapping(value = "/auth/evaluacion/", method = RequestMethod.PUT)
	public ResponseEntity<String> editarEvaluacion(@RequestBody Evaluacion evaluacion) {
		
		if(evaluacion.getCalificacion() < 0 || evaluacion.getCalificacion() > 10){
			String respuesta = "{\"respuesta\":\"La calificación debe ser un entero entre 0 y 10.\"}";
			return new ResponseEntity<String>(respuesta, HttpStatus.CONFLICT);
		}

		Evaluacion evaluacion_bd = evaluacionService.obtenerEvaluacion(evaluacion.getAlumno_id(), evaluacion.getEjercicio_id());
		if(evaluacion_bd != null){
			evaluacionService.editarEvaluacion(evaluacion);
			return new ResponseEntity<String>(HttpStatus.OK);
		}
		
		return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		
	}

	// BORRAR EVALUACION
	@RequestMapping(value = "/resource/evaluacion/{id_alumno}/{id_ejercicio}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> borrarEvaluacion(@PathVariable long id_alumno, @PathVariable long id_ejercicio) {

		Evaluacion evaluacion = evaluacionService.obtenerEvaluacion(id_alumno, id_ejercicio);
	    if(evaluacion != null){
	    	evaluacionService.eliminarEvaluacion(id_alumno, id_ejercicio);
	    }
	    
	    return new ResponseEntity<Void>(HttpStatus.OK);
	}

  
}