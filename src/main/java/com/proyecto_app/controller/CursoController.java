package com.proyecto_app.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.proyecto_app.model.Alumno;
import com.proyecto_app.model.AlumnoCurso;
import com.proyecto_app.model.Curso;
import com.proyecto_app.service.AlumnoCursoService;
import com.proyecto_app.service.CursoService;

@Controller
public class CursoController {
	
	@Autowired
	private CursoService cursoService;
	
	@Autowired
	private AlumnoCursoService alumnoCursoService;
	
	
	// OBTENER LISTADO DE CURSOS (GESTION ADMIN)

	@RequestMapping(value = "/admin/cursos/", method = RequestMethod.GET, produces = "application/json") 
	public @ResponseBody List<Curso> obtenerListadoCursos() throws JsonProcessingException {
		System.out.println("controller cursos admin");
				
		try{
			List<Curso> listadoCursos = cursoService.obtenerCursos();
            System.out.println(listadoCursos);
			return listadoCursos;
			
			
		} catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
	}
	
	
	// OBTENER LISTADO DE CURSOS DEL PROFESOR

	@RequestMapping(value = "/auth/cursos/profesor/{profesor_id}", method = RequestMethod.GET, produces = "application/json") 
	public @ResponseBody List<Curso> obtenerListadoCursosProfesor(@PathVariable long profesor_id) throws JsonProcessingException {
		System.out.println("controller cursos para profesor_id:");
		System.out.println(profesor_id);
				
		try{
			List<Curso> listadoCursos = cursoService.obtenerCursosProfesor(profesor_id);
            System.out.println(listadoCursos);
			return listadoCursos;
			
			
		} catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
	}
	
	
	// OBTENER LISTADO DE CURSOS DEL ALUMNO INSCRITOS/ NO INSCRITOS

	@RequestMapping(value = "/admin/cursos/alumno/{alumno_id}", method = RequestMethod.GET, produces = "application/json") 
	public @ResponseBody List<List<Curso>> obtenerListadosCursosAlumno(@PathVariable long alumno_id) throws JsonProcessingException {
		System.out.println("controller cursos para alumno_id:");
		System.out.println(alumno_id);
		
		List<Curso> listadoCursosInscrito = null;
		List<Curso> listadoCursosNoInscrito = null;
		
		try{
			listadoCursosInscrito = alumnoCursoService.obtenerCursosAlumno(alumno_id);
            System.out.println(listadoCursosInscrito);
			
		} catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

		try{
			listadoCursosNoInscrito = alumnoCursoService.obtenerCursosAlumnoNoInscrito(alumno_id);
		} catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
		 
		List<List<Curso>> listadoCursos = new ArrayList<>();
		listadoCursos.add(listadoCursosInscrito);
		listadoCursos.add(listadoCursosNoInscrito);
		
		
		return listadoCursos;
	}	
	
	// OBTENER LISTADO DE CURSOS PARA EL ALUMNO

	@RequestMapping(value = "/resource/cursos/alumno/{alumno_id}", method = RequestMethod.GET, produces = "application/json") 
	public @ResponseBody List<Curso> obtenerListadoCursosAlumno(@PathVariable long alumno_id) throws JsonProcessingException {
		System.out.println("controller, cursos para alumno_id:");
		System.out.println(alumno_id);
				
		//try{
			List<Curso> listadoCursos = alumnoCursoService.obtenerCursosAlumno(alumno_id);
            System.out.println("controller cursos obtenidos:");
            System.out.println(listadoCursos);
			return listadoCursos;
			
		/*} catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }*/
	}	
	
	
	// OBTENER LISTADO DE ALUMNOS DEL CURSO

	@RequestMapping(value = "/auth/cursos/alumno/curso/{curso_id}", method = RequestMethod.GET, produces = "application/json") 
	public @ResponseBody List<Alumno> obtenerListadoAlumnosDeCurso(@PathVariable long curso_id) throws JsonProcessingException {
				
		List<Alumno> listadoAlumnos = null;
		
		Curso curso_bd = cursoService.get(curso_id);
		if(curso_bd != null){
			listadoAlumnos = alumnoCursoService.obtenerAlumnosCurso(curso_id);
		}
		
		return listadoAlumnos;
			
	}		
	
	
	// SUSCRIBIR ALUMNO EN UN CURSO

	@RequestMapping(value = "/admin/cursos/alumno/curso/{alumno_id}/{curso_id}", method = RequestMethod.POST, produces = "application/json") 
	public ResponseEntity<Void> crearAlumnoCurso(@PathVariable long alumno_id, @PathVariable long curso_id) {
		System.out.println("suscribir alumno " + alumno_id + " en curso " + curso_id);

		try{	
			AlumnoCurso alumnoCurso = new AlumnoCurso();
			alumnoCurso.setAlumnoId(alumno_id);
			alumnoCurso.setCursoId(curso_id);
			alumnoCursoService.crearAlumnoCurso(alumnoCurso);
		} catch (Exception e){
            System.out.println(e.getMessage());
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
		
        System.out.println("Alumno suscrito con éxito");
	    return new ResponseEntity<Void>(HttpStatus.OK);
			
	}
	
	
	// DESUSCRIBIR ALUMNO DE UN CURSO

	@RequestMapping(value = "/admin/cursos/alumno/curso/{alumno_id}/{curso_id}", method = RequestMethod.DELETE, produces = "application/json") 
	public ResponseEntity<Void> borrarAlumnoCurso(@PathVariable long alumno_id, @PathVariable long curso_id) {
		System.out.println("borrar alumno " + alumno_id + " en curso " + curso_id);

		try{
			alumnoCursoService.eliminarAlumnoCurso(alumno_id, curso_id);
		} catch (Exception e){
            System.out.println(e.getMessage());
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		
        System.out.println("Alumno desuscrito con éxito");
	    return new ResponseEntity<Void>(HttpStatus.OK);
			
	}	

	
	// OBTENER CURSO POR ID
	
	@RequestMapping(value = "/admin/curso/{id_curso}", method = RequestMethod.GET, produces = "application/json") 
	public ResponseEntity <Curso> obtenerTema(@PathVariable long id_curso){
		
		Curso curso = null;
		
		try{
			curso = cursoService.get(id_curso);
            System.out.println(curso.getNombre());
            return new ResponseEntity<Curso>(curso, HttpStatus.OK);
            
		} catch (Exception e){
            System.out.println(e.getMessage());
            return new ResponseEntity<Curso>(curso, HttpStatus.NOT_FOUND);
        }
	}	
	
	
	// CREAR CURSO
    
	@RequestMapping(value = "/admin/curso/", method = RequestMethod.POST)
	public ResponseEntity<String> crearTema(@RequestBody Curso curso) {
	    System.out.println("Creando curso " + curso.getNombre() );
	    cursoService.insert(curso);
	
	    return new ResponseEntity<String>(HttpStatus.CREATED);
	}
  
	
	// BORRAR CURSO

	@RequestMapping(value = "/admin/curso/{curso_id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> borrarCurso(@PathVariable long curso_id) {
	    System.out.println("borrar curso " + curso_id);

	    Curso curso = cursoService.get(curso_id);
	    if(curso != null){
	    	cursoService.delete(curso_id);
	    }

	    return new ResponseEntity<Void>(HttpStatus.OK);
	}

	
	// EDITAR CURSO

	@RequestMapping(value = "/admin/curso/", method = RequestMethod.PUT)
	public ResponseEntity<Void> editarCurso(@RequestBody Curso curso) {
	      
	    // comprobamos si existe un tema con el ID en la base de datos, y se actualiza el registro
	    Curso curso_bd = cursoService.get(curso.getId());
		if(curso_bd != null){
			cursoService.update(curso);
		}	      

	    return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
  
}