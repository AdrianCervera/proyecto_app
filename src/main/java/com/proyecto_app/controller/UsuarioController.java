package com.proyecto_app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.proyecto_app.model.Admin;
import com.proyecto_app.model.Alumno;
import com.proyecto_app.model.Profesor;
import com.proyecto_app.model.Usuario;
import com.proyecto_app.model.UsuarioRol;
import com.proyecto_app.service.AdminService;
import com.proyecto_app.service.AlumnoService;
import com.proyecto_app.service.ProfesorService;
import com.proyecto_app.service.UsuarioRolService;
import com.proyecto_app.service.UsuarioService;
import com.proyecto_app.utiles.ValidarFormulario;

@Controller
public class UsuarioController {
	
	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private UsuarioRolService usuarioRolService;
	
	@Autowired
	private AlumnoService alumnoService;

	@Autowired
	private ProfesorService profesorService;

	@Autowired
	private AdminService adminService;

	
	@RequestMapping(value = "/auth/usuario/{tipo}", method = RequestMethod.GET)
	public ResponseEntity<List<?>> listarUsuarios(@PathVariable String tipo) {
		System.out.println(tipo);
		
		List<?> usuarios = null;
				
		if(tipo.equals("admin")){
			//List<Usuario> usuarios = usuarioService.findAllUsers();
			usuarios = adminService.obtenerAdmins();
		}
		else if(tipo.equals("profesor")){
			//List<Usuario> usuarios = usuarioService.findAllUsers();
			usuarios = profesorService.obtenerProfesors();
			
		}
		else if(tipo.equals("alumno")){
			//List<Alumno> alumnos = alumnoService.listadoAlumnos();
			usuarios = alumnoService.obtenerAlumnos();
		}
		
		else{
			System.out.println("Variable tipo no reconocida");
			return new ResponseEntity<List<?>>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<?>>(usuarios, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/auth/usuario/{tipo}/{id_usuario}", method = RequestMethod.GET)
	public ResponseEntity<Usuario> obtenerUsuario(@PathVariable String tipo, @PathVariable long id_usuario) {
		Usuario usuario = usuarioService.get(id_usuario);
		return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
	}
	
	
	///

	@RequestMapping(value = "/auth/usuario/", method = RequestMethod.GET)
	public ResponseEntity<List<Usuario>> listarUsuarios() {
		System.out.println("EN JAVA USUARIO \n");
		
		List<Usuario> usuarios = usuarioService.findAllUsers();
		System.out.println("EN JAVA USUARIO listarUsuarios \n");		
		System.out.println(usuarios);
		//if(usuarios.isEmpty()){
		//	return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
		//}
		return new ResponseEntity<List<Usuario>>(usuarios, HttpStatus.OK);
	}
    
	
  ////////////////////////////////////////////////////////////////////////////////
  //   ALUMNO
  ////////////////////////////////////////////////////////////////////////////////

	
  // CREAR ALUMNO
	
  @RequestMapping(value = "/admin/usuario/alumno/", method = RequestMethod.POST)
  public ResponseEntity<String> crearAlumno(@RequestBody Alumno alumno) {
      System.out.println("Creando alumno ");
      System.out.println(alumno.getDni());
      System.out.println(alumno.getLogin());
      System.out.println(alumno.getTelefono());
      
      
      Alumno alumno_creado = null;
     // UserDetails alumno_creado = null;

      try{
    	  alumno_creado = alumnoService.get(alumno.getLogin());
      } catch (Exception e) {
    	  System.out.println("Exception Usuario controller try l.88");
    	  System.out.println(e);
      }
      
      System.out.println("#############");     
      System.out.println(alumno_creado);
      System.out.println("=============");
	  if(alumno_creado != null){
		  System.out.println("Ya existe el alumno: " + alumno_creado.getLogin());
		  return new ResponseEntity<String>("{\"respuesta\":\"Ya existe un alumno con el login indicado.\"}", HttpStatus.CONFLICT);
		   
	  }
	  else{
		  
		  // eliminamos espacios en el telefono
	      System.out.println(alumno.getTelefono());
		  alumno.setTelefono(alumno.getTelefono().replaceAll("\\s+",""));
	      System.out.println(alumno.getTelefono());
		  
		  // comprobamos datos del formulario
		  ValidarFormulario validarFormulario = new ValidarFormulario().Validar((Usuario)alumno);
	      
		  if(!validarFormulario.isValidado()){
			  String respuesta = "{\"respuesta\":\"" + validarFormulario.getRespuesta() + "\"}";
			  return new ResponseEntity<String>(respuesta, HttpStatus.CONFLICT); 
		  }
		  
		  // si los campos del formulario son correctos, insertamos el alumno  
	      try{
	    	  alumnoService.insert(alumno);
		      System.out.println("ALUMNO CREADO");
		      
		      UsuarioRol usuarioRol = new UsuarioRol(alumno.getId(), 3);
		      usuarioRolService.insert(usuarioRol);
		      System.out.println("ROL USUARIO CREADO");
		      
			  return new ResponseEntity<String>(HttpStatus.CREATED);
	    	  
	      } catch (Exception e) {
	    	  System.out.println("Exception insert alumno");
	    	  System.out.println(e);
			  return new ResponseEntity<String>("{\"respuesta\":\"Error al realizar la inserción en BD.\"}", HttpStatus.CONFLICT); 
	      }
		  
	  }
	  
  }
  
  
  // EDITAR ALUMNO

  @RequestMapping(value = "/admin/usuario/alumno/", method = RequestMethod.PUT)
  public ResponseEntity<String> editarAlumno(@RequestBody Alumno alumno) {

	
	Alumno alumno_actual = alumnoService.get(alumno.getId());
	if(alumno_actual != null){
		
		// eliminamos espacios en el telefono
	    System.out.println(alumno.getTelefono());
		alumno.setTelefono(alumno.getTelefono().replaceAll("\\s+",""));
	    System.out.println(alumno.getTelefono());
		  
		// comprobamos datos del formulario
		ValidarFormulario validarFormulario = new ValidarFormulario().Validar((Usuario)alumno);
	      
		if(!validarFormulario.isValidado()){
			String respuesta = "{\"respuesta\":\"" + validarFormulario.getRespuesta() + "\"}";
			return new ResponseEntity<String>(respuesta, HttpStatus.CONFLICT); 
		}
		  
		// si los campos del formulario son correctos, actualizamos el alumno
		alumnoService.update(alumno);
		System.out.println("Actualizado.");
        return new ResponseEntity<String>("", HttpStatus.OK);
	}

    return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
	  
  }

  
  // BORRAR ALUMNO

  @RequestMapping(value = "/admin/usuario/alumno/{alumno_id}", method = RequestMethod.DELETE)
  public ResponseEntity<String> borrarAlumno(@PathVariable("alumno_id") long alumno_id) {
		System.out.println("borrar id: " + alumno_id);
		
		Alumno alumno = alumnoService.get(alumno_id);
		if(alumno != null){
			alumnoService.delete(alumno_id);
			System.out.println("Borrado.");
		    return new ResponseEntity<String>(HttpStatus.OK);
		}
        return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
  }
  
  
  ////////////////////////////////////////////////////////////////////////////////
  //   PROFESOR
  ////////////////////////////////////////////////////////////////////////////////

  
  // CREAR PROFESOR
  
  @RequestMapping(value = "/admin/usuario/profesor/", method = RequestMethod.POST)
  public ResponseEntity<String> crearProfesor(@RequestBody Profesor profesor) {
      System.out.println("Creando profesor ");
      System.out.println(profesor.getLogin());   
      
      Profesor profesor_creado = null;;

      try{
    	  profesor_creado = profesorService.get(profesor.getLogin());
      } catch (Exception e) {
    	  System.out.println("Exception Usuario controller try l.88");
    	  System.out.println(e);
      }
      
      System.out.println("#############");     
      System.out.println(profesor_creado);
      System.out.println("=============");
	  if(profesor_creado != null){
		  System.out.println("Ya existe el profesor: " + profesor_creado.getLogin());
		  return new ResponseEntity<String>("{\"respuesta\":\"Ya existe un profesor con el login indicado.\"}", HttpStatus.CONFLICT);
		   
	  }
	  else{
		  
		  // eliminamos espacios en el telefono
	      System.out.println(profesor.getTelefono());
	      profesor.setTelefono(profesor.getTelefono().replaceAll("\\s+",""));
	      System.out.println(profesor.getTelefono());
		  
		  // comprobamos datos del formulario
		  ValidarFormulario validarFormulario = new ValidarFormulario().Validar((Usuario)profesor);
	      
		  if(!validarFormulario.isValidado()){
			  String respuesta = "{\"respuesta\":\"" + validarFormulario.getRespuesta() + "\"}";
			  return new ResponseEntity<String>(respuesta, HttpStatus.CONFLICT); 
		  }
		  
		  // si los campos del formulario son correctos, insertamos el profesor  
	      try{
	    	  profesorService.insert(profesor);
		      System.out.println("PROFESOR CREADO");
		      
		      UsuarioRol usuarioRol = new UsuarioRol(profesor.getId(), 2);
		      usuarioRolService.insert(usuarioRol);
		      System.out.println("ROL USUARIO CREADO");
		      
			  return new ResponseEntity<String>(HttpStatus.CREATED);
	    	  
	      } catch (Exception e) {
	    	  System.out.println("Exception insert profesor");
	    	  System.out.println(e);
			  return new ResponseEntity<String>("{\"respuesta\":\"Error al realizar la inserción en BD.\"}", HttpStatus.CONFLICT); 
	      }
		  
	  }
	  
  }
  
  
  
  // EDITAR PROFESOR

	  @RequestMapping(value = "/admin/usuario/profesor/", method = RequestMethod.PUT)
	  public ResponseEntity<String> editarProfesor(@RequestBody Profesor profesor) {
		
		Profesor profesor_actual = profesorService.get(profesor.getId());
		if(profesor_actual != null){
			
		  // eliminamos espacios en el telefono
		  System.out.println(profesor.getTelefono());
		  profesor.setTelefono(profesor.getTelefono().replaceAll("\\s+",""));
		  System.out.println(profesor.getTelefono());
		  
		  // comprobamos datos del formulario
		  ValidarFormulario validarFormulario = new ValidarFormulario().Validar((Usuario)profesor);
		  
		  if(!validarFormulario.isValidado()){
			  String respuesta = "{\"respuesta\":\"" + validarFormulario.getRespuesta() + "\"}";
			  return new ResponseEntity<String>(respuesta, HttpStatus.CONFLICT); 
		  }
			
		  profesorService.update(profesor);
		  System.out.println("Actualizado.");
	      return new ResponseEntity<String>("", HttpStatus.OK);
		}

	    return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		  
	  }  
  
  
  // BORRAR PROFESOR

	  @RequestMapping(value = "/admin/usuario/profesor/{id}", method = RequestMethod.DELETE)
	  public ResponseEntity<String> borrarProfesor(@PathVariable("id") long id) {
			System.out.println("borrar id: " + id);
			
			Profesor profesor = profesorService.get(id);
			if(profesor != null){
				profesorService.delete(id);
				System.out.println("Borrado.");
			    return new ResponseEntity<String>(HttpStatus.OK);
			}
	        return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
	  }
	  
  
	
  ////////////////////////////////////////////////////////////////////////////////
  //   ADMIN
  ////////////////////////////////////////////////////////////////////////////////


  
  // CREAR ADMIN
  
  @RequestMapping(value = "/admin/usuario/admin/", method = RequestMethod.POST)
  public ResponseEntity<String> crearAdmin(@RequestBody Admin admin) {
      System.out.println("Creando admin ");
      System.out.println(admin.getLogin());
      
      Admin admin_creado = null;;

      try{
    	  admin_creado = adminService.get(admin.getLogin());
      } catch (Exception e) {
    	  System.out.println("Exception Usuario controller try l.88");
    	  System.out.println(e);
      }
      
	  if(admin_creado != null){
		  System.out.println("Ya existe el admin: " + admin_creado.getLogin());
		  return new ResponseEntity<String>("{\"respuesta\":\"Ya existe un administrador con el login indicado.\"}", HttpStatus.CONFLICT);
		   
	  }
	  else{
		  
		  // eliminamos espacios en el telefono
	      System.out.println(admin.getTelefono());
	      admin.setTelefono(admin.getTelefono().replaceAll("\\s+",""));
	      System.out.println(admin.getTelefono());
		  
		  // comprobamos datos del formulario
		  ValidarFormulario validarFormulario = new ValidarFormulario().Validar((Usuario)admin);
	      
		  if(!validarFormulario.isValidado()){
			  String respuesta = "{\"respuesta\":\"" + validarFormulario.getRespuesta() + "\"}";
			  return new ResponseEntity<String>(respuesta, HttpStatus.CONFLICT); 
		  }
		  
		  // si los campos del formulario son correctos, insertamos el admin  
	      try{
	    	  adminService.insert(admin);
		      System.out.println("ADMIN CREADO");
		      
		      UsuarioRol usuarioRol = new UsuarioRol(admin.getId(), 1);  // mejora: sacar id rol de la tabla de rol
		      usuarioRolService.insert(usuarioRol);
		      System.out.println("ROL USUARIO CREADO");
		      
			  return new ResponseEntity<String>(HttpStatus.CREATED);
	    	  
	      } catch (Exception e) {
	    	  System.out.println("Exception insert admin");
	    	  System.out.println(e);
			  return new ResponseEntity<String>("{\"respuesta\":\"Error al realizar la inserción en BD.\"}", HttpStatus.CONFLICT); 
	      }
		  
	  }
	  
  }
  
  
  
  // EDITAR ADMIN

	  @RequestMapping(value = "/admin/usuario/admin/", method = RequestMethod.PUT)
	  public ResponseEntity<String> editarAdmin(@RequestBody Admin admin) {
		
		Admin admin_actual = adminService.get(admin.getId());
		if(admin_actual != null){
			
		  // eliminamos espacios en el telefono
		  System.out.println(admin.getTelefono());
		  admin.setTelefono(admin.getTelefono().replaceAll("\\s+",""));
		  System.out.println(admin.getTelefono());
		  
		  // comprobamos datos del formulario
		  ValidarFormulario validarFormulario = new ValidarFormulario().Validar((Usuario)admin);
		  
		  if(!validarFormulario.isValidado()){
			  String respuesta = "{\"respuesta\":\"" + validarFormulario.getRespuesta() + "\"}";
			  return new ResponseEntity<String>(respuesta, HttpStatus.CONFLICT); 
		  }
			
		  adminService.update(admin);
		  System.out.println("Actualizado.");
	      return new ResponseEntity<String>("", HttpStatus.OK);
		}

	    return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		  
	  }  
  
  
      // BORRAR ADMIN

	  @RequestMapping(value = "/admin/usuario/admin/{id}", method = RequestMethod.DELETE)
	  public ResponseEntity<String> borrarAdmin(@PathVariable("id") long id) {
			System.out.println("borrar id: " + id);
			
			Admin admin = adminService.get(id);
			if(admin != null){
				adminService.delete(id);
				System.out.println("Borrado.");
			    return new ResponseEntity<String>(HttpStatus.OK);
			}
	        return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
	  }
	
  
}