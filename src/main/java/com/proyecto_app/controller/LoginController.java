package com.proyecto_app.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.proyecto_app.auth.SecurityService;
import com.proyecto_app.model.Alumno;
import com.proyecto_app.model.Usuario;
import com.proyecto_app.service.AlumnoService;
import com.proyecto_app.service.UsuarioService;

@Controller
public class LoginController {
	
    @Autowired
    private SecurityService securityService;

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private AlumnoService alumnoService;
	
	@Autowired
	private UserDetailsService userDetailsService;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<String> login(@RequestBody ObjectNode login_usuario) {
		  
		System.out.println(login_usuario);
		String login = login_usuario.get("login").textValue();
		String password = login_usuario.get("password").textValue();
		String rol = login_usuario.get("rol").textValue();
		
		System.out.println(rol);
		
		Usuario usuario = usuarioService.get(login);
		if(usuario != null){
			
			if(usuario.getPassword().equals(password)){
				System.out.println("password correcta");
				
				// si es alumno, comprobamos que esta habilitado
				if(rol.equals("alumno")){
					System.out.println("=ES ALUMNO=");
					Alumno alumno = alumnoService.get(usuario.getId());
					if(alumno != null){
						System.out.println(alumno.getLogin());
						System.out.println(alumno.isHabilitado());
						if(!alumno.isHabilitado()){
							System.out.println("Usuario no habilitado.");
							return new ResponseEntity<String>("{\"respuesta\":\"Usuario no habilitado.\"}", HttpStatus.UNAUTHORIZED);
						}
					}
				}
				
				// comprobar usuario.rol = rol
				/*
				 * 
				 * 
				 * 
				 */
				
				// autenticar usuario
				UserDetails user = userDetailsService.loadUserByUsername(login);
				System.out.println("=  AUTHORITIES:  ================");
				System.out.println(user.getAuthorities());
				securityService.autologin(user.getUsername(), user.getPassword());

				System.out.println("{\"respuesta\":" + usuario.getId() + "}");
				return new ResponseEntity<String>("{\"respuesta\":" + usuario.getId() + "}", HttpStatus.OK);
					
			}
			else{
				System.out.println("Contraseña incorrecta");
				return new ResponseEntity<String>("{\"respuesta\":\"Usuario o contraseña incorrecta.\"}", HttpStatus.UNAUTHORIZED);
			}
			
		}
		else{
			System.out.println("usuario no existe");
			return new ResponseEntity<String>("{\"respuesta\":\"Usuario o contraseña incorrecta.\"}", HttpStatus.UNAUTHORIZED);
		}
	}

	
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public ResponseEntity<String> logout(HttpServletRequest request, HttpServletResponse response) {
		
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication(); // concern you

		System.out.println("LOGOUT");
		System.out.println(auth.getName());
		System.out.println(auth.getAuthorities());
		System.out.println("is auth?:");
		System.out.println(auth.isAuthenticated());
		
		// si es usuario anonimo
		if(auth.getAuthorities().size() == 1){
			if(auth.getAuthorities().toString().equals("[ROLE_ANONYMOUS]")){
				System.out.println("is ANONYMOUS");
				return new ResponseEntity<String>(HttpStatus.OK);
			}
		}
		else{
			// logout usuario
		    SecurityContextLogoutHandler ctxLogOut = new SecurityContextLogoutHandler();
	
		    ctxLogOut.logout(request, response, auth); // concern you
	
			System.out.println(auth.getName());
			System.out.println(auth.getAuthorities());
			System.out.println(auth.getAuthorities().size());
			System.out.println("is auth?:");
			System.out.println(auth.isAuthenticated());
				
			return new ResponseEntity<String>(HttpStatus.OK);
		}
		
		return null;

	}

}

