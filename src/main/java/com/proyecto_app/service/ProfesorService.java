package com.proyecto_app.service;

import java.util.List;
import com.proyecto_app.model.Profesor;

public interface ProfesorService{
	
	public List<Profesor> obtenerProfesors();
	
	public void insert(Profesor profesor);

	public void update(Profesor profesor);

	public void delete(long id);

	public Profesor get(long id);
	
	public Profesor get(String login);

}
