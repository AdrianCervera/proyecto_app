package com.proyecto_app.service;

import java.util.List;
import com.proyecto_app.model.Admin;

public interface AdminService{
	
	public List<Admin> obtenerAdmins();
	
	public void insert(Admin admin);

	public void update(Admin admin);

	public void delete(long id);

	public Admin get(long id);
	
	public Admin get(String login);

}
