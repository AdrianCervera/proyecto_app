package com.proyecto_app.service;

import java.util.List;

import com.proyecto_app.model.RespuestaEjercicio;

public interface RespuestaEjercicioService{
	
	//public List<Ejercicio> findAllTeoria();
	
	public void insert(RespuestaEjercicio respuesta);

	public void update(RespuestaEjercicio respuesta);

	public void delete(long id);

	public RespuestaEjercicio get(long tema_id, long alumno_id);
	
	public List<RespuestaEjercicio> obtenerEjerciciosTema(long tema_id, long alumno_id, boolean examen);
	
}
