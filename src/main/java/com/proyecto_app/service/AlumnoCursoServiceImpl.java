package com.proyecto_app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto_app.dao.AlumnoCursoDao;
import com.proyecto_app.model.Alumno;
import com.proyecto_app.model.AlumnoCurso;
import com.proyecto_app.model.Curso;


@Service("alumnoCursoService")
public class AlumnoCursoServiceImpl implements AlumnoCursoService{

    
    private static List<Curso> cursos;
    private static List<Alumno> alumnos;
	
	@Autowired
	private AlumnoCursoDao alumnoCursoDao;
	
	public AlumnoCursoServiceImpl(){
		super();
	}
	
	@Transactional
	public List<Curso> obtenerCursosAlumno(long alumno_id) {
		cursos = alumnoCursoDao.getCursos(alumno_id);
		return cursos;
	}
	
	@Transactional
	public List<Curso> obtenerCursosAlumnoNoInscrito(long alumno_id) {
		cursos = alumnoCursoDao.getCursosNoInscrito(alumno_id);
		return cursos;
	}
	
	@Transactional
	public List<Alumno> obtenerAlumnosCurso(long curso_id) {
		alumnos = alumnoCursoDao.getAlumnos(curso_id);
		return alumnos;
	}
	
	@Transactional
	public void crearAlumnoCurso(AlumnoCurso alumnoCurso) {
		alumnoCursoDao.insert(alumnoCurso);
	}

	@Transactional
	public void eliminarAlumnoCurso(long alumno_id, long curso_id) {
		alumnoCursoDao.delete(alumno_id, curso_id);
	}
	
}