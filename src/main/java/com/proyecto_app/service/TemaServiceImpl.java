package com.proyecto_app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto_app.dao.TemaDao;
import com.proyecto_app.model.Ejercicio;
import com.proyecto_app.model.Tema;
import com.proyecto_app.model.Teoria;


@Service("temaService")
public class TemaServiceImpl implements TemaService{

	@Autowired
	private TeoriaService teoriaService;
	
	@Autowired
	private EjercicioService ejercicioService;
    
    private static List<Tema> tema;
	
	@Autowired
	private TemaDao temaDao;
	
	public TemaServiceImpl(){
		super();
	}

	@Transactional
	public List<Tema> findAllTemas() {
        tema = temaDao.findAllTemas();
		return tema;
	}

	@Transactional
	public List<Tema> obtenerTemasCurso(long curso_id) {
        tema = temaDao.obtenerTemasCurso(curso_id);
		return tema;
	}

	@Transactional
	public void insert(Tema tema) {
		temaDao.insert(tema);
	}
	
	@Transactional
	public void update(Tema tema) {
		temaDao.update(tema);
	}
	
	@Transactional
	public void delete(long id) {
		
	    // eliminamos la teoria relacionada
	    Teoria teoria = teoriaService.get_tema(id);
	    if(teoria != null){
	    	teoriaService.delete(teoria.getId());
	    }
	    
	    // eliminamos los ejercicios relacionados
	    List <Ejercicio> ejercicios_tema = ejercicioService.obtenerEjerciciosTema(id, false);
	    for(Ejercicio ejercicio : ejercicios_tema) {
	    	ejercicioService.delete(ejercicio.getId());
	    }
	    
	    // eliminamos los ejercicios de examen relacionados (por separado por posible futura modificación)
	    List <Ejercicio> ejercicios_examen_tema = ejercicioService.obtenerEjerciciosTema(id, true);
	    for(Ejercicio ejercicio : ejercicios_examen_tema) {
	    	ejercicioService.delete(ejercicio.getId());
	    }
    	
    	// eliminadas dependencias, podemos borrar el tema
		temaDao.delete(id);
	}
	
	@Transactional
	public Tema get(long id) {
		return temaDao.get(id);
	}
	
}