package com.proyecto_app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto_app.dao.UsuarioRolDao;
import com.proyecto_app.model.Rol;
import com.proyecto_app.model.UsuarioRol;


@Service("rolService")
public class UsuarioRolServiceImpl implements UsuarioRolService{

    
    private static List<UsuarioRol> roles;
	
	@Autowired
	private UsuarioRolDao usuarioRolDao;
	
	public UsuarioRolServiceImpl(){
		super();
	}

	@Transactional
	public List<UsuarioRol> findAll() {
        roles = usuarioRolDao.findAll();
		return roles;
	}

	@Transactional
	public void insert(UsuarioRol rol) {
		usuarioRolDao.insert(rol);
		
	}

	@Transactional
	public void delete(UsuarioRol rol) {
		usuarioRolDao.delete(rol);
		
	}

	@Transactional
	public List<Rol> getRoles(long id) {
		List<Rol> roles_obj = usuarioRolDao.getRoles(id);
		return roles_obj;
	}

	
}
