package com.proyecto_app.service;

import java.util.List;

import com.proyecto_app.model.Ejercicio;

public interface EjercicioService{
	
	//public List<Ejercicio> findAllTeoria();
	
	public void insert(Ejercicio ejercicio);

	public void update(Ejercicio ejercicio);

	public void delete(long id);

	public Ejercicio get(long id);
	
	public List<Ejercicio> obtenerEjerciciosTema(long tema_id, boolean examen);
	
}
