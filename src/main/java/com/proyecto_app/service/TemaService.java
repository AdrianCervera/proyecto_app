package com.proyecto_app.service;

import java.util.List;

import com.proyecto_app.model.Tema;

public interface TemaService{
	
	public List<Tema> findAllTemas();
	
	public List<Tema> obtenerTemasCurso( long curso_id);
	
	public void insert(Tema tema);

	public void update(Tema tema);

	public void delete(long id);

	public Tema get(long id);
	
}
