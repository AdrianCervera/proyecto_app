package com.proyecto_app.service;

import java.util.List;

import com.proyecto_app.model.Curso;

public interface CursoService{
	
	public List<Curso> obtenerCursos();
	
	public List<Curso> obtenerCursosProfesor(long profesor_id);
	
	public void insert(Curso curso);

	public void update(Curso curso);

	public void delete(long id);

	public Curso get(long id);
	
}
