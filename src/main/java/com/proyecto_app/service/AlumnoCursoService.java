package com.proyecto_app.service;

import java.util.List;

import com.proyecto_app.model.Alumno;
import com.proyecto_app.model.AlumnoCurso;
import com.proyecto_app.model.Curso;

public interface AlumnoCursoService{
	
	public List<Curso> obtenerCursosAlumno(long alumno_id);  // cursos en los que esta inscrito el alumno
	
	public List<Curso> obtenerCursosAlumnoNoInscrito(long alumno_id);  // cursos en los que NO esta inscrito el alumno
	
	public List<Alumno> obtenerAlumnosCurso(long curso_id);
		
	public void crearAlumnoCurso(AlumnoCurso alumnoCurso);  // inscribir un alumno en un curso
	
	public void eliminarAlumnoCurso(long alumno_id, long curso_id);  // eliminar un alumno de un curso
	
}
