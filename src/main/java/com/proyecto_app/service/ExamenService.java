package com.proyecto_app.service;

import com.proyecto_app.model.Examen;

public interface ExamenService{
		
	public void insert(Examen examen);

	public void update(Examen examen);

	public void delete(long id);

	public Examen get(long id);
	
}
