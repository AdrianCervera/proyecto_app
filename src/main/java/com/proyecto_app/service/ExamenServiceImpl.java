package com.proyecto_app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto_app.dao.ExamenDao;
import com.proyecto_app.model.Examen;


@Service("examenService")
public class ExamenServiceImpl implements ExamenService{

    	
	@Autowired
	private ExamenDao examenDao;
	
	public ExamenServiceImpl(){
		super();
	}

	@Transactional
	public void insert(Examen examen) {
		examenDao.insert(examen);
	}
	
	@Transactional
	public void update(Examen examen) {
		examenDao.insert(examen);
	}
	
	@Transactional
	public void delete(long id) {
		examenDao.delete(id);
	}
	
	@Transactional
	public Examen get(long id) {
		return examenDao.get(id);
	}

	
}