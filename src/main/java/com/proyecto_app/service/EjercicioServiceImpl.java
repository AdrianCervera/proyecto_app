package com.proyecto_app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto_app.dao.EjercicioDao;
import com.proyecto_app.model.Ejercicio;


@Service("ejercicioService")
public class EjercicioServiceImpl implements EjercicioService{

	
    private static List<Ejercicio> ejercicios;
	
	@Autowired
	private EjercicioDao ejercicioDao;
	
	public EjercicioServiceImpl(){
		super();
	}

	@Transactional
	public void insert(Ejercicio ejercicio) {
		ejercicioDao.insert(ejercicio);
	}
	
	@Transactional
	public void update(Ejercicio ejercicio) {
		ejercicioDao.update(ejercicio);
	}
	
	@Transactional
	public void delete(long id) {
		ejercicioDao.delete(id);
	}
	
	@Transactional
	public Ejercicio get(long id) {
		return ejercicioDao.get(id);
	}

	// obtener los ejercicios de un tema - el bool examen indica si obtiene los ejercicios tipo examen del tema
	@Transactional
	public List<Ejercicio> obtenerEjerciciosTema(long tema_id, boolean examen) {
		ejercicios = ejercicioDao.obtenerEjercicios(tema_id, examen);
		return ejercicios;
	}


	
}