package com.proyecto_app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto_app.dao.AdminDao;
import com.proyecto_app.model.Admin;

@Service("adminService")
public class AdminServiceImpl implements AdminService{
	
	@Autowired
	private AdminDao adminDao;
	
	
	public AdminServiceImpl(){
		super();
	}

	@Transactional
	public List<Admin> obtenerAdmins() {
        List<Admin> admins = adminDao.obtenerAdmins();
		return admins;
	}

	@Transactional
	public void insert(Admin admin) {
		adminDao.insert(admin);
	}
	
	@Transactional
	public void update(Admin admin) {
		adminDao.update(admin);
	}
	
	@Transactional
	public void delete(long id) {
		adminDao.delete(id);
	}
	
	@Transactional
	public Admin get(long id) {
		return adminDao.get(id);
	}
	
	@Transactional
	public Admin get(String login) {
		return adminDao.get(login);
	}
		
}