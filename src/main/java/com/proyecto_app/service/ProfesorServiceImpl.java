package com.proyecto_app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto_app.dao.ProfesorDao;
import com.proyecto_app.model.Profesor;

@Service("profesorService")
public class ProfesorServiceImpl implements ProfesorService{
	
	@Autowired
	private ProfesorDao profesorDao;
	
	
	public ProfesorServiceImpl(){
		super();
	}

	@Transactional
	public List<Profesor> obtenerProfesors() {
        List<Profesor> profesors = profesorDao.obtenerProfesors();
		return profesors;
	}

	@Transactional
	public void insert(Profesor profesor) {
		profesorDao.insert(profesor);
	}
	
	@Transactional
	public void update(Profesor profesor) {
		profesorDao.update(profesor);
	}
	
	@Transactional
	public void delete(long id) {
		profesorDao.delete(id);
	}
	
	@Transactional
	public Profesor get(long id) {
		return profesorDao.get(id);
	}
	
	@Transactional
	public Profesor get(String login) {
		return profesorDao.get(login);
	}
		
}