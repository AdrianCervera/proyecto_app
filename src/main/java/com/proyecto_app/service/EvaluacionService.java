package com.proyecto_app.service;

import java.util.List;
import com.proyecto_app.model.Evaluacion;

public interface EvaluacionService{
	
	public Evaluacion obtenerEvaluacion(long alumno_id, long ejercicio_id);  // obtener evaluacion
		
	// obtener evaluaciones alumno de un tema (profesor) -> array de array de evaluaciones ejs normales y array ejs examen
	public List<List<Evaluacion>> obtenerEvaluaciones(long alumno_id, long tema_id);
		
	public void crearEvaluacion(Evaluacion evaluacion);  // se crea una evaluacion -> alumno al responder ejercicio

	public void editarEvaluacion(Evaluacion evaluacion);  // se edita una evaluacion -> profesor al evaluar ejercicio
	
	public void eliminarEvaluacion(long alumno_id, long ejercicio_id);  // eliminar una evaluacion
	
}
