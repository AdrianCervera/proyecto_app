package com.proyecto_app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto_app.dao.AlumnoCursoDao;
import com.proyecto_app.dao.AlumnoDao;
import com.proyecto_app.dao.EvaluacionDao;
import com.proyecto_app.dao.TemaDao;
import com.proyecto_app.model.Alumno;
import com.proyecto_app.model.AlumnoCurso;
import com.proyecto_app.model.Tema;
import com.proyecto_app.model.Ejercicio;
import com.proyecto_app.model.Evaluacion;



@Service("evaluacionService")
public class EvaluacionServiceImpl implements EvaluacionService{

    
    private static List<Alumno> alumnos;
    private static List<Tema> temas;
    private static List<Ejercicio> ejercicios;
    private static List<Evaluacion> evaluaciones;

	@Autowired
	private EvaluacionDao evaluacionDao;

	@Autowired
	private TemaDao temaDao;

	@Autowired
	private AlumnoDao alumnoDao;

	
	@Transactional
	public void crearAlumnoCurso(Evaluacion evaluacion) {
		evaluacionDao.insert(evaluacion);
	}

	@Transactional
	public void eliminarAlumnoCurso(long alumno_id, long curso_id) {
		evaluacionDao.delete(alumno_id, curso_id);
	}

	@Transactional
	public Evaluacion obtenerEvaluacion(long alumno_id, long ejercicio_id) {  // TODO
		// evaluacion concreta para responder y evaluar (vista form)
		return evaluacionDao.get(alumno_id, ejercicio_id);
	}

	@Transactional
	public List<List<Evaluacion>> obtenerEvaluaciones(long alumno_id, long tema_id) {  // TODO
		// buscar para el tema y el alumno, todos los ejercicios en 2 arrays, de tipo normal y examen (vista lista)
		return null;
	}

	@Transactional
	public void crearEvaluacion(Evaluacion evaluacion) {
		evaluacionDao.insert(evaluacion);
	}

	@Transactional
	public void editarEvaluacion(Evaluacion evaluacion) {
		evaluacionDao.update(evaluacion);
	}

	@Transactional
	public void eliminarEvaluacion(long alumno_id, long ejercicio_id) {
		evaluacionDao.delete(alumno_id, ejercicio_id);
	}

	
}