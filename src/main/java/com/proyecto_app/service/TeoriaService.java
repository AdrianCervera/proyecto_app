package com.proyecto_app.service;

import com.proyecto_app.model.Teoria;

public interface TeoriaService{
		
	public void insert(Teoria teoria);

	public void update(Teoria teoria);

	public void delete(long id);

	public Teoria get(long id);
	
	public Teoria get_tema(long id_tema);
	
}
