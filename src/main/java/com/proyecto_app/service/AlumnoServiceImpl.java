package com.proyecto_app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto_app.dao.AlumnoDao;
import com.proyecto_app.model.Alumno;

@Service("alumnoService")
public class AlumnoServiceImpl implements AlumnoService{
	
	@Autowired
	private AlumnoDao alumnoDao;
	
	
	public AlumnoServiceImpl(){
		super();
	}

	@Transactional
	public List<Alumno> obtenerAlumnos() {
        List<Alumno> alumnos = alumnoDao.obtenerAlumnos();
		return alumnos;
	}

	@Transactional
	public void insert(Alumno alumno) {
		alumnoDao.insert(alumno);
	}
	
	@Transactional
	public void update(Alumno alumno) {
		alumnoDao.update(alumno);
	}
	
	@Transactional
	public void delete(long id) {
		alumnoDao.delete(id);
	}
	
	@Transactional
	public Alumno get(long id) {
		return alumnoDao.get(id);
	}
	
	@Transactional
	public Alumno get(String login) {
		return alumnoDao.get(login);
		
	}
/*
	@Transactional
	public UserDetails get(String login) {
		System.out.println("get login");
		
		// obtenemos el alumno de la bd
		Alumno alumno = alumnoDao.get(login);
				
		// obtenemos los roles
		List<GrantedAuthority> authorities = buildUserAuthority(usuariRolDao.getRoles(alumno.getId()));
		
		System.out.println("====================\nauthorities:");
		System.out.println(authorities);
		
		return buildUserForAuthentication(alumno, authorities);
		
		//return alumno;
	}
	

	// convert user to org.springframework.security.core.userdetails.User
	private UserDetails buildUserForAuthentication(Alumno alumno, List<GrantedAuthority> authorities) {
		return new org.springframework.security.core.userdetails.User(alumno.getLogin(), alumno.getPassword(), authorities);
	}
	
	

	private List<GrantedAuthority> buildUserAuthority(List<Rol> list) {

		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();
		System.out.println(list);
		System.out.println("== for: ==");

		// Build user's authorities
		for (Rol rol : list) {
			setAuths.add(new SimpleGrantedAuthority(rol.getRol()));
		}
		System.out.println(setAuths);

		List<GrantedAuthority> result = new ArrayList<GrantedAuthority>(setAuths);
		System.out.println(result);

		return result;
	}
	
	*/
	/* /////////////  SECURITY GUIAS /////////////////////////// 
	
	http://www.mkyong.com/spring-security/spring-security-hibernate-annotation-example/
	https://hellokoding.com/registration-and-login-example-with-spring-security-spring-boot-spring-data-jpa-hsql-jsp/
	
	*/////////////////////////////////////////	
	
}