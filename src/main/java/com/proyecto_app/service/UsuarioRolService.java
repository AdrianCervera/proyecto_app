package com.proyecto_app.service;

import com.proyecto_app.model.Rol;
import com.proyecto_app.model.UsuarioRol;

import java.util.List;

public interface UsuarioRolService{
	
	public List<UsuarioRol> findAll();
	
	public void insert(UsuarioRol rol);
	
	public void delete(UsuarioRol rol);

	public List<Rol> getRoles(long id);  // devuelve roles de un usuario
	
}
