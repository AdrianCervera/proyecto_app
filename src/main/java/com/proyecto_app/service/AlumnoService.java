package com.proyecto_app.service;

import java.util.List;
import com.proyecto_app.model.Alumno;

public interface AlumnoService{
	
	public List<Alumno> obtenerAlumnos();
	
	public void insert(Alumno alumno);

	public void update(Alumno alumno);

	public void delete(long id);

	public Alumno get(long id);
	
	public Alumno get(String login);

}
