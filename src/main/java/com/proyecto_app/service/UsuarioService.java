package com.proyecto_app.service;

import com.proyecto_app.model.Usuario;
import java.util.List;

public interface UsuarioService{
	
	public List<Usuario> findAllUsers();
	
	public void insert(Usuario usuario);

	public void update(Usuario usuario);

	public void delete(long id);

	public Usuario get(long id);
	
	public Usuario get(String login);
	
}
