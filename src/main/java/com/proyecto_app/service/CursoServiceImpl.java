package com.proyecto_app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto_app.dao.CursoDao;
import com.proyecto_app.model.Curso;
import com.proyecto_app.model.Tema;


@Service("cursoService")
public class CursoServiceImpl implements CursoService{

	@Autowired
	private TemaService temaService;
    
    private static List<Curso> cursos;
	
	@Autowired
	private CursoDao cursoDao;
	
	public CursoServiceImpl(){
		super();
	}
	
	@Transactional
	public List<Curso> obtenerCursos() {
        cursos = cursoDao.obtenerCursos();
		return cursos;
	}
	
	@Transactional
	public List<Curso> obtenerCursosProfesor(long profesor_id) {
        cursos = cursoDao.obtenerCursosProfesor(profesor_id);
		return cursos;
	}

	@Transactional
	public void insert(Curso curso) {
		cursoDao.insert(curso);
	}
	
	@Transactional
	public void update(Curso curso) {
		cursoDao.update(curso);
	}
	
	@Transactional
	public void delete(long id) {
		
		// eliminamos todo el contenido relacionado con el curso
	    List <Tema> temas = temaService.obtenerTemasCurso(id);
	    for(Tema tema : temas) {
	    	temaService.delete(tema.getId());
	    }
	    
	    // TODO faltaria eliminar alumno_curso_rel - obtener registros del curso y borrarlos
    	
    	// eliminadas dependencias, podemos borrar el curso
		
		cursoDao.delete(id);
	}
	
	@Transactional
	public Curso get(long id) {
		return cursoDao.get(id);
	}
	
}