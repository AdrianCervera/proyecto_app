package com.proyecto_app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto_app.dao.RespuestaEjercicioDao;
import com.proyecto_app.model.RespuestaEjercicio;


@Service("respuestaEjercicioService")
public class RespuestaEjercicioServiceImpl implements RespuestaEjercicioService{

	
    private static List<RespuestaEjercicio> respuestas;
	
	@Autowired
	private RespuestaEjercicioDao respuestaDao;
	
	public RespuestaEjercicioServiceImpl(){
		super();
	}

	@Transactional
	public void insert(RespuestaEjercicio ejercicio) {
		respuestaDao.insert(ejercicio);
	}
	
	@Transactional
	public void update(RespuestaEjercicio ejercicio) {
		respuestaDao.insert(ejercicio);
	}
	
	@Transactional
	public void delete(long id) {
		respuestaDao.delete(id);
	}
	
	@Transactional
	public RespuestaEjercicio get(long tema_id, long alumno_id) {
		return respuestaDao.get(tema_id, alumno_id);
	}

	// obtener las respuestas de ejercicios de un tema para un alumno - el bool examen indica si obtiene de los ejercicios tipo examen del tema
	@Transactional
	public List<RespuestaEjercicio> obtenerEjerciciosTema(long tema_id, long alumno_id, boolean examen) {
		respuestas = respuestaDao.obtenerRespuestas(tema_id, alumno_id, examen);
		return respuestas;
	}


	
}