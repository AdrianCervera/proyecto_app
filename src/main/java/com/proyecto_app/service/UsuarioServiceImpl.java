package com.proyecto_app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto_app.dao.UsuarioDao;
import com.proyecto_app.model.Usuario;


@Service("usuarioService")
public class UsuarioServiceImpl implements UsuarioService{

    
    private static List<Usuario> usuarios;
	
	@Autowired
	private UsuarioDao usuarioDao;
	
	public UsuarioServiceImpl(){
		super();
	}

	@Transactional
	public List<Usuario> findAllUsers() {
		usuarios = usuarioDao.findAllUsers();
		return usuarios;
	}

	@Transactional
	public void insert(Usuario usuario) {
		usuarioDao.insert(usuario);
	}
	
	@Transactional
	public void update(Usuario usuario) {
		usuarioDao.insert(usuario);
	}
	
	@Transactional
	public void delete(long id) {
		usuarioDao.delete(id);
	}
	
	@Transactional
	public Usuario get(long id) {
		return usuarioDao.get(id);
	}

	@Transactional
	public Usuario get(String login) {
		return usuarioDao.get(login);
	}
	
}
