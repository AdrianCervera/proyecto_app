package com.proyecto_app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto_app.dao.TeoriaDao;
import com.proyecto_app.model.Teoria;


@Service("teoriaService")
public class TeoriaServiceImpl implements TeoriaService{
	
	@Autowired
	private TeoriaDao teoriaDao;
	
	public TeoriaServiceImpl(){
		super();
	}

	@Transactional
	public void insert(Teoria teoria) {
		teoriaDao.insert(teoria);
	}
	
	@Transactional
	public void update(Teoria teoria) {
		teoriaDao.update(teoria);
	}
	
	@Transactional
	public void delete(long id) {
		teoriaDao.delete(id);
	}
	
	@Transactional
	public Teoria get(long id) {
		return teoriaDao.get(id);
	}
	@Transactional
	public Teoria get_tema(long id_tema) {
		return teoriaDao.get_tema(id_tema);
	}
	
}